#!/bin/sh
#cd mount
#find . -print0 | cpio -o --null --format=newc > ../debugfs.cpio
#cd ..
qemu-system-x86_64 \
    -m 1024M \
    -kernel bzImage \
    -initrd debugfs.cpio \
    -monitor /dev/null \
    -append "root=/dev/ram console=ttyS0 oops=panic panic=1 kpti=1 nokaslr quiet" \
    -cpu kvm64,+smep,+smap \
    -smp cores=2,threads=2 \
    -nographic -enable-kvm \
    -gdb tcp::12345
