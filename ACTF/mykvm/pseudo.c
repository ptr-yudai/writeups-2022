// local variable allocation has failed, the output may be wrong!
unsigned __int64 __fastcall run_code(const void *code, size_t codesize)
{
  int fd; // [rsp+10h] [rbp-220h]
  int vmfd; // [rsp+14h] [rbp-21Ch]
  int vcpufd; // [rsp+1Ch] [rbp-214h]
  size_t mmap_size; // [rsp+30h] [rbp-200h]
  kvm_run *run; // [rsp+38h] [rbp-1F8h]
  kvm_userspace_memory_region region; // [rsp+40h] [rbp-1F0h] BYREF
  kvm_regs regs; // [rsp+60h] [rbp-1D0h] OVERLAPPED BYREF
  kvm_sregs sregs; // [rsp+F0h] [rbp-140h] BYREF
  unsigned __int64 canary; // [rsp+228h] [rbp-8h]

  canary = __readfsqword(0x28u);

  fd = open("/dev/kvm", 524290);
  if ( fd == -1 )
    errx(1, "failed to open /dev/kvm");

  /* VM作成 */
  vmfd = ioctl(fd, KVM_CREATE_VM, 0LL);

  /* ROM用意 */
  memcpy(
    (void *)(int)((_DWORD)&unk_602100
                - (((((unsigned int)((int)&unk_602100 >> 31) >> 20) + (unsigned __int16)&unk_602100) & 0xFFF)
                 - ((unsigned int)((int)&unk_602100 >> 31) >> 20))
                + 4096),
    code,
    codesize);

  region.slot = 0;
  region.flags = 0;
  region.guest_phys_addr = 0LL;
  region.memory_size = 0x40000000LL;
  region.userspace_addr = (int)((_DWORD)&unk_602100
                              - (((((unsigned int)((int)&unk_602100 >> 31) >> 20) + (unsigned __int16)&unk_602100) & 0xFFF)
                               - ((unsigned int)((int)&unk_602100 >> 31) >> 20))
                              + 4096);
  ioctl(vmfd, KVM_SET_USER_MEMORY_REGION, &region);

  /* CPU作成 */
  vcpufd = ioctl(vmfd, KVM_CREATE_VCPU, 0LL);
  mmap_size = ioctl(fd, KVM_GET_VCPU_MMAP_SIZE, 0LL);
  run = (kvm_run *)mmap(0LL, mmap_size, 3, 1, vcpufd, 0LL);

  ioctl(vcpufd, KVM_GET_REGS, &regs);
  regs.rip = 0LL;
  regs.rsp = 0x4000LL;
  regs.rflags = 2LL;
  ioctl(vcpufd, KVM_SET_REGS, &regs);

  ioctl(vcpufd, KVM_GET_SREGS, &sregs);
  sregs.cs.selector = 0;
  sregs.cs.base = 0LL;
  ioctl(vcpufd, KVM_SET_SREGS, &sregs);

  /* 実行 */
  while ( 1 )
  {
    ioctl(vcpufd, KVM_RUN, 0LL);
    switch ( run->exit_reason )
    {
      case KVM_EXIT_IO:
        putchar(*((char *)&run->request_interrupt_window + run->io.data_offset));
        break;
      case KVM_PPC_PVINFO_FLAGS_EV_IDLE|KVM_MP_STATE_SIPI_RECEIVED:
        fwrite("KVM_EXIT_HLT\n", 1uLL, 0xDuLL, stderr);
        return __readfsqword(0x28u) ^ canary;
      case KVM_EXIT_SHUTDOWN:
        errx(1, "KVM_EXIT_SHUTDOWN");
      case KVM_PPC_PVINFO_FLAGS_EV_IDLE|KVM_PPC_PAGE_SIZES_MAX_SZ:
        errx(1, "KVM_EXIT_FAIL_ENTRY: hardware_entry_failure_reason = 0x%llx", run->hw.hardware_exit_reason);
      case KVM_PPC_PVINFO_FLAGS_EV_IDLE|KVM_S390_RESET_IPL:
        errx(1, "KVM_EXIT_INTERNAL_ERROR: suberror = 0x%x", run->ex.exception);
      default:
        errx(1, "Unhandled reason: %d", run->exit_reason);
    }
  }
}
