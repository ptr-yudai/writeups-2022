from ptrlib import *
import random

def insert(x, y, name):
    assert len(name) == 0x20
    sock.sendlineafter("choice > ", "0")
    sock.sendlineafter("value: ", str(x))
    sock.sendlineafter("value: ", str(y))
    sock.sendafter("name: ", name)
def remove(x, y):
    sock.sendlineafter("choice > ", "1")
    sock.sendlineafter("value: ", str(x))
    sock.sendlineafter("value: ", str(y))
def edit(x, y, name):
    assert len(name) == 0x20
    sock.sendlineafter("choice > ", "2")
    sock.sendlineafter("value: ", str(x))
    sock.sendlineafter("value: ", str(y))
    sock.sendafter("name: ", name)
def show(x, y):
    sock.sendlineafter("choice > ", "3")
    sock.sendlineafter("value: ", str(x))
    sock.sendlineafter("value: ", str(y))
def query(x1, y1, x2, y2):
    sock.sendlineafter("choice > ", "4")
    sock.sendlineafter("value: ", str(x1))
    sock.sendlineafter("value: ", str(y1))
    sock.sendlineafter("value: ", str(x2))
    sock.sendlineafter("value: ", str(y2))

import os
while True:
    os.system("reset")
    sock = Process("./treepwn")

    field = set()

    for rnd in range(0x200):
        choice = random_int(0, 1)
        #choice = random_int(0, 3)
        if choice == 0:
            if len(field) < 0x40:
                x = random_int(0, 9)
                y = random_int(0, 9)
                name = b"A"*0x20 # random_bytes(0x20, 0x20)
                print(f"insert({x}, {y}, {name})")
                insert(x, y, name)
                field.add((x, y))

        elif choice == 1 or choice == 2:
            if len(field):
                x, y = random.sample(field, 1)[0]
                print(f"remove({x}, {y})")
                remove(x, y)
                field.remove((x, y))

        elif choice == 2:
            name = random_bytes(0x20, 0x20)
            if len(field):
                x, y = random.sample(field, 1)[0]
                print(f"edit({x}, {y}, {name})")
                edit(x, y, name)

        elif choice == 3:
            x1 = random_int(0, 0xfff)
            y1 = random_int(0, 0xfff)
            x2 = random_int(0, 0xfff)
            y2 = random_int(0, 0xfff)
            print(f"query({x1}, {y1}, {x2}, {y2})")
            query(x1, y1, x2, y2)

    sock.close()
