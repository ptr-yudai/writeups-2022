import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;

class Main {
    public static void main(String[] args) {
        byte[] srcBuff = Base64.getDecoder().decode
            ("7mePfqpM6Wd1El2sj4dlUboU6PieF7La8IJ1e76cfp4=");

        SecretKeySpec keySpec = new SecretKeySpec
            ("XNV]JSXNV]JRXOV\\87465231".getBytes(), "AES");
        IvParameterSpec ivSpec = new IvParameterSpec
            ("PXOPYJTYMP^M\\ZOWiat15620".getBytes());

        try {
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            System.out.println("A");
            cipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);
            System.out.println("B");
            String x = new String(cipher.doFinal(srcBuff));

            System.out.println(x);
        } catch(Exception e) {
            System.out.println(e);
        }
    }
}
