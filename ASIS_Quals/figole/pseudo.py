from Crypto.Cipher import AES

## MainActivity?
assert l2() == l3(flag, l1())

# UT.gb= base64.b64decode

# UT.gf>
# DH.gb= "7mePfqpM6Wd1El2sj4dlUboU6PieF7La8IJ1e76cfp4="

# UT.gff>
# DH.gdd= "58504e58564f5d504a59534a58544e59564d5d504a5e524d585c4f5a564f5c57"

# UT.g= 引数のhex文字列hsをバイト列にする（dead12 --> \xde\xad\x12）

# UT.td= 16進数charを10進数intに直す ('d'->13)

# UT.hb= 引数の1バイトhex文字列hsを数字に直す ("21"->33)

# DX.getK=
def getK(t: int, s: str):
    total: bytes = UT.g(s)
    c = [0 for i in range(len(total) // 2)]
    if t == 0:
        for i in range(len(total)):
            if i % 2 == 0:
                c[i // 2] = total[i]
    else:
        for i in range(len(total)):
            if i % 2 == 1:
                c[i // 2] = total[i]
    return bytes(c)

# DX.ech=
def ech(message: str, s: str):
    key = DX.getK(0, s + "38693761347436313535323633323130")
    iv  = DX.getK(1, s + "38693761347436313535323633323130")
    cipher = AES.new(key, AES.MODE_CBC, iv)
    return base64.b64encode(cipher.encrypt(message))

# MainActivity.l1
def l1():
    #UT.gf()
    return "7mePfqpM6Wd1El2sj4dlUboU6PieF7La8IJ1e76cfp4="

# MainActivity.l2
def l2():
    #UT.gff()
    return "58504e58564f5d504a59534a58544e59564d5d504a5e524d585c4f5a564f5c57"

# MainActivity.l3
def l3(s1: str, s2: str):
    DX.ech(s1, s2)

# DActivity
assert y == l3(flag, cdec(q))

# DActivity.cdec
def cdec(s):
    return xor(bytes.fromhex(s), "key")
