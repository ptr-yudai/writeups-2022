import base64
from Crypto.Cipher import AES
from ptrlib import xor

"""
l3(flag, "7mePfqpM6Wd1El2sj4dlUboU6PieF7La8IJ1e76cfp4=")
--> "58504e58564f5d504a59534a58544e59564d5d504a5e524d585c4f5a564f5c57"
"""

def g(s):
    o = b''
    for i in range(0, len(s), 2):
        o += bytes.fromhex(s[i:i+2])
    return o

def getK(t: int, s: str):
    total: bytes = g(s)
    print(total, len(total))
    c = [0 for i in range(len(total) // 2)]
    if t == 0:
        for i in range(len(total)):
            if i % 2 == 0:
                c[i // 2] = total[i]
    else:
        for i in range(len(total)):
            if i % 2 == 1:
                c[i // 2] = total[i]
    return bytes(c)

c = base64.b64decode("7mePfqpM6Wd1El2sj4dlUboU6PieF7La8IJ1e76cfp4=")
s = "58504e58564f5d504a59534a58544e59564d5d504a5e524d585c4f5a564f5c57"
s = xor(bytes.fromhex(s), "key").decode()
key = getK(0, s + "38693761347436313535323633323130")
iv  = getK(1, s + "38693761347436313535323633323130")
print(key)
print(iv)
cipher = AES.new(key, AES.MODE_CBC, iv)
print(cipher.decrypt(c))
