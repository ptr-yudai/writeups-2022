import re
import string

flags = []

with open("output.txt", "r") as f:
    for l in f:
        r = re.findall("ASIS\{(.+)\}, d = (\d+), i = (\d+)", l)
        if len(r) == 0: continue
        r = r[0]

        d, i = int(r[1]), int(r[2])
        if d == 40:
            flags.append(r[0])

real_flag = ""

B = set(list(string.printable[:62] + '!?@-_{|}'))
for i in range(40):
    A = set()
    for flag in flags:
        A.add(flag[i])

    real_flag += (B - A).pop()

print(real_flag)
