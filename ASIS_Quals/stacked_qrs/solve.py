from PIL import Image
from pyzbar.pyzbar import decode

def put_marker(img, x, y):
    for i in range(8):
        for j in range(8):
            img.putpixel((x + j, y + i), 1)
    for i in range(8):
        img.putpixel((x + i, y), 0)
        img.putpixel((x, y + i), 0)
        img.putpixel((x + i, y + 7), 0)
        img.putpixel((x + 7, y + i), 0)
    for i in range(4):
        for j in range(4):
            img.putpixel((x + 2 + j, y + 2 + i), 0)

W = H = 21

img = Image.open("stacked_qrs.png")
img = img.resize((img.size[0] // 10, img.size[1] // 10), Image.BOX)

img = img.crop((0, 0, W, H))
put_marker(img, W-8, H-8)
print(decode(img))

img.save("dump.png")

