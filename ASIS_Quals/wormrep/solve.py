from ptrlib import *

with open("wormrep.klr.enc1", "rb") as f:
    buf = f.read()

xml = xor(buf, chr(0xcf^0x20))

with open("dump.xml", "wb") as f:
    f.write(xml)
