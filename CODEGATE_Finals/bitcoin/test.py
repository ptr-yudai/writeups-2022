from ptrlib import *

def pack1(elements):
    payload = bytes([len(elements)])
    for elem in elements:
        assert len(elem[0]) == 0x24
        payload += elem[0]
        payload += bytes([len(elem[1])])
        payload += elem[1]
        payload += p32(elem[2])
    return payload

def pack2(elements):
    payload = bytes([len(elements)])
    for elem in elements:
        assert len(elem[0]) == 8
        payload += elem[0]
        payload += bytes([len(elem[1])])
        payload += elem[1]
    return payload

import hashlib

data  = p64(0x760536d38faef3b7)
data += p64(0x337f4eee4466cbe7)
data += p64(0x314fbb328712e335)
data += p64(0x100a1022cea5623e)
print(hashlib.sha256(hashlib.sha256(data).digest()).hexdigest())

