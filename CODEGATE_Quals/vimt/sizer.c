#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <termios.h>

int main(int argc, char **argv) {
  if (argc < 3) {
    printf("Usage: %s <x size> <y size>\n", argv[0]);
    return 1;
  }

  struct winsize ws;
  ws.ws_col = atoi(argv[1]);
  ws.ws_row = atoi(argv[2]);
  return ioctl(1, TIOCSWINSZ, &ws);
}
