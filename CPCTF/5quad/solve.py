#!/usr/bin/python3

import jwt
#pyjwt==0.4.3

pubkey = """-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAzlUYqKvBVup0Tv62RGUm
bsNIpY8JTZ3p5vPQdQqwzFjpm0mZGPnFo7xV/u2LA7anZI9gd/3zOqvbI0rwbLxm
YQXIViJX8l/RFi6/nMfRTOxZaGcBefZ/lNrnJY1vQOxaF/bRM2kx9wQvIJf+gVvq
WReix2vFplMhFTKPEAzK4EB6IzSOJ9s6yf7umUshOzxXI6LY6dIPYLGTvKm7PJdn
fi+YJlv0+OE2j25HsWGj3raeQJoVEL2tiEzLN+5ID7ddR2bN5xmmWkk3pfyOaZZj
2U/igL/Qz5e9bP6kYpBzjZIROsCpnxO5pooWjAb+13Aemb3d+ecsmf9RBJvU20KW
YQIDAQAB
-----END PUBLIC KEY-----"""

payload = {"username": "ganso-yoshiking1gou", "coins": 123}

cookie = jwt.encode( payload, pubkey, algorithm="HS256")

print(cookie.decode())
