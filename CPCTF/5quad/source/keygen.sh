openssl genrsa -out tmp 2048
openssl rsa -pubout < tmp > rs256.key.pub
openssl pkcs8 -in tmp -out rs256.key -topk8 -nocrypt
rm tmp