import jwt from "jsonwebtoken";

const publicKey = `-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAzlUYqKvBVup0Tv62RGUm
bsNIpY8JTZ3p5vPQdQqwzFjpm0mZGPnFo7xV/u2LA7anZI9gd/3zOqvbI0rwbLxm
YQXIViJX8l/RFi6/nMfRTOxZaGcBefZ/lNrnJY1vQOxaF/bRM2kx9wQvIJf+gVvq
WReix2vFplMhFTKPEAzK4EB6IzSOJ9s6yf7umUshOzxXI6LY6dIPYLGTvKm7PJdn
fi+YJlv0+OE2j25HsWGj3raeQJoVEL2tiEzLN+5ID7ddR2bN5xmmWkk3pfyOaZZj
2U/igL/Qz5e9bP6kYpBzjZIROsCpnxO5pooWjAb+13Aemb3d+ecsmf9RBJvU20KW
YQIDAQAB
-----END PUBLIC KEY-----`;

const privateKey = process.env["PRIVATE_KEY"] ?? "";

const algorithms: jwt.Algorithm[] = [
  "HS256",
  "HS384",
  "HS512",
  "RS256",
  "RS384",
  "RS512",
  "ES256",
  "ES384",
  "ES512",
  "PS256",
  "PS384",
  "PS512",
  // "none"
];

export const signJWT = (obj: Object, algorithm: jwt.Algorithm): string => {
  const token = jwt.sign(obj, privateKey, { algorithm, noTimestamp: true });

  return token;
};

export const verifyJWT = (token: string): { [key: string]: unknown } => {
  const res = jwt.verify(token, publicKey, { algorithms });

  if (typeof res !== "string") {
    return res;
  } else {
    throw new Error("JWT verification failed");
  }
};
