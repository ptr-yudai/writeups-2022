import type { FastifyInstance } from "fastify";
import { signJWT } from "../jwt";
import authHook from "./authHook";

interface IPostRegister {
  username: string;
}

export default async (fastify: FastifyInstance) => {
  fastify.post<{ Body: IPostRegister }>("/register", async (request, reply) => {
    const { username } = request.body;

    if (
      typeof username !== "string" ||
      username === "" ||
      username.length > 20
    ) {
      reply.code(400).send("validation failed");
    }

    const token = signJWT({ username, coins: 0 }, "RS256");

    return reply.cookie("token", token).redirect("/");
  });

  fastify.register(async (fastify) => {
    fastify.addHook("onRequest", authHook);

    fastify.post("/getcoin", async (request, reply) => {
      const { username, coins } = request.user;

      const token = signJWT({ username, coins: coins + 1 }, "RS256");

      return reply.cookie("token", token).redirect("/");
    });
  });
};
