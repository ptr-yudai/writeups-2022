import type { onRequestHookHandler } from "fastify";
import { verifyJWT } from "../jwt";

const authHook: onRequestHookHandler = (request, reply, done) => {
  const jwt = request.cookies["token"];

  if (jwt) {
    try {
      const res = verifyJWT(jwt);
      if (
        typeof res["username"] === "string" &&
        typeof res["coins"] == "number"
      ) {
        request.user = {
          username: res["username"],
          coins: res["coins"],
        };
        done();
        return;
      }
    } catch (e) {
      console.error(e);
    }
  }

  // if not logged in, redirect to /register
  return reply.redirect("/register");
};

export default authHook;
