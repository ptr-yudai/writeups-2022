import type { FastifyInstance } from "fastify";
import authHook from "./authHook";

const flag = process.env["FLAG"] || "CPCTF{fake}";

export default async (fastify: FastifyInstance) => {
  fastify.get("/register", async (request, reply) => {
    return reply.view("templates/register.ejs", { user: request.user });
  });

  fastify.register(async (fastify) => {
    fastify.addHook("onRequest", authHook);

    fastify.get("/", async (request, reply) => {
      return reply.view("templates/index.ejs", { user: request.user, flag });
    });

    fastify.get("/getcoin", async (request, reply) => {
      return reply.view("templates/getcoin.ejs", { user: request.user });
    });

    fastify.get("/logout", async (_request, reply) => {
      return reply.clearCookie("token").redirect("/register");
    });
  });
};
