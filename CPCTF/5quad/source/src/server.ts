import Fastify from "fastify";
import fastifyCookie from "@fastify/cookie";
import fastifyFormbody from "@fastify/formbody";
import pointOfView from "point-of-view";
import ejs from "ejs";
import "dotenv/config";
import template from "./routes/template";
import api from "./routes/api";

const fastify = Fastify();

fastify.register(fastifyCookie);
fastify.register(fastifyFormbody);
fastify.register(pointOfView, {
  engine: { ejs },
});

// add request.user
fastify.decorateRequest("user", null);
declare module "fastify" {
  interface FastifyRequest {
    user: {
      username: string;
      coins: number;
    };
  }
}

fastify.register(template);
fastify.register(api);

const start = async () => {
  try {
    await fastify.listen(3000, "0.0.0.0");
  } catch (err) {
    fastify.log.error(err);
    process.exit(1);
  }
};

start();
