# CPCTF 2022
- Overall Evaluation: Good
- Duration: 6h (Very Good)

- Pwn
  - Source Code: Open
  - Quality: Normal
  - Quantity: Bad (Only 2 pwn tasks)
- Rev
  - Quality: Normal
  - Quantity: Good
- Web
  - Source Code: Open
  - Quality: Good
  - Quantity: Good
- Crypto
  - Source Code: Open
  - Quality: Normal
  - Quantity: Good
- Misc
  - Quality: Not Good
  - Quantity: Too Many
- Forensics
  - Quality: Not Good
  - Quantity: Too Many
  
### Notes
- This CTF has OSINT
- This CTF has PPC
