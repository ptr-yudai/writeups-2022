#include <stdio.h>
#include <stdlib.h>

typedef struct {
  unsigned long len;
  char *content;
} Message;

Message *msg[10];

int get_int(){
  char buf[10];
  int num;
  fgets(buf,10,stdin);
  num = atoi(buf);
  return num;
}

void menu(){
  puts("1. new");
  puts("2. edit");
  puts("3. show");
  puts("4. delete");
}

void new(){
  printf("index> ");
  int index = get_int();
  if(index < 0 || 10 <= index){
    puts("invalid index");
    return;
  }

  msg[index] = malloc(sizeof(Message));

  printf("msg_len> ");
  int len = get_int();

  if(len <= 0){
    puts("invalid length");
    return;
  }
  
  msg[index]->len = len;
  msg[index]->content = malloc(len);

  printf("content> ");
  fgets(msg[index]->content,len,stdin);
}

void edit() {
  printf("index> ");
  int index = get_int();
  if(index < 0 || 10 <= index){
    puts("invalid index");
    return;
  }

  if(!msg[index]){
    puts("empty msg");
    return;
  }

  free(msg[index]->content);

  printf("new_len> ");
  int new_len = get_int();

  if(new_len <= 0){
    puts("invalid length");
    return;
  }

  msg[index]->len = new_len;
  msg[index]->content = malloc(new_len);

  printf("new_content> ");
  fgets(msg[index]->content,msg[index]->len,stdin);
}

void show() {
  printf("index> ");
  int index = get_int();
  if(index < 0 || 10 <= index){
    puts("invalid index");
    return;
  }

  if(!msg[index]){
    puts("empty msg");
    return;
  }

  puts(msg[index]->content);
}

void delete() {
  printf("index> ");
  int index = get_int();
  if(index < 0 || 10 <= index){
    puts("invalid index");
    return;
  }

  if(!msg[index]){
    puts("empty msg");
    return;
  }

  free(msg[index]->content);
  free(msg[index]);
}

int main() {
  setvbuf(stdin, NULL, _IONBF, 0);
  setvbuf(stdout, NULL, _IONBF, 0);
  setvbuf(stderr, NULL, _IONBF, 0);

  while(1){
    menu();
    printf("> ");
    int choice = get_int();
    switch(choice) {
      case 1:
        new();
        break;
      case 2:
        edit();
        break;
      case 3:
        show();
        break;
      case 4:
        delete();
        break;
      default:
        exit(0);
        break;
    }
  }

  return 0;
}
