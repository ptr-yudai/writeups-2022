from Crypto.Util.number import *

m = bytes_to_long(b'CPCTF{?????????????????????????????}')

p = getPrime(512)
q = getPrime(512)
N = p * q
phi = (p - 1) * (q - 1)
d = 0
while d^3 < N:
        e = getPrime(514)
        d = pow(e, -1, phi)

c = pow(m, e, N)

mask = (1 << 1024) - (1 << 512)
dd = int(d) & mask

print('N =', N)
print('e =', e)
print('c =', c)
print('d_upper =', dd)
