from PIL import Image

with open("3E", "rb") as f:
    buf = f.read()

img = Image.new("RGB", (539, 71), (255, 255, 255))

o = 1
for y in range(71):
    for x in range(539):
        r = buf[y + y*539*3 + x*3]
        g = buf[y + y*539*3 + x*3+1]
        b = buf[y + y*539*3 + x*3+2]
        img.putpixel((x, y), (r|g|b, r|g|b, r|g|b))

img.save("output.png")

# CPCTF{15_th15_f113_png}
