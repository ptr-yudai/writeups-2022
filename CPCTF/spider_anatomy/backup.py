from z3 import *

s = Solver()
flag = BitVec("flag", 32)

s.add(RotateLeft(flag, 16) == 0x4fbe2823)

r = s.check()
if r == sat:
    v = s.model()[flag].as_long()
    if v >> 31:
        v = -((v ^ 0xffffffff) + 1)
    print(v)
else:
    print(r)
