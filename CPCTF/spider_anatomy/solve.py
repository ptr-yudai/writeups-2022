import angr
import claripy
from logging import getLogger, WARN

getLogger("angr").setLevel(WARN + 1)
getLogger("claripy").setLevel(WARN + 1)

p = angr.Project("./dump.o", load_options={"auto_load_libs": False})
state = p.factory.blank_state(addr=0x40001a)
flag = claripy.BVV('flag', 32)
state.regs.edi = flag

@p.hook(0x400062, length=0)
def check(state):
    print("hook called!")
    print(state.regs.eax)
    state.add_constraints(state.regs.al == 1)

simgr = p.factory.simulation_manager(state)
simgr.run()
