from randcrack import RandCrack
from ptrlib import *

sock = Socket("nc theluck2.cpctf.space 30001")

rc = RandCrack()

for i in range(624):
    if i % 100 == 0:
        print(i)
    sock.sendlineafter(": ", str("1"))
    r = int(sock.recvregex("is (\d+)\!")[0])
    rc.submit(r)
    sock.sendlineafter(": ", "y")

sock.sendlineafter(": ", str(rc.predict_randrange(0, 4294967295)))

sock.sh()

