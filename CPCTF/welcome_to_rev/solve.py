import angr
import claripy
from logging import getLogger, WARN
getLogger("angr").setLevel(WARN + 1)

p = angr.Project("./main", load_options={"auto_load_libs": False})
state = p.factory.entry_state()
simgr = p.factory.simulation_manager(state)
simgr.explore(find=0x401275, avoid=0x401288)

try:
    found = simgr.found[0]
    print(found.posix.dumps(0))
except IndexError:
    print("Not Found")
