from ptrlib import xor

prefix = "CPCTF"
ans = 'VEVASnm%gJ$ Jv%xx`"!"$c&h'

print(xor(ans, xor(ans, prefix)[0:1]))

