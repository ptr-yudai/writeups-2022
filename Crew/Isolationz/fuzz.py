from ptrlib import *

def gen_expr(n=10000):
    p = Process([
        "/home/ptr/ricsec/fuzzuf/build/tools/nautilus/generator",
        "-g", "./test.json",
        "-t", "1000",
        "-n", str(n)
    ])
    for i in range(n):
        yield p.recvline().decode()
    p.close()

def add(expr):
    sock.sendlineafter(">> ", "1")
    sock.sendlineafter(": ", str(len(expr) + 10))
    sock.sendlineafter(": ", expr)

def delete(index):
    sock.sendlineafter(">> ", "2")
    sock.sendlineafter(": ", str(index))

exprs = gen_expr()

sock = Process(["./ld-linux-x86-64.so.2", "--library-path", "./", "./chall"])

vacant = [True for i in range(10)]
for i in range(100):
    c = random_int(0, 1)
    if c == 0 and any(vacant):
        expr = next(exprs)
        print(f"add('{expr}')")
        add(expr)
        for i in range(10):
            if vacant[i]:
                vacant[i] = False
                break

    elif c == 1 and not all(vacant):
        while True:
            index = random_int(0, 9)
            if not vacant[index]:
                vacant[index] = True
                break
        print(f"delete('{index}')")
        delete(index)

sock.interactive()
