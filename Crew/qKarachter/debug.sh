#!/bin/sh
qemu-system-x86_64 \
    -m 64M \
    -kernel ./bzImage \
    -initrd ./testfs.cpio \
    -append "console=ttyS0 oops=panic panic=1 kpti=1 nokaslr quiet" \
    -cpu kvm64,+smep,+smap \
    -monitor /dev/null \
    -nographic \
    -gdb tcp::12345

