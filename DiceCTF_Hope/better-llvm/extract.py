def dicegang():
 x = input().encode()
 for (a, b) in zip(x, bytes.fromhex('4e434e0a53455f0a584f4b4646530a5e424344410a435e0d4e0a484f0a5e424b5e0a4f4b5953')):
  if a ^ 42 != b:
   return False
 return True
if dicegang():
 print('ok fine you got the flag')
else:
 print('nope >:)')
