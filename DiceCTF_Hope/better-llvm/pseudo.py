from ptrlib import *

with open("chall", "rb") as f:
    data1, data2 = [], []
    f.seek(0x21e0)
    for i in range(0x104//4):
        data1.append(u32(f.read(4)))
    f.seek(0x2300)
    for i in range(0x104//4):
        data1.append(u32(f.read(4)))
    f.seek(0x30e0)
    magic = f.read(54)

def dicegang():
    x = input().encode()
    for (a, b) in zip(x, bytes.fromhex('4e434e0a53455f0a584f4b4646530a5e424344410a435e0d4e0a484f0a5e424b5e0a4f4b5953')):
        if a ^ 42 != b:
            return False
    return True
if dicegang():
    print('ok fine you got the flag')
else:
    print('nope >:)')

code = dicegang
table = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_{}"

d = {}
for j in range(65):
    d[table[j]] = (data1[j], data2[j])

t = (flag, d, False, True, 0, 1, None)
buf = [0] * 0x18690

# hope{111111112222222}
