from ptrlib import *

with open("chall", "rb") as f:
    data1, data2 = [], []
    f.seek(0x21e0)
    for i in range(0x104):
        data1.append(u32(f.read(4), signed=True))
    f.seek(0x2300)
    for i in range(0x104):
        data2.append(u32(f.read(4), signed=True))

# t = (flag, d, False, True, 0, 1, None)
table = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_{}"

d = {}
for j in range(65):
    d[table[j]] = (data1[j], data2[j])

# manually decompiled
def dicegang():
    a, b = d[flag[0]]
    for flag_char in flag[1:]:
        x, y = d[flag_char]
        x -= a
        y -= b
        for char in d:
            n, m = d[char]
            n -= a
            m -= b
            if x*m - y*n < 0:
                return False

        x += a
        y += b
        a, b = x, y
    return True

flag = 'h'
a, b = d[flag[0]]
for pos in range(0x15-1):
    for next_char in table:
        x, y = d[next_char]
        x -= a
        y -= b
        for char in d:
            n, m = d[char]
            n -= a
            m -= b
            if x * m - y * n < 0:
                break
        else:
            if next_char == flag[pos]:
                continue
            else:
                a, b = x + a, y + b
                flag += next_char
                break
        
        #x += a
        #y += b
        #a, b = x, y
    print(flag)
