from ptrlib import *

with open("output.txt", "rb") as f:
    enc = f.read()

print(hex(ord(";") ^ ord("}") ^ ord("?")))
key = xor(enc, "hope{may")[:8]
print(xor(enc, key))
