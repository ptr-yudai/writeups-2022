use std::io::Bytes;
use std::io::Cursor;
use std::io::Write;
use std::ptr::NonNull;

pub trait Device {
    fn get_byte(&mut self) -> u8;
    fn send_byte(&mut self, b: u8);
}

#[derive(Debug)]
pub struct Reader {
    inner: Bytes<Cursor<[u8; 4]>>,
}

impl Reader {
    pub fn new(inner: Bytes<Cursor<[u8; 4]>>) -> Self {
        Self { inner }
    }
}

#[derive(Debug, Default)]
pub struct Writer {
    pub inner: Cursor<Vec<u8>>,
}

impl Writer {
    pub fn new(cc: Cursor<Vec<u8>>) -> Self {
        Self { inner: cc }
    }
}

#[derive(Debug, Default)]
pub struct Arithmetic {
    pos: usize,
    regs: [u8; 3],
}

impl Arithmetic {
    pub fn new() -> Self {
        Self {
            pos: 0,
            ..Default::default()
        }
    }
}

#[derive(Debug)]
pub struct Tape {
    pub pos: usize,
    pub buffer: [u8; 25000],
}

impl Tape {
    pub fn new() -> Self {
        Self {
            pos: 0,
            buffer: [0; 25000],
        }
    }
}

impl Device for Tape {
    fn get_byte(&mut self) -> u8 {
        self.pos = (self.pos + 24999) % 25000;
        self.buffer[self.pos]
    }

    fn send_byte(&mut self, input: u8) {
        self.buffer[self.pos] = input;
        self.pos = (self.pos + 1) % 25000;
    }
}

impl Device for Reader {
    fn get_byte(&mut self) -> u8 {
        self.inner.next().unwrap().unwrap()
    }

    fn send_byte(&mut self, b: u8) {}
}

impl Device for Writer {
    fn get_byte(&mut self) -> u8 {
        0
    }

    fn send_byte(&mut self, b: u8) {
        self.inner.write_all(&[b]).unwrap();
    }
}

impl Device for Arithmetic {
    fn get_byte(&mut self) -> u8 {
        match self.regs[2] {
            0 => self.regs[0] * self.regs[1],
            1 => self.regs[0] + self.regs[1],
            2 => self.regs[0] - self.regs[1],
            3 => self.regs[0] ^ self.regs[1],
            4 => self.regs[0] / self.regs[1],
            5 => self.regs[0] % self.regs[1],
            _ => 0,
        }
    }

    fn send_byte(&mut self, b: u8) {
        self.regs[self.pos] = b;
        self.pos = (self.pos + 1) % 3;
    }
}
