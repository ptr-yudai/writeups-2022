mod drive;

use std::io::*;

use drive::device::*;
use drive::vm::vm_run;
use drive::vm::State;
use rand::thread_rng;
use rand::RngCore;

fn main() {
    let input = stdin().bytes().filter_map(|b| b.ok()).collect::<Vec<_>>();

    for _ in 0..10 {
        let r = thread_rng().next_u32();
        let writer = Cursor::new(vec![0u8; 4]);
        let reader = Cursor::new([0; 4]);
        let mut vm = State {
            pc: 0,
            buffer: [0; 4],
            input: input.clone(),
            devices: vec![
                Box::new(Reader::new(reader.bytes())),
                Box::new(Writer::new(writer)),
                Box::new(Arithmetic::new()),
                Box::new(Tape::new()),
            ],
        };
        vm_run(&mut vm);
        let ii = writer.get_ref();
        let u = u32::from_le_bytes(ii[..].try_into().unwrap());
        if u != encrypt(r) {
            std::process::exit(1);
        }
    }

    println!("{}", std::fs::read_to_string("flag.txt").unwrap());
}

fn encrypt(u: u32) -> u32 {
    let b1 = ((u >> 8) ^ ((u >> 24) + u)) as u8;
    let b2 = ((u >> 16) ^ ((u >> 8) + u)) as u8;
    let mut b0 = (u ^ ((u >> 24) + (u >> 16))) as u8;
    let b3 = ((u >> 24) ^ ((u >> 16) + (u >> 8))) as u8;
    if b3 != 0 {
        if b3 != 1 {
            let mut l = -((b3 & 0xfe) as i8);
            loop {
                b0 = (b1 + b2 * b0) % b3;
                b0 = (b1 + b2 * b0) % b3;
                l += 2;
                if l == 0 {
                    break;
                }
            }
        }
        b0 = (b1 + b2 * b0) % b3;
    }
    ((b3 as u32) << 24) | ((b2 as u32) << 16) | ((b1 as u32) << 8) | b0 as u32
}
