use super::device::*;

pub struct InnerState {
    tape: Tape,
}

pub struct State {
    pub pc: usize,                     // 0
    pub buffer: [u8; 4],               // 8
    pub input: Vec<u8>,                // 0x10, 0x18, 0x20
    pub devices: Vec<Box<dyn Device>>, // 0x38
}

pub fn vm_run(state: &mut State) -> u8 {
    loop {
        let op = state.input[state.pc];
        match op {
            0 => {
                state.pc += 1;
                break 1; // ???
            }
            1..=15 | 17..=31 => {
                state.pc += 1;
                return state.next_byte();
            }
            16 => {
                state.pc += 1;
                let pos = state.next_byte() as usize;
                let byte = state.next_byte();
                state.buffer[pos] = byte;
            }
            32 => {
                state.pc += 1;
                let pos = state.next_byte() as usize;
                let bpos = state.next_byte() as usize;
                state.devices[pos].send_byte(state.buffer[bpos]);
            }
            33 => {
                state.pc += 1;
                let bpos = state.next_byte() as usize;
                let pos = state.next_byte() as usize;
                state.buffer[bpos] = state.devices[pos].get_byte();
            }
            80 => {
                state.pc += 1;
                let r1 = state.buffer[state.next_byte() as usize];
                let r2 = state.buffer[state.next_byte() as usize];
                let word = state.next_word();
                if r1 <= r2 {
                    state.pc = word as usize;
                }
            }
            81 => {
                state.pc += 1;
                let r1 = state.buffer[state.next_byte() as usize];
                let r2 = state.buffer[state.next_byte() as usize];
                let word = state.next_word();
                if r1 <= r2 {
                    state.pc = word as usize;
                }
            }
            _ => {
                state.pc += 1;
                break state.next_byte();
            }
        }
    }
}

impl State {
    fn next_byte(&mut self) -> u8 {
        let b = self.input[self.pc];
        self.pc += 1;
        b
    }

    fn next_word(&mut self) -> u16 {
        let b =
            u16::from_le_bytes(self.input[self.pc..self.pc + 2].try_into().unwrap()).rotate_left(8);
        self.pc += 2;
        b
    }
}
