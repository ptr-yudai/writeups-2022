from PIL import Image

img = Image.open("output.png")
out = Image.new("1", img.size, 1)

for y in range(img.size[1]):
    for x in range(img.size[0]):
        r, g, b, _ = img.getpixel((x, y))
        a, b, c = r & 1, g & 1, b & 1
        out.putpixel((x, y), a ^ b ^ c)

out.save("decoded.png")
