from ptrlib import *

flag = b""
for i in range(100):
    sock = Socket("nc mc.ax 31968")
    l = sock.recvlineafter("> ")
    if i*32 == len(l):
        break
    block = l[i*32:(i+1)*32]
    sock.sendlineafter("< ", block)
    flag += bytes.fromhex(sock.recvlineafter("> ").decode()[:32])
    sock.close()

print(flag)
