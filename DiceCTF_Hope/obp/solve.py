from ptrlib import *

enc = bytes.fromhex(open("output.txt", "r").read())
key = xor(enc, 'hope')[0]
print(xor(enc, chr(key)))
