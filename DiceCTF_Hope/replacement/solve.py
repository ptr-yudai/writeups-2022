from ptrlib import *

with open("output.txt", "r") as f:
    buf = f.read()

table = {
    'h': 'h',
    'w': 'o',
    'f': 'p',
    'y': 'e',
    'A': '{',
    'u': '}',
    'g': ' ',
    'k': 't',
    '"': 'a',
    'S': 'n',
    'q': 'i',
    'V': 'v',
    '.': 'r',
    't': 'y',
    'M': 'g',
    'l': 'u',
    's': 'c',
    '_': 's',
    '}': 'm',
    'i': 'l',
    'j': 'b',
    ' ': 'w',
    ',': 'x',
    'T': '_',
    'd': 'd',
    'v': 'm',
    'c': 'f',
    'B': '.',
    'a': 'B',
    'o': 'A',
    '{': 't',
    'n': 'E',
    'r': ',',
    'x': 'I',
    'b': '"',
    'p': 'O',
    'E': 'k',
    'e': 'q',
    'O': 'V',
    'm': 'S',
    'I': 'j'
}

out = ""
for c in buf:
    if c == 'g':
        out += " "
    else:
        out += c
print(out)

flag = ""
for c in buf:
    if c == '\n':
        flag += '\n'
    elif c in table:
        flag += table[c]
    else:
        flag += '?'
print(flag)

