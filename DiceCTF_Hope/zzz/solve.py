import angr
import claripy
from logging import getLogger, WARN
getLogger("angr").setLevel(WARN + 1)

p = angr.Project("./zzz", load_options={"auto_load_libs": False})
state = p.factory.entry_state()
simgr = p.factory.simulation_manager(state)
simgr.explore(find=0x4021d1, avoid=0x4011a9)

try:
    found = simgr.found[0]
    print(found.posix.dumps(0))
except IndexError:
    print("Not Found")
