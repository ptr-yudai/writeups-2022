from ptrlib import *

with open("/home/ptr/temp/dump.bin", "rb") as f:
    buf = f.read()

for block in chunks(buf, 8):
    print("dq " + hex(u64(block)))
