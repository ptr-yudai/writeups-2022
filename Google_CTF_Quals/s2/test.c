#define _GNU_SOURCE
#include <assert.h>
#include <dlfcn.h>
#include <errno.h>
#include <fcntl.h>
#include <gmp.h>
#include <linux/audit.h>
#include <linux/filter.h>
#include <linux/futex.h>
#include <linux/seccomp.h>
#include <linux/userfaultfd.h>
#include <poll.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/msg.h>
#include <sys/prctl.h>
#include <sys/ptrace.h>
#include <sys/resource.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/syscall.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <sys/un.h>
#include <sys/wait.h>
#include <termios.h>
#include <time.h>
#include <unistd.h>
#include <linux/kvm.h>

#define X86_CHECK_ARCH_AND_LOAD_SYSCALL_NR                     \
  BPF_STMT(BPF_LD | BPF_W | BPF_ABS,                              \
           (offsetof(struct seccomp_data, arch))),                \
    BPF_JUMP(BPF_JMP | BPF_JEQ | BPF_K, AUDIT_ARCH_I386, 1, 0),   \
    BPF_STMT(BPF_RET | BPF_K, SECCOMP_RET_ALLOW),          \
    BPF_STMT(BPF_LD | BPF_W | BPF_ABS,                            \
             (offsetof(struct seccomp_data, nr)))

#define x86_NR_open 5
#define x86_NR_read 3
#define x86_NR_write 4

int main() {
  prctl(PR_SET_NO_NEW_PRIVS, 1, 0, 0, 0);
  struct sock_filter filter[] = {
    X86_CHECK_ARCH_AND_LOAD_SYSCALL_NR,
    BPF_JUMP(BPF_JMP | BPF_JEQ | BPF_K, x86_NR_open, 0, 1),
    BPF_STMT(BPF_RET + BPF_K, SECCOMP_RET_USER_NOTIF),
    BPF_JUMP(BPF_JMP | BPF_JEQ | BPF_K, x86_NR_read, 0, 1),
    BPF_STMT(BPF_RET + BPF_K, SECCOMP_RET_USER_NOTIF),
    BPF_JUMP(BPF_JMP | BPF_JEQ | BPF_K, x86_NR_write, 0, 1),
    BPF_STMT(BPF_RET + BPF_K, SECCOMP_RET_USER_NOTIF),
    BPF_STMT(BPF_RET | BPF_K, SECCOMP_RET_ALLOW),
  };

  struct sock_fprog prog = {
    .len = sizeof(filter) / sizeof(filter[0]),
    .filter = filter,
  };

  int fd = open("dump.bin", O_RDWR | O_CREAT, 0777);
  write(fd, prog.filter, 8 * prog.len);
  close(fd);
  exit(0);

  int notifyFd = syscall(__NR_seccomp,
                         SECCOMP_SET_MODE_FILTER,
                         SECCOMP_FILTER_FLAG_NEW_LISTENER,
                         &prog);
  if (notifyFd == -1) exit(1);

  int pid = fork();
  if (pid) {
    /* parent */
    struct seccomp_notif_sizes sizes;
    struct seccomp_notif *req;
    struct seccomp_notif_resp *resp;

    req = malloc(0x1000);
    resp = malloc(0x1000);
    printf("%p, %p\n", req, resp);

    memset(req, 0, sizes.seccomp_notif);
    puts("recv...");
    ioctl(notifyFd, SECCOMP_IOCTL_NOTIF_RECV, req);
    puts("recv!");

    resp->id = req->id;
    resp->error = 0;
    resp->val = 0;
    resp->flags = SECCOMP_USER_NOTIF_FLAG_CONTINUE;
    
    printf("%ld\n", SECCOMP_USER_NOTIF_FLAG_CONTINUE);
    printf("%ld\n", SECCOMP_IOCTL_NOTIF_SEND);
    printf("%p\n", resp);
    getchar();
    puts("send...");
    ioctl(notifyFd, SECCOMP_IOCTL_NOTIF_SEND, resp);
    puts("send!");
    
  } else {
    puts("mkdir");
    mkdir("hehe", 0777);
    perror("mkdir done");
  }
  
  return 0;
}
