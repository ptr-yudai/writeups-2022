from ptrlib import xor, ror

base = 0x1430 - 0x830

with open("checker_drv.sys", "rb") as f:
    f.seek(0x1430 - base)
    dat_1430 = f.read(0x20)
    f.seek(0x1b30 - base)
    dat_1b30 = f.read(0x10)

    f.seek(0x1a00)
    dat_3000 = f.read(0x2a)
    dat_3030 = f.read(0x100)

    dat_1b30 = xor(dat_1b30, dat_1430[:0x10])
    dat_1b30 = xor(dat_1b30, dat_1430[0x10:])

print(dat_3000)
o = b''
for c in dat_3000:
    o += bytes([ror(c, 3, bits=8)])
dat_3000 = o

o = b''
for c in dat_3000:
    o += bytes([c ^ 0x26])
dat_3000 = o

o = b''
for c in dat_3000:
    o += bytes([ror(c, 4, bits=8)])
dat_3000 = o

o = b''
for c in dat_3000:
    c -= 0x37
    if c < 0: c = (-c ^ 0xff) + 1
    o += bytes([c])
dat_3000 = o

o = b''
for c in dat_3000:
    c -= 0x7b
    if c < 0: c = (-c ^ 0xff) + 1
    o += bytes([c])
dat_3000 = o

o = b''
for c in dat_3000:
    o += bytes([ror(c, 7, bits=8)])
dat_3000 = o

o = b''
for c in dat_3000:
    for v in range(0x100):
        if (v * 0xad) & 0xff == c:
            o += bytes([c])
            break
dat_3000 = o

o = b''
for c in dat_3000:
    o += bytes([ror(c, 2, bits=8)])
dat_3000 = o

print(dat_3000)
