from pwn import *
from ptrlib import xor, ror

base = 0x1430 - 0x830

with open("checker_drv.sys", "rb") as f:
    f.seek(0x1430 - base)
    dat_1430 = f.read(0x20)
    f.seek(0x1b30 - base)
    dat_1b30 = f.read(0x10)

    f.seek(0x1a00)
    dat_3000 = f.read(0x30)
    dat_3030 = f.read(0x100)

    dat_1b30 = xor(dat_1b30, dat_1430[:0x10])
    dat_1b30 = xor(dat_1b30, dat_1430[0x10:])

dat_1b30 = xor(dat_1b30, dat_3030[0xE0:0xF0])
print(disasm(dat_1b30, arch='amd64', os='windows'))
dat_1b30 = xor(dat_1b30, dat_3030[0xF0:0x100])

print("-"*20)

dat_1b30 = xor(dat_1b30, dat_3030[0x40:0x50])
print(disasm(dat_1b30, arch='amd64', os='windows'))
dat_1b30 = xor(dat_1b30, dat_3030[0x50:0x60])

print("-"*20)

dat_1b30 = xor(dat_1b30, dat_3030[0xC0:0xD0])
print(disasm(dat_1b30, arch='amd64', os='windows'))
dat_1b30 = xor(dat_1b30, dat_3030[0xD0:0xE0])

print("-"*20)

dat_1b30 = xor(dat_1b30, dat_3030[0x00:0x10])
print(disasm(dat_1b30, arch='amd64', os='windows'))
dat_1b30 = xor(dat_1b30, dat_3030[0x10:0x20])

print("-"*20)

dat_1b30 = xor(dat_1b30, dat_3030[0x20:0x30])
print(disasm(dat_1b30, arch='amd64', os='windows'))
dat_1b30 = xor(dat_1b30, dat_3030[0x30:0x40])

print("-"*20)

dat_1b30 = xor(dat_1b30, dat_3030[0x80:0x90])
print(disasm(dat_1b30, arch='amd64', os='windows'))
dat_1b30 = xor(dat_1b30, dat_3030[0x90:0xa0])

print("-"*20)

dat_1b30 = xor(dat_1b30, dat_3030[0x60:0x70])
print(disasm(dat_1b30, arch='amd64', os='windows'))
dat_1b30 = xor(dat_1b30, dat_3030[0x70:0x80])

print("-"*20)

dat_1b30 = xor(dat_1b30, dat_3030[0xa0:0xb0])
print(disasm(dat_1b30, arch='amd64', os='windows'))
dat_1b30 = xor(dat_1b30, dat_3030[0xb0:0xc0])
