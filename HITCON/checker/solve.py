from ptrlib import xor, ror, rol

base = 0x1430 - 0x830

with open("checker_drv.sys", "rb") as f:
    f.seek(0x1430 - base)
    dat_1430 = f.read(0x20)
    f.seek(0x1b30 - base)
    dat_1b30 = f.read(0x10)

    f.seek(0x1a00)
    dat_3000 = f.read(0x2b)
    dat_3030 = f.read(0x100)

    dat_1b30 = xor(dat_1b30, dat_1430[:0x10])
    dat_1b30 = xor(dat_1b30, dat_1430[0x10:])

print(dat_3000)
o = b''
for c in dat_3000:
    o += bytes([rol(c, 3, bits=8)])
dat_3000 = o

o = b''
for c in dat_3000:
    o += bytes([c ^ 0x26])
dat_3000 = o

o = b''
for c in dat_3000:
    o += bytes([rol(c, 4, bits=8)])
dat_3000 = o

o = b''
for c in dat_3000:
    o += bytes([(c + 0x37) & 0xff])
dat_3000 = o

o = b''
for c in dat_3000:
    o += bytes([(c + 0x7b) & 0xff])
dat_3000 = o

o = b''
for c in dat_3000:
    o += bytes([rol(c, 7, bits=8)])
dat_3000 = o

o = b''
for c in dat_3000:
    o += bytes([(c * 0xad) & 0xff])
dat_3000 = o

o = b''
for c in dat_3000:
    o += bytes([rol(c, 2, bits=8)])
dat_3000 = o

print(dat_3000)
