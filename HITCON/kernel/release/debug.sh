#!/bin/bash

qemu-system-x86_64 \
    -m 128M \
    -nographic \
    -kernel ./bzImage \
    -append 'console=ttyS0 loglevel=3 oops=panic panic=1 nokaslr' \
    -monitor /dev/null \
    -initrd ./testramfs.cpio.gz  \
    -smp cores=2,threads=2 \
    -cpu kvm64,smep,smap \
    -gdb tcp::12345
