with open("meow_way.exe", "rb") as f:
    buf = f.read()
    f.seek(0x3c18)
    enc = f.read(0x30)

flag = b""
ofs1 = ofs2 = -1
for i in range(48):
    ofs1 = buf.index(b'\x67\x8b\x74\x24\x0c\x67\x8b\x4c\x24\x14', ofs1+1)
    t = buf[ofs1 + 11]
    key = buf[ofs1 + 15]
    ofs2 = buf.index(b'\x52\x50\x6a\x00', ofs2+1)
    val = buf[ofs2 + 5]
    if t == 2:
        c = (enc[i] ^ key) - val
    else:
        c = (val - (enc[i] ^ key))
    if c < 0: c = (-c ^ 0xff) + 1
    flag += bytes([c])

print(flag)

