# Function to convert integer to Roman values
def printRoman(number):
    num = [1, 4, 5, 9, 10, 40, 50, 90,
        100, 400, 500, 900, 1000]
    sym = ["I", "IV", "V", "IX", "X", "XL",
        "L", "XC", "C", "CD", "D", "CM", "M"]
    i = 12
      
    while number:
        div = number // num[i]
        number %= num[i]
  
        while div:
            print(sym[i],end="")
            div -= 1
        i -= 1
  
# Driver code
if __name__ == "__main__":
    """
    for i in range(1000 + 5000, 9999):
        printRoman(i)
        print()
    """
    for i in range(1000, 9999):
        printRoman((i // 1000))
        printRoman((i // 100) % 10)
        printRoman((i // 10) % 10)
        printRoman(i % 10)
        print()
