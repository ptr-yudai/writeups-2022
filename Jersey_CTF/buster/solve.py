import requests
import threading
import time

def check(line):
    s = True
    for i in range(5):
        r = requests.get(f"https://jerseyctf.xyz/{line}")
        if r.text.strip() == f"Welcome to {line}!":
            s = False
            break
    if s:
        print("-"*20)
        print(f"https://jerseyctf.xyz/{line}")
        print("-"*20)

l = []

f = open("directory-list-2.3-big.txt", "r")
cnt = 0
for line in f:
    cnt += 1
    if cnt % 1000 == 0:
        print(cnt)
    line = line.strip()
    threading.Thread(target=check, args=(line,)).start()
    time.sleep(0.01)
