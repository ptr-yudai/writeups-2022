from z3 import *

with open("context-clues", "rb") as f:
    f.seek(0xda0)
    ans = bytes(f.read(0x20))

orig = [BitVec(f'f{i}', 8) for i in range(0x20)]
flag = list(orig)
#flag = [0x40 + i for i in range(0x20)]
#print(''.join(map(chr, flag)))

if isinstance(flag[0], int): print(list(map(hex, flag)))
for i in range(3, 12): # if i == 11 then swap
    flag[i], flag[2] = flag[2], flag[i]

for i in range(6, 17): # if i == 16 then swap
    flag[i] ^= 0x37

for i in range(12, 29):
    flag[i], flag[2] = flag[2], flag[i]

if isinstance(flag[0], int): print(list(map(hex, flag)))
flag[0x10+4], flag[8+3] = flag[8+3], flag[0x10+4]

if isinstance(flag[0], int): print(list(map(hex, flag)))
for j in range(12, 21): # if j == 20 then swap
    flag[j] ^= flag[j-1]

if isinstance(flag[0], int): print(list(map(hex, flag)))
for i in range(17, 28):
    flag[i] ^= 0x37

if isinstance(flag[0], int): print(list(map(hex, flag)))
flag[0x18], flag[5] = flag[5], flag[0x18]
for j in range(3, 8): # if j == 7 then swap
    flag[j] ^= flag[j-1]

if isinstance(flag[0], int): print(list(map(hex, flag)))
for j in range(21, 32):
    flag[j] ^= flag[j-1]

if isinstance(flag[0], int): print(list(map(hex, flag)))
flag[0x18+3], flag[0x18+6] = flag[0x18+6], flag[0x18+3]
for k in range(4, 15): # if k == 14 then swap
    flag[k] ^= 0x78

if isinstance(flag[0], int): print(list(map(hex, flag)))
for j in range(8, 22):
    flag[j] ^= flag[j-1]

print("a")
flag[8+1], flag[6] = flag[6], flag[8+1]

if isinstance(flag[0], int): print(list(map(hex, flag)))
for k in range(13, 22): # if k == 21 then swap
    flag[8+4], flag[k] = flag[k], flag[8+4]

if isinstance(flag[0], int): print(list(map(hex, flag)))
for k in range(15, 18):
    flag[k] ^= 0x78

flag[0x18+4], flag[0x18+1] = flag[0x18+1], flag[0x18+4]

#if isinstance(flag[0], int): print(list(map(hex, flag)))
#for k in range(22, 30):
#    flag[8+4], flag[k] = flag[k], flag[8+4]

#if isinstance(flag[0], int): print(list(map(hex, flag)))
#flag[8+7], flag[0x18+3] = flag[0x18+3], flag[8+7]

#if isinstance(flag[0], int): print(list(map(hex, flag)))

s = Solver()
for i in range(0x20):
    s.add(ans[i] == flag[i])

if isinstance(flag[0], int): print(list(map(hex, flag)))
r = s.check()
if r == sat:
    m = s.model()
    o = ""
    for c in orig:
        o += chr(m[c].as_long())
    print(o)
else:
    print(r)
