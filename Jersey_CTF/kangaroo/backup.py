from ptrlib import *
import re

elf = ELF("kangaroo")
f = open("kangaroo", "rb")

f.seek(elf.symbol('accepts') - 0x200000)
accepts = list(f.read(0x100))

f.seek(elf.symbol('jumps') - 0x200000)
jumps = [u32(f.read(4)) for i in range(0x8000)]

states_base = 0x225780
with open("src.c", "r") as f:
    src = f.read().replace(" ", "").replace("\n", "")
r = re.findall("_setjmp\(stru_([0-9A-F]+)\)\)\{nxstate=jumps\[\(unsigned__int8\)\*flag\+(\d+)\];", src)
states = {}
for x, y in r:
    idx = (int(x, 16) - states_base) // 0xc8
    states[idx] = int(y)

def DFS(n, s=None):
    if s is None: s = []
    #print(s)
    counts = {}
    for i in range(0x100):
        c = jumps[n+i]
        counts[i] = (c, jumps[n:n+0x100].count(c))

    v = sorted(counts.items(), key=lambda x: x[1][1])
    for (i, (c, v)) in v:
        if v > 3: break
        if accepts[c] == 1:
            print(s)
            return
        if c in states:
            DFS(states[c], list(s) + [i])

DFS(0)
