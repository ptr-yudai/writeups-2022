from z3 import *

neko = [('0x0', '0x227210', 106), ('0x2200', '0x22ae38', 192), ('0x6f00', '0x227918', 204), ('0x2b00', '0x228fc0', 115), ('0x4800', '0x227e90', 88), ('0x3200', '0x2264c8', 205), ('0x1100', '0x226658', 243), ('0x1300', '0x22b860', 14), ('0x7c00', '0x2287f0', 97), ('0x3e00', '0x22a4d8', 232), ('0x6300', '0x226720', 179), ('0x1400', '0x2268b0', 14), ('0x1600', '0x22afc8', 217), ('0x7100', '0x225c30', 157), ('0x600', '0x229e98', 162), ('0x5b00', '0x22a1b8', 166), ('0x5f00', '0x228340', 9), ('0x3800', '0x2272d8', 51), ('0x2300', '0x227dc8', 3), ('0x3100', '0x22a5a0', 195), ('0x6400', '0x22b158', 165), ('0x7300', '0x228bd8', 105), ('0x4300', '0x22a988', 53), ('0x6900', '0x229470', 243), ('0x4e00', '0x22af00', 48), ('0x7000', '0x228598', 196), ('0x3b00', '0x227c38', 81), ('0x2f00', '0x226a40', 184), ('0x1800', '0x229c40', 144), ('0x5800', '0x225f50', 79)]

N = len(neko)
orig = list(b'jct???????????\xa4??1tY_a9???6???')
flag = list(orig)

# 0000 --> 2200
print(flag, neko[0])
assert(flag[0] == neko[0][2])
flag, neko = flag[1:], neko[1:]
for i in range(len(flag)):
    flag[i] += 0x5d

# 2200 --> 6f00
print(flag, neko[0])
assert(flag[0] == neko[0][2])
flag, neko = flag[1:], neko[1:]
for i in range(len(flag)):
    flag[i] ^= 0x65
for i in range(len(flag)):
    flag[i] = (flag[i] + 24) % 0x100

# 6f00 --> 2b00
print(flag, neko[0])
assert(flag[0] == neko[0][2])
flag, neko = flag[1:], neko[1:]
for i in range(4):
    for j in range(1, len(flag)):
        flag[0], flag[j] = flag[j], flag[0]
for i in range(len(flag)):
    flag[i] = (flag[i] + 0x65) % 0x100

# 2b00 --> 4800
print(flag, neko[0])
assert(flag[0] == neko[0][2])
flag, neko = flag[1:], neko[1:]
for i in range(5):
    for j in range(1, len(flag)):
        flag[0], flag[j] = flag[j], flag[0]

# 4800 --> 3200
print(flag, neko[0])
assert(flag[0] == neko[0][2])
flag, neko = flag[1:], neko[1:]
for i in range(4):
    for j in range(1, len(flag)):
        flag[0], flag[j] = flag[j], flag[0]
for i in range(len(flag)):
    flag[i] = (flag[i] + 0x65) % 0x100

# 3200 --> 1100
print(flag, neko[0])
assert(flag[0] == neko[0][2])
flag, neko = flag[1:], neko[1:]
for i in range(len(flag)):
    flag[i] = (flag[i] + 0x5d) % 0x100

# 1100 --> 1300
print(flag, neko[0])
assert(flag[0] == neko[0][2])
flag, neko = flag[1:], neko[1:]
for i in range(len(flag)):
    flag[i] ^= 0x1c

# 1300 --> 7c00
print(flag, neko[0])
assert(flag[0] == neko[0][2])
flag, neko = flag[1:], neko[1:]
for i in range(len(flag)):
    flag[i] += 0x5d

# 7c00 --> 3e00
print(flag, neko[0])
assert(flag[0] == neko[0][2])
flag, neko = flag[1:], neko[1:]
for i in range(len(flag)):
    flag[i] += 0x5d

# 3e00 --> 6300
print(flag, neko[0])
assert(flag[0] == neko[0][2])
flag, neko = flag[1:], neko[1:]
for i in range(3):
    for j in range(len(flag)):
        flag[0], flag[j] = flag[j], flag[0]
    for i in range(len(flag)):
        flag[i] -= 0x42

# 6300 --> 1400
print(hex(neko[0][2]))
assert(flag[0] == neko[0][2])
flag, neko = flag[1:], neko[1:]
for i in range(len(flag)):
    flag[i] ^= 0x65
for i in range(len(flag)):
    flag[i] += 24

# 1400 --> 1600
assert(flag[0] == neko[0][2])
flag, neko = flag[1:], neko[1:]
