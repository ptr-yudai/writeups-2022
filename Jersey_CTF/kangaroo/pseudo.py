# perms is something like [1, 6, 0, 5, 2, 3, 7, 4]
# initial inds: [0, 1, 2, 3, 4, 5, 6, 7]
def permute(inds, perm):
    for i in range(8):
        j = i
        while True:
            perm = perms[j]
            if perm < 0: break
            inds[i], inds[perm] = inds[perm], inds[i]
            j, perms[j] = perms[j], perms[j] - 8
    for i in range(8):
        perms[i] += 8

if ope == "accept":
    print("Wow!! That was great!!")
    exit(0)

elif ope == "reject":
    print("Hm, that wasn't very interesting...")
    exit(0)

elif ope == "start":
    print("I sure am in a JUMPY mood today!!")
    print("Let's see what you gave me to JUMP on!!")
    if argc > 1:
        flag = argv[1]
        #GOTO states[nxstate]

    print("Hey!! You didn't give me anything!!")
    exit(1)

elif ope == "env":
    nxstate = jumps[flag[0] + 28928]
    permute(transform_inds, unk_2253a8)
    #GOTO control

elif ope == "transforms":
    for i in range(4):
        for j in range(1, len(flag)):
            flag[0], flag[j] = flag[j], flag[0]
    #GOTO states[nxstate]

elif ope == "control":
    flag++
    if len(flag) == 0:
        if accepts[nxstate]:
            #GOTO accept
        else:
            #GOTO reject
    #GOTO transform[transform_inds]

elif ope == "start": # states[0]
    nxstate = jumps[flag[0]]
    permute(transform_inds, perms)
    #GOTO control

elif ope == "stru_226a40":
    nxstate = jumps[flag[0] + 6144]
    permute(transform_inds, unk_2250e0)
    #GOTO control

elif ope == "stru_227468":
    nxstate = jumps[flag[0] + 9472]
    permute(transform_inds, unk_225148)
    #GOTO control

elif ope == "stru_228ca0":
    nxstate = jumps[flag[0] + 17408]
    permute(transform_inds, unk_225240)
    #GOTO control

elif ope == "stru_22a1b8":
    nxstate = jumps[flag[0] + 24320]
    permute(transform_inds, unk_225318)
    #GOTO control

elif ope == "stru_22a348":
    nxstate = jumps[flag[0] + 24832]
    permute(transform_inds, unk_225328)
    #GOTO control

elif ope == "stru_227dc8":
    nxstate = jumps[flag[0] + 12544]
    permute(transform_inds, unk_2251a8)
    #GOTO control

elif ope == "stru_227918":
    nxstate = jumps[flag[0] + 11008]
    permute(transform_inds, unk_225178)
    #GOTO control

elif ope == "stru_228340":
    nxstate = jumps[flag[0] + 14336]
    permute(transform_inds, unk_2251e0)
    #GOTO control

elif ope == "stru_2279e0":
    nxstate = jumps[flag[0] + 11264]
    permute(transform_inds, unk_225180)
    #GOTO control

elif ope == "stru_227850":
    nxstate = jumps[flag[0] + 10752]
    permute(transform_inds, unk_225170)
    #GOTO control

###
### ...
###

elif ope == "stru_22bd28":
    for i in range(0, len(flag), 4):
        bs = len(flag) - i if len(flag) - i < 4 else 4
        for j in range(1, bs):
            flag[i], flag[i+j] = flag[i+j], flag[i]
    #GOTO states[nxstate]

elif ope == "stru_22beb8":
    for i in range(len(flag)):
        flag[i] ^= 0x1c
    #GOTO state[nxstate]

elif ope == "stru_22bf80":
    for i in range(4):
        for j in range(1, len(flag)):
            flag[0], flag[j] = flag[j], flag[0]
    for i in range(len(flag)):
        flag[i] += 0x65
    #GOTO state[nxstate]

elif ope == "stru_22c048":
    for i in range(len(flag), 3):
        bs = len(flag) - i if len(flag) - i < 3 else 3
        for j in range(1, bs):
            flag[i], flag[i+j] = flag[i+j], flag[i]
    for i in range(len(flag)):
        flag[i] = (flag[i] - 0x53) % 0x100

elif ope == "stru_22c110":
    for i in range(len(flag)):
        flag[i] ^= 0x65
    for i in range(len(flag)):
        flag[i] = (flag[i] + 24) % 0x100
    #GOTO state[nxstate]

elif ope == "stru_22c1d8":
    for i in range(3):
        for j in range(1, len(flag)):
            flag[0], flag[j] = flag[j], flag[0]
    for i in range(len(flag)):
        flag[i] -= 0x42
    #GOTO state[nxstate]
