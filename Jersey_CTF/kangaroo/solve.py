from z3 import *

neko = [('0x0', '0x227210', 106), ('0x2200', '0x22ae38', 192), ('0x6f00', '0x227918', 204), ('0x2b00', '0x228fc0', 115), ('0x4800', '0x227e90', 88), ('0x3200', '0x2264c8', 205), ('0x1100', '0x226658', 243), ('0x1300', '0x22b860', 14), ('0x7c00', '0x2287f0', 97), ('0x3e00', '0x22a4d8', 232), ('0x6300', '0x226720', 179), ('0x1400', '0x2268b0', 14), ('0x1600', '0x22afc8', 217), ('0x7100', '0x225c30', 157), ('0x600', '0x229e98', 162), ('0x5b00', '0x22a1b8', 166), ('0x5f00', '0x228340', 9), ('0x3800', '0x2272d8', 51), ('0x2300', '0x227dc8', 3), ('0x3100', '0x22a5a0', 195), ('0x6400', '0x22b158', 165), ('0x7300', '0x228bd8', 105), ('0x4300', '0x22a988', 53), ('0x6900', '0x229470', 243), ('0x4e00', '0x22af00', 48), ('0x7000', '0x228598', 196), ('0x3b00', '0x227c38', 81), ('0x2f00', '0x226a40', 184), ('0x1800', '0x229c40', 144), ('0x5800', "", 79)]

N = len(neko)
orig = [BitVec(f'flag{i}', 8) for i in range(N)]
flag = list(orig)

s = Solver()

# 0000 --> 2200
s.add(flag[0] == neko[0][2])
flag, neko = flag[1:], neko[1:]
for i in range(len(flag)):
    flag[i] += 0x5d

# 2200 --> 6f00
s.add(flag[0] == neko[0][2])
flag, neko = flag[1:], neko[1:]
for i in range(len(flag)):
    flag[i] ^= 0x65
for i in range(len(flag)):
    flag[i] += 24

# 6f00 --> 2b00
s.add(flag[0] == neko[0][2])
flag, neko = flag[1:], neko[1:]
for i in range(4):
    for j in range(1, len(flag)):
        flag[0], flag[j] = flag[j], flag[0]
for i in range(len(flag)):
    flag[i] += 0x65

# 2b00 --> 4800
s.add(flag[0] == neko[0][2])
flag, neko = flag[1:], neko[1:]
for i in range(5):
    for j in range(1, len(flag)):
        flag[0], flag[j] = flag[j], flag[0]

# 4800 --> 3200
s.add(flag[0] == neko[0][2])
flag, neko = flag[1:], neko[1:]
for i in range(4):
    for j in range(1, len(flag)):
        flag[0], flag[j] = flag[j], flag[0]
for i in range(len(flag)):
    flag[i] += 0x65

# 3200 --> 1100
s.add(flag[0] == neko[0][2])
flag, neko = flag[1:], neko[1:]
for i in range(len(flag)):
    flag[i] += 0x5d

# 1100 --> 1300
s.add(flag[0] == neko[0][2])
flag, neko = flag[1:], neko[1:]
for i in range(len(flag)):
    flag[i] ^= 0x1c

# 1300 --> 7c00
s.add(flag[0] == neko[0][2])
flag, neko = flag[1:], neko[1:]
for i in range(len(flag)):
    flag[i] += 0x5d

# 7c00 --> 3e00
s.add(flag[0] == neko[0][2])
flag, neko = flag[1:], neko[1:]
for i in range(len(flag)):
    flag[i] += 0x5d

# 3e00 --> 6300
s.add(flag[0] == neko[0][2])
flag, neko = flag[1:], neko[1:]
for i in range(3):
    for j in range(len(flag)):
        flag[0], flag[j] = flag[j], flag[0]
for i in range(len(flag)):
    flag[i] -= 0x42

# 6300 --> 1400
print(hex(neko[0][2]))
s.add(flag[0] == neko[0][2])
flag, neko = flag[1:], neko[1:]
for i in range(len(flag)):
    flag[i] ^= 0x65
for i in range(len(flag)):
    flag[i] += 24

# 1400 --> 1600
s.add(flag[0] == neko[0][2])
flag, neko = flag[1:], neko[1:]
for i in range(3):
    for j in range(len(flag)):
        flag[0], flag[j] = flag[j], flag[0]
for i in range(len(flag)):
    flag[i] -= 0x42

# 1600 --> 7100
s.add(flag[0] == neko[0][2])
flag, neko = flag[1:], neko[1:]
for i in range(0, len(flag), 4):
    bs = 4 if len(flag)-i >= 4 else len(flag)-i
    for j in range(1, bs):
        flag[i], flag[i+j] = flag[i+j], flag[i]

# 7100 --> 0600
s.add(flag[0] == neko[0][2])
flag, neko = flag[1:], neko[1:]
for i in range(len(flag)):
    flag[i] ^= 0x65
for i in range(len(flag)):
    flag[i] += 24

# 0600 --> 5b00
s.add(flag[0] == neko[0][2])
flag, neko = flag[1:], neko[1:]
for i in range(0, len(flag), 4):
    bs = 4 if len(flag)-i >= 4 else len(flag)-i
    for j in range(1, bs):
        flag[i], flag[i+j] = flag[i+j], flag[i]

# 5b00 --> 5f00
s.add(flag[0] == neko[0][2])
flag, neko = flag[1:], neko[1:]
for i in range(4):
    for j in range(1, len(flag)):
        flag[0], flag[j] = flag[j], flag[0]
for i in range(len(flag)):
    flag[i] += 0x65

# 5f00 --> 3800
s.add(flag[0] == neko[0][2])
flag, neko = flag[1:], neko[1:]
for i in range(4):
    for j in range(1, len(flag)):
        flag[0], flag[j] = flag[j], flag[0]
for i in range(len(flag)):
    flag[i] += 0x65

# 3800 --> 2300
s.add(flag[0] == neko[0][2])
flag, neko = flag[1:], neko[1:]
for i in range(len(flag)):
    flag[i] += 0x5d

# 2300 --> 3100
s.add(flag[0] == neko[0][2])
flag, neko = flag[1:], neko[1:]
for i in range(len(flag)):
    flag[i] ^= 0x65
for i in range(len(flag)):
    flag[i] += 24

# 3100 --> 6400
s.add(flag[0] == neko[0][2])
flag, neko = flag[1:], neko[1:]
for i in range(len(flag)):
    flag[i] ^= 0x65
for i in range(len(flag)):
    flag[i] += 24

# 6400 --> 7300
s.add(flag[0] == neko[0][2])
flag, neko = flag[1:], neko[1:]
for i in range(len(flag)):
    flag[i] += 0x5d

# 7300 --> 4300
s.add(flag[0] == neko[0][2])
flag, neko = flag[1:], neko[1:]
for i in range(5):
    for j in range(1, len(flag)):
        flag[0], flag[j] = flag[j], flag[0]

# 4300 --> 6900
s.add(flag[0] == neko[0][2])
flag, neko = flag[1:], neko[1:]
for i in range(3):
    for j in range(len(flag)):
        flag[0], flag[j] = flag[j], flag[0]
for i in range(len(flag)):
    flag[i] -= 0x42

# 6900 --> 4e00
s.add(flag[0] == neko[0][2])
flag, neko = flag[1:], neko[1:]
for i in range(len(flag)):
    flag[i] += 0x5d

# 4e00 --> 7000
s.add(flag[0] == neko[0][2])
flag, neko = flag[1:], neko[1:]
for i in range(0, len(flag), 3):
    bs = 3 if len(flag)-i>=3 else len(flag)-i
    for j in range(1, bs):
        flag[i], flag[i+j] = flag[i+j], flag[i]
for i in range(len(flag)):
    flag[i] -= 0x53

# 7000 --> 3b00
s.add(flag[0] == neko[0][2])
flag, neko = flag[1:], neko[1:]
for i in range(0, len(flag), 4):
    bs = 4 if len(flag)-i >= 4 else len(flag)-i
    for j in range(1, bs):
        flag[i], flag[i+j] = flag[i+j], flag[i]

# 3b00 --> 2f00
s.add(flag[0] == neko[0][2])
flag, neko = flag[1:], neko[1:]
for i in range(len(flag)):
    flag[i] ^= 0x65
for i in range(len(flag)):
    flag[i] += 24

# 2f00 --> 1800
s.add(flag[0] == neko[0][2])
flag, neko = flag[1:], neko[1:]
for i in range(0, len(flag), 4):
    bs = 4 if len(flag)-i >= 4 else len(flag)-i
    for j in range(1, bs):
        flag[i], flag[i+j] = flag[i+j], flag[i]

# 1800 --> 5800
s.add(flag[0] == neko[0][2])
flag, neko = flag[1:], neko[1:]
for i in range(5):
    for j in range(1, len(flag)):
        flag[0], flag[j] = flag[j], flag[0]

# terminal
s.add(flag[0] == neko[0][2])

# check
r = s.check()
if r == sat:
    m = s.model()
    print(m)
    ans = {}
    o = b""
    for c in orig:
        if m[c] is not None:
            ans[int(str(c)[4:])] = m[c].as_long()
    for i in range(N):
        o += b'?' if i not in ans else bytes([ans[i]])
    print(o)
else:
    print(r)


