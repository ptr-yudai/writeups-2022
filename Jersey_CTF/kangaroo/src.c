int __cdecl main(int argc, const char **argv, const char **envp)
{
  int i; // [rsp+14h] [rbp-9Ch]
  int this_block_size; // [rsp+18h] [rbp-98h]
  int k; // [rsp+1Ch] [rbp-94h]
  int n; // [rsp+20h] [rbp-90h]
  int ii; // [rsp+24h] [rbp-8Ch]
  int kk; // [rsp+28h] [rbp-88h]
  int nn; // [rsp+2Ch] [rbp-84h]
  int i1; // [rsp+30h] [rbp-80h]
  int i2; // [rsp+34h] [rbp-7Ch]
  int i3; // [rsp+38h] [rbp-78h]
  int i4; // [rsp+3Ch] [rbp-74h]
  int bs; // [rsp+40h] [rbp-70h]
  int i6; // [rsp+44h] [rbp-6Ch]
  int i7; // [rsp+48h] [rbp-68h]
  int j; // [rsp+50h] [rbp-60h]
  char v19; // [rsp+54h] [rbp-5Ch]
  int _len; // [rsp+58h] [rbp-58h]
  int m; // [rsp+5Ch] [rbp-54h]
  char v22; // [rsp+60h] [rbp-50h]
  int v23; // [rsp+64h] [rbp-4Ch]
  int jj; // [rsp+68h] [rbp-48h]
  char v25; // [rsp+6Ch] [rbp-44h]
  int v26; // [rsp+70h] [rbp-40h]
  int mm; // [rsp+74h] [rbp-3Ch]
  char v28; // [rsp+78h] [rbp-38h]
  int i5; // [rsp+80h] [rbp-30h]
  char v30; // [rsp+84h] [rbp-2Ch]
  char *this_block; // [rsp+88h] [rbp-28h]
  char *_flag; // [rsp+90h] [rbp-20h]
  char *v33; // [rsp+98h] [rbp-18h]
  char *v34; // [rsp+A0h] [rbp-10h]
  char *v35; // [rsp+A8h] [rbp-8h]

  if ( _setjmp(stru_22AFC8) )
  {
    nxstate = jumps[(unsigned __int8)*flag + 28928];
    permute((__int64)&transform_inds, (__int64)&unk_2253A8);
    longjmp(control, 1);
  }
  if ( _setjmp(stru_226A40) )
  {
    nxstate = jumps[(unsigned __int8)*flag + 6144];
    permute((__int64)&transform_inds, (__int64)&qword_2250E0);
    longjmp(control, 1);
  }
  if ( _setjmp(stru_227468) )
  {
    nxstate = jumps[(unsigned __int8)*flag + 9472];
    permute((__int64)&transform_inds, (__int64)&qword_225148);
    longjmp(control, 1);
  }
  if ( _setjmp(stru_228CA0) )
  {
    nxstate = jumps[(unsigned __int8)*flag + 17408];
    permute((__int64)&transform_inds, (__int64)&unk_225240);
    longjmp(control, 1);
  }
  if ( _setjmp(accept) )
  {
    puts("Wow!! That was great!!");
    return 0;
  }
  else
  {
    if ( _setjmp(stru_22BD28) )
    {
      for ( i = 0; i < flag_len; i += 4 )
      {
        this_block_size = 4;
        if ( flag_len - i < 4 )
          this_block_size = flag_len - i;
        this_block = &flag[i];
        for ( j = 1; this_block_size > j; ++j )
        {
          v19 = *this_block;
          *this_block = this_block[j];
          this_block[j] = v19;
        }
      }
      longjmp(&states[nxstate], 1);
    }
    if ( _setjmp(stru_22A1B8) )
    {
      nxstate = jumps[(unsigned __int8)*flag + 24320];
      permute((__int64)&transform_inds, (__int64)&unk_225318);
      longjmp(control, 1);
    }
    if ( _setjmp(stru_22BF80) )
    {
      for ( k = 0; k <= 3; ++k )
      {
        _flag = flag;
        _len = flag_len;
        for ( m = 1; _len > m; ++m )
        {
          v22 = *_flag;
          *_flag = _flag[m];
          _flag[m] = v22;
        }
      }
      for ( n = 0; n < flag_len; ++n )
        flag[n] += 0x65;
      longjmp(&states[nxstate], 1);
    }
    if ( _setjmp(stru_22A348) )
    {
      nxstate = jumps[(unsigned __int8)*flag + 24832];
      permute((__int64)&transform_inds, (__int64)&unk_225328);
      longjmp(control, 1);
    }
    if ( _setjmp(stru_227DC8) )
    {
      nxstate = jumps[(unsigned __int8)*flag + 12544];
      permute((__int64)&transform_inds, (__int64)&unk_2251A8);
      longjmp(control, 1);
    }
    if ( _setjmp(stru_227918) )
    {
      nxstate = jumps[(unsigned __int8)*flag + 11008];
      permute((__int64)&transform_inds, (__int64)&unk_225178);
      longjmp(control, 1);
    }
    if ( _setjmp(start) )
    {
      puts("I sure am in a JUMPY mood today!!");
      puts("Let's see what you gave me to JUMP on!!");
      if ( argc > 1 )
      {
        flag = strdup(argv[1]);
        flag_len = strlen(flag);
        if ( flag_len )
          longjmp(&states[nxstate], 1);
      }
      puts("Hey!! You didn't give me anything!!");
      return 1;
    }
    else
    {
      if ( _setjmp(stru_228340) )
      {
        nxstate = jumps[(unsigned __int8)*flag + 14336];
        permute((__int64)&transform_inds, (__int64)&unk_2251E0);
        longjmp(control, 1);
      }
      if ( _setjmp(stru_2279E0) )
      {
        nxstate = jumps[(unsigned __int8)*flag + 11264];
        permute((__int64)&transform_inds, (__int64)&unk_225180);
        longjmp(control, 1);
      }
      if ( _setjmp(stru_227850) )
      {
        nxstate = jumps[(unsigned __int8)*flag + 10752];
        permute((__int64)&transform_inds, (__int64)&unk_225170);
        longjmp(control, 1);
      }                                         // a
      if ( _setjmp(stru_229DD0) )
      {
        nxstate = jumps[(unsigned __int8)*flag + 23040];
        permute((__int64)&transform_inds, (__int64)&unk_2252F0);
        longjmp(control, 1);
      }
      if ( _setjmp(stru_228EF8) )
      {
        nxstate = jumps[(unsigned __int8)*flag + 18176];
        permute((__int64)&transform_inds, (__int64)&unk_225258);
        longjmp(control, 1);
      }
      if ( _setjmp(stru_225CF8) )
      {
        nxstate = jumps[(unsigned __int8)*flag + 1792];
        permute((__int64)&transform_inds, (__int64)&qword_225058);
        longjmp(control, 1);
      }
      if ( _setjmp(stru_2288B8) )
      {
        nxstate = jumps[(unsigned __int8)*flag + 16128];
        permute((__int64)&transform_inds, (__int64)&unk_225218);
        longjmp(control, 1);
      }
      if ( _setjmp(stru_226FB8) )
      {
        nxstate = jumps[(unsigned __int8)*flag + 7936];
        permute((__int64)&transform_inds, (__int64)&qword_225118);
        longjmp(control, 1);
      }
      if ( _setjmp(stru_2264C8) )
      {
        nxstate = jumps[(unsigned __int8)*flag + 4352];
        permute((__int64)&transform_inds, (__int64)&qword_2250A8);
        longjmp(control, 1);
      }
      if ( _setjmp(stru_2281B0) )
      {
        nxstate = jumps[(unsigned __int8)*flag + 13824];
        permute((__int64)&transform_inds, (__int64)&unk_2251D0);
        longjmp(control, 1);
      }
      if ( _setjmp(stru_228728) )
      {
        nxstate = jumps[(unsigned __int8)*flag + 15616];
        permute((__int64)&transform_inds, (__int64)&unk_225208);
        longjmp(control, 1);
      }
      if ( _setjmp(stru_2275F8) )
      {
        nxstate = jumps[(unsigned __int8)*flag + 9984];
        permute((__int64)&transform_inds, (__int64)&qword_225158);
        longjmp(control, 1);
      }
      if ( _setjmp(stru_229920) )
      {
        nxstate = jumps[(unsigned __int8)*flag + 21504];
        permute((__int64)&transform_inds, (__int64)&unk_2252C0);
        longjmp(control, 1);
      }
      if ( _setjmp(stru_226B08) )
      {
        nxstate = jumps[(unsigned __int8)*flag + 6400];
        permute((__int64)&transform_inds, (__int64)&qword_2250E8);
        longjmp(control, 1);
      }
      if ( _setjmp(stru_229790) )
      {
        nxstate = jumps[(unsigned __int8)*flag + 20992];
        permute((__int64)&transform_inds, (__int64)&unk_2252B0);
        longjmp(control, 1);
      }
      if ( _setjmp(stru_227530) )
      {
        nxstate = jumps[(unsigned __int8)*flag + 9728];
        permute((__int64)&transform_inds, (__int64)&qword_225150);
        longjmp(control, 1);
      }
      if ( _setjmp(stru_22B860) )
      {
        nxstate = jumps[(unsigned __int8)*flag + 31744];
        permute((__int64)&transform_inds, (__int64)&unk_225400);
        longjmp(control, 1);
      }
      if ( _setjmp(stru_2267E8) )
      {
        nxstate = jumps[(unsigned __int8)*flag + 5376];
        permute((__int64)&transform_inds, (__int64)&qword_2250C8);
        longjmp(control, 1);
      }
      if ( _setjmp(stru_22B090) )
      {
        nxstate = jumps[(unsigned __int8)*flag + 29184];
        permute((__int64)&transform_inds, (__int64)&unk_2253B0);
        longjmp(control, 1);
      }
      if ( _setjmp(stru_22B6D0) )
      {
        nxstate = jumps[(unsigned __int8)*flag + 31232];
        permute((__int64)&transform_inds, (__int64)&unk_2253F0);
        longjmp(control, 1);
      }
      if ( _setjmp(stru_228278) )
      {
        nxstate = jumps[(unsigned __int8)*flag + 14080];
        permute((__int64)&transform_inds, (__int64)&unk_2251D8);
        longjmp(control, 1);
      }
      if ( _setjmp(transforms) )
      {
        for ( ii = 0; ii <= 4; ++ii )
        {
          v33 = flag;
          v23 = flag_len;
          for ( jj = 1; v23 > jj; ++jj )
          {
            v25 = *v33;
            *v33 = v33[jj];
            v33[jj] = v25;
          }
        }
        longjmp(&states[nxstate], 1);
      }
      if ( _setjmp(stru_227148) )
      {
        nxstate = jumps[(unsigned __int8)*flag + 8448];
        permute((__int64)&transform_inds, (__int64)&qword_225128);
        longjmp(control, 1);
      }
      if ( _setjmp(stru_225910) )
      {
        nxstate = jumps[(unsigned __int8)*flag + 512];
        permute((__int64)&transform_inds, (__int64)&qword_225030);
        longjmp(control, 1);
      }
      if ( _setjmp(stru_225E88) )
      {
        nxstate = jumps[(unsigned __int8)*flag + 2304];
        permute((__int64)&transform_inds, (__int64)&qword_225068);
        longjmp(control, 1);
      }
      if ( _setjmp(stru_225B68) )
      {
        nxstate = jumps[(unsigned __int8)*flag + 1280];
        permute((__int64)&transform_inds, (__int64)&qword_225048);
        longjmp(control, 1);
      }
      if ( !_setjmp(reject) )
      {
        if ( _setjmp(stru_228A48) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 16640];
          permute((__int64)&transform_inds, (__int64)&unk_225228);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_226658) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 4864];
          permute((__int64)&transform_inds, (__int64)&qword_2250B8);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_22B220) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 29696];
          permute((__int64)&transform_inds, (__int64)&unk_2253C0);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_225C30) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 1536];
          permute((__int64)&transform_inds, (__int64)&qword_225050);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_229218) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 19200];
          permute((__int64)&transform_inds, (__int64)&unk_225278);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_227F58) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 13056];
          permute((__int64)&transform_inds, (__int64)&unk_2251B8);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_22A7F8) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 26368];
          permute((__int64)&transform_inds, (__int64)&unk_225358);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_226018) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 2816];
          permute((__int64)&transform_inds, (__int64)&qword_225078);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_229F60) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 23552];
          permute((__int64)&transform_inds, (__int64)&unk_225300);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_228B10) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 16896];
          permute((__int64)&transform_inds, (__int64)&unk_225230);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_2260E0) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 3072];
          permute((__int64)&transform_inds, (__int64)&qword_225080);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_22AB18) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 27392];
          permute((__int64)&transform_inds, (__int64)&unk_225378);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_226338) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 3840];
          permute((__int64)&transform_inds, (__int64)&qword_225098);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_2293A8) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 19712];
          permute((__int64)&transform_inds, (__int64)&unk_225288);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_22BAB8) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 32512];
          permute((__int64)&transform_inds, (__int64)&unk_225418);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_228E30) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 17920];
          permute((__int64)&transform_inds, (__int64)&unk_225250);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_226978) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 5888];
          permute((__int64)&transform_inds, (__int64)&qword_2250D8);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_22A0F0) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 24064];
          permute((__int64)&transform_inds, (__int64)&unk_225310);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_226400) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 4096];
          permute((__int64)&transform_inds, (__int64)&qword_2250A0);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_22AE38) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 28416];
          permute((__int64)&transform_inds, (__int64)&unk_225398);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_226C98) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 6912];
          permute((__int64)&transform_inds, (__int64)&qword_2250F8);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_22A410) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 25088];
          permute((__int64)&transform_inds, (__int64)&unk_225330);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_2259D8) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 768];
          permute((__int64)&transform_inds, (__int64)&qword_225038);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_22AF00) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 28672];
          permute((__int64)&transform_inds, (__int64)&unk_2253A0);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_22ABE0) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 27648];
          permute((__int64)&transform_inds, (__int64)&unk_225380);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_22B798) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 31488];
          permute((__int64)&transform_inds, (__int64)&unk_2253F8);
          longjmp(control, 1);
        }
        if ( _setjmp(control) )
        {
          ++flag;
          if ( !--flag_len )
          {
            if ( accepts[nxstate] )
              longjmp(accept, 1);
            longjmp(reject, 1);
          }
          longjmp(&transforms[transform_inds], 1);
        }
        if ( _setjmp(stru_228D68) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 17664];
          permute((__int64)&transform_inds, (__int64)&unk_225248);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_226720) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 5120];
          permute((__int64)&transform_inds, (__int64)&qword_2250C0);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_22A730) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 26112];
          permute((__int64)&transform_inds, (__int64)&unk_225350);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_22B928) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 32000];
          permute((__int64)&transform_inds, (__int64)&unk_225408);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_2261A8) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 3328];
          permute((__int64)&transform_inds, (__int64)&qword_225088);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_228980) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 0x4000];
          permute((__int64)&transform_inds, (__int64)&unk_225220);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_226E28) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 7424];
          permute((__int64)&transform_inds, (__int64)&qword_225108);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_228FC0) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 18432];
          permute((__int64)&transform_inds, (__int64)&unk_225260);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_225DC0) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 2048];
          permute((__int64)&transform_inds, (__int64)&qword_225060);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_229AB0) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 22016];
          permute((__int64)&transform_inds, (__int64)&unk_2252D0);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_22B158) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 29440];
          permute((__int64)&transform_inds, (__int64)&unk_2253B8);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_227C38) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 12032];
          permute((__int64)&transform_inds, (__int64)&unk_225198);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_2280E8) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 13568];
          permute((__int64)&transform_inds, (__int64)&unk_2251C8);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_2268B0) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 5632];
          permute((__int64)&transform_inds, (__int64)&qword_2250D0);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_22A4D8) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 25344];
          permute((__int64)&transform_inds, (__int64)&unk_225338);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_2292E0) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 19456];
          permute((__int64)&transform_inds, (__int64)&unk_225280);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_22B540) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 30720];
          permute((__int64)&transform_inds, (__int64)&unk_2253E0);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_22A280) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 24576];
          permute((__int64)&transform_inds, (__int64)&unk_225320);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_22B9F0) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 32256];
          permute((__int64)&transform_inds, (__int64)&unk_225410);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_2273A0) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 9216];
          permute((__int64)&transform_inds, (__int64)&qword_225140);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_228598) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 15104];
          permute((__int64)&transform_inds, (__int64)&unk_2251F8);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_22A668) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 25856];
          permute((__int64)&transform_inds, (__int64)&unk_225348);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_22A8C0) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 26624];
          permute((__int64)&transform_inds, (__int64)&unk_225360);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_225AA0) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 1024];
          permute((__int64)&transform_inds, (__int64)&qword_225040);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_229B78) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 22272];
          permute((__int64)&transform_inds, (__int64)&unk_2252D8);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_22C1D8) )
        {
          for ( kk = 0; kk <= 2; ++kk )
          {
            v34 = flag;
            v26 = flag_len;
            for ( mm = 1; v26 > mm; ++mm )
            {
              v28 = *v34;
              *v34 = v34[mm];
              v34[mm] = v28;
            }
          }
          for ( nn = 0; nn < flag_len; ++nn )
            flag[nn] -= 0x42;
          longjmp(&states[nxstate], 1);
        }
        if ( _setjmp(stru_22A988) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 26880];
          permute((__int64)&transform_inds, (__int64)&unk_225368);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_229470) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 19968];
          permute((__int64)&transform_inds, (__int64)&unk_225290);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_2272D8) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 8960];
          permute((__int64)&transform_inds, (__int64)&qword_225138);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_229E98) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 23296];
          permute((__int64)&transform_inds, (__int64)&unk_2252F8);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_227080) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 0x2000];
          permute((__int64)&transform_inds, (__int64)&qword_225120);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_22A028) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 23808];
          permute((__int64)&transform_inds, (__int64)&unk_225308);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_228660) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 15360];
          permute((__int64)&transform_inds, (__int64)&unk_225200);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_226EF0) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 7680];
          permute((__int64)&transform_inds, (__int64)&qword_225110);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_229858) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 21248];
          permute((__int64)&transform_inds, (__int64)&unk_2252B8);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_226270) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 3584];
          permute((__int64)&transform_inds, (__int64)&qword_225090);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_22B478) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 30464];
          permute((__int64)&transform_inds, (__int64)&unk_2253D8);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_226BD0) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 6656];
          permute((__int64)&transform_inds, (__int64)&qword_2250F0);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_227E90) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 12800];
          permute((__int64)&transform_inds, (__int64)&unk_2251B0);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_2284D0) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 14848];
          permute((__int64)&transform_inds, (__int64)&unk_2251F0);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_22ACA8) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 27904];
          permute((__int64)&transform_inds, (__int64)&unk_225388);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_229600) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 20480];
          permute((__int64)&transform_inds, (__int64)&unk_2252A0);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_22A5A0) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 25600];
          permute((__int64)&transform_inds, (__int64)&unk_225340);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_22C110) )
        {
          for ( i1 = 0; i1 < flag_len; ++i1 )
            flag[i1] ^= 0x65u;
          for ( i2 = 0; i2 < flag_len; ++i2 )
            flag[i2] += 24;
          longjmp(&states[nxstate], 1);
        }
        if ( _setjmp(stru_2287F0) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 15872];
          permute((__int64)&transform_inds, (__int64)&unk_225210);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_228BD8) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 17152];
          permute((__int64)&transform_inds, (__int64)&unk_225238);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_228020) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 13312];
          permute((__int64)&transform_inds, (__int64)&unk_2251C0);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_229088) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 18688];
          permute((__int64)&transform_inds, (__int64)&unk_225268);
          longjmp(control, 1);
        }
        if ( _setjmp(states) )
        {
          nxstate = jumps[(unsigned __int8)*flag];
          permute((__int64)&transform_inds, (__int64)&perms);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_229150) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 18944];
          permute((__int64)&transform_inds, (__int64)&unk_225270);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_22AD70) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 28160];
          permute((__int64)&transform_inds, (__int64)&unk_225390);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_2276C0) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 10240];
          permute((__int64)&transform_inds, (__int64)&unk_225160);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_227788) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 10496];
          permute((__int64)&transform_inds, (__int64)&unk_225168);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_228408) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 14592];
          permute((__int64)&transform_inds, (__int64)&unk_2251E8);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_226590) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 4608];
          permute((__int64)&transform_inds, (__int64)&qword_2250B0);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_225F50) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 2560];
          permute((__int64)&transform_inds, (__int64)&qword_225070);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_229C40) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 22528];
          permute((__int64)&transform_inds, (__int64)&unk_2252E0);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_22B608) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 30976];
          permute((__int64)&transform_inds, (__int64)&unk_2253E8);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_227D00) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 12288];
          permute((__int64)&transform_inds, (__int64)&unk_2251A0);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_2299E8) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 21760];
          permute((__int64)&transform_inds, (__int64)&unk_2252C8);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_227210) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 8704];
          permute((__int64)&transform_inds, (__int64)&qword_225130);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_22BDF0) )
        {
          for ( i3 = 0; i3 < flag_len; ++i3 )
            flag[i3] += 93;
          longjmp(&states[nxstate], 1);
        }
        if ( _setjmp(stru_22AA50) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 27136];
          permute((__int64)&transform_inds, (__int64)&unk_225370);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_22C048) )
        {
          for ( i4 = 0; i4 < flag_len; i4 += 3 )
          {
            bs = 3;
            if ( flag_len - i4 < 3 )
              bs = flag_len - i4;
            v35 = &flag[i4];
            for ( i5 = 1; bs > i5; ++i5 )
            {
              v30 = *v35;
              *v35 = v35[i5];
              v35[i5] = v30;
            }
          }
          for ( i6 = 0; i6 < flag_len; ++i6 )
            flag[i6] -= 0x53;
          longjmp(&states[nxstate], 1);
        }
        if ( _setjmp(stru_2296C8) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 20736];
          permute((__int64)&transform_inds, (__int64)&unk_2252A8);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_227B70) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 11776];
          permute((__int64)&transform_inds, (__int64)&unk_225190);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_22BEB8) )
        {
          for ( i7 = 0; i7 < flag_len; ++i7 )
            flag[i7] ^= 0x1Cu;
          longjmp(&states[nxstate], 1);
        }
        if ( _setjmp(stru_22B3B0) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 30208];
          permute((__int64)&transform_inds, (__int64)&unk_2253D0);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_226D60) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 7168];
          permute((__int64)&transform_inds, (__int64)&qword_225100);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_22B2E8) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 29952];
          permute((__int64)&transform_inds, (__int64)&unk_2253C8);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_227AA8) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 11520];
          permute((__int64)&transform_inds, (__int64)&unk_225188);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_225848) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 256];
          permute((__int64)&transform_inds, (__int64)&qword_225028);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_229D08) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 22784];
          permute((__int64)&transform_inds, (__int64)&unk_2252E8);
          longjmp(control, 1);
        }
        if ( _setjmp(stru_229538) )
        {
          nxstate = jumps[(unsigned __int8)*flag + 20224];
          permute((__int64)&transform_inds, (__int64)&unk_225298);
          longjmp(control, 1);
        }
        longjmp(start, 1);
      }
      puts("Hm, that wasn't very interesting...");
      return 0;
    }
  }
}
