from ptrlib import *

elf = ELF("kangaroo")
f = open("kangaroo", "rb")

f.seek(elf.symbol('accepts') - 0x200000)
accepts = list(f.read(0x100))

f.seek(elf.symbol('jumps') - 0x200000)
jumps = [u32(f.read(4)) for i in range(0x8000)]

n = 0x6f00
print(jumps[n:n+0x100])
for i, c in enumerate(jumps[n:n+0x100]):
    if jumps[n:n+0x100].count(c) == 1:
        print(hex(i), hex(c))
