import re

users = {}
ets = {}

f = open("snort.log", "r")
for line in f:
    line = line.strip()
    r = re.findall(".+ Alert:\[\d+\] \"(.+)\" \[IMPACT\] from (.+) at .+ \[Classification: (.+)\] \[Priority: (\d)\]", line)
    msg, user, cls, prio = r[0]

    if user not in users:
        users[user] = 0
    users[user] += 1

    if user != "jerseyctf":
        continue

    if prio == "4":
        print(line)

    if msg.startswith("ET"):
        et = re.findall("ET ([A-Z0-9_]+) ", msg)[0]
        if et not in ets:
            ets[et] = 0
        ets[et] += 1

    elif msg.startswith("GPL"):
        #print(line)
        pass



print(users)
print(ets)
