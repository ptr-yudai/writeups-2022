from ptrlib import *
import random
import time

def create(name):
    assert is_cin_safe(name)
    sock.sendlineafter("off\n", "0")
    sock.sendlineafter("id =\n", name)
def login(name):
    assert is_cin_safe(name)
    sock.sendlineafter("off\n", "1")
    sock.sendlineafter("id =\n", name)
def sendmsg(msg, who):
    assert is_cin_safe(msg)
    assert is_cin_safe(who)
    sock.sendlineafter("off\n", "2")
    sock.sendlineafter("message =\n", msg)
    sock.sendlineafter("whom =\n", who)
def inbox(index):
    sock.sendlineafter("off\n", "3")
    sock.sendlineafter("index =\n", str(index))
    if b'Inbox message' in sock.recvline():
        return sock.recvline()
def delete(index):
    sock.sendlineafter("off\n", "4")
    sock.sendlineafter("index =\n", str(index))
def logout():
    sock.sendlineafter("off\n", "5")

sock = Process("./mail")

accounts = set()
l = False

while True:
    choice = random_int(5)
    if choice == 0:
        name = random_bytes(1, 5, charset="ABC")
        print(f"create({name})")
        create(name)
        accounts.add(name)

    elif choice == 1 and accounts:
        name = random.choice(list(accounts))
        print(f"login({name})")
        login(name)
        l = True

    elif choice == 2 and l:
        msg = random_bytes(1, 5, charset="ABC")
        name = random.choice(list(accounts))
        print(f"sendmsg({msg}, {name})")
        sendmsg(msg, name)

    elif choice == 3 and l:
        id = random_int(0, 3)
        print(f"inbox({id})")
        inbox(id)

    elif choice == 4 and l:
        id = random_int(0, 3)
        print(f"delete({id})")
        delete(id)

    elif choice == 5 and l:
        print(f"logout()")
        logout()
        l = False

    time.sleep(0.01)

sock.interactive()
