import proofofwork
import math
import hashlib
from ptrlib import *

difficulty = 26

sock = Socket("nc 47.242.113.232 31337")
prefix = sock.recvregex("sha256\(\"(.+)\"\+\"\?\"\)")[0]
print(prefix)
s = proofofwork.sha256("0"*((math.ceil(26/4)*4)//4),
                       text=prefix+b"????????????",
                       alphabet=b'0123456789abcdef')
if s is None:
    print("Bad luck!")
    exit(1)
print(s, s[len(prefix):])
print(hashlib.sha256(s).hexdigest())
sock.sendline(s[len(prefix):])

sock.sh()

