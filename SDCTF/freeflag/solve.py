from PIL import Image

img = Image.open("touched.png")

output = ""
for i in range(64):
    r, g, b, a = img.getpixel((i, 0))
    r0, r1 = r & 1, (r & 2) >> 1
    g0, g1 = g & 1, (g & 2) >> 1
    b0, b1 = b & 1, (b & 2) >> 1
    a0, a1 = a & 1, (a & 2) >> 1
    c = 0
    c |= (b1 << 7)
    c |= (r1 << 5)
    c |= (g1 << 6)
    c |= (r0 << 4)
    c |= (a0 << 3)
    c |= (g0 << 2)
    c |= (b0 << 1)
    c |= (a1 << 0)
    output += chr(c)

print(output)
