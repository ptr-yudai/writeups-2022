import io
import base64

u32 = lambda x: int.from_bytes(x, 'big')

with open("output.key", "r") as f:
    b64 = ""
    for line in f:
        if "----" not in line:
            b64 += line.strip()
    data = base64.b64decode(b64)

f = io.BytesIO(data)

AUTH_MAGIC = f.read(len("openssh-key-v1\x00"))
print("AUTH_MAGIC:", AUTH_MAGIC)

ciphername = f.read(u32(f.read(4)))
print("ciphername:", ciphername)
kdfname = f.read(u32(f.read(4)))
print("kdfname:", kdfname)
kdfoptions = f.read(u32(f.read(4)))
print("kdfoptions:", kdfoptions)
num_of_keys = u32(f.read(4))
print("num_of_keys:", num_of_keys)

pubs = []
for i in range(num_of_keys):
    pubs.append(f.read(u32(f.read(4))))

pub = pubs[0]
g = io.BytesIO(pub)

h_base = f.tell() + 4
priv = f.read(u32(f.read(4)))
h = io.BytesIO(priv)

print("[PUBLIC KEY]")
keytype = g.read(u32(g.read(4)))
print(" keytype:", keytype)
e = int.from_bytes(g.read(u32(g.read(4))), "big")
print(" e:", hex(e))
n = int.from_bytes(g.read(u32(g.read(4))), "big")
print(" n:", hex(n))

print("[PRIVATE KEY]")
assert h.read(4) == h.read(4) # check-int
keytype = h.read(u32(h.read(4)))
print(" keytype:", keytype)
n = int.from_bytes(h.read(u32(h.read(4))), "big")
print(" n:", hex(n))
e = int.from_bytes(h.read(u32(h.read(4))), "big")
print(" e:", hex(e))
d = int.from_bytes(h.read(u32(h.read(4))), "big")
print(" d:", hex(d))
iqmp = int.from_bytes(h.read(u32(h.read(4))), "big")
print(" iqmp:", hex(iqmp))
p = int.from_bytes(h.read(u32(h.read(4))), "big")
print(" p:", hex(p))
q = int.from_bytes(h.read(u32(h.read(4))), "big")
print(" q:", hex(q))
print(" comment: ", h.read(u32(h.read(4))))
print(" padding: ", h.read())
