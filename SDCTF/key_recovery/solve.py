import io
import base64

u32 = lambda x: int.from_bytes(x, 'big')

with open("id_rsa.corrupted", "r") as f:
    b64 = ""
    for line in f:
        if "----" not in line:
            b64 += line.strip()
    data = base64.b64decode(b64)

print(len(data))

f = io.BytesIO(data)

AUTH_MAGIC = f.read(len("openssh-key-v1\x00"))
print("AUTH_MAGIC:", AUTH_MAGIC)

ciphername = f.read(u32(f.read(4)))
print("ciphername:", ciphername)
kdfname = f.read(u32(f.read(4)))
print("kdfname:", kdfname)
kdfoptions = f.read(u32(f.read(4)))
print("kdfoptions:", kdfoptions)
num_of_keys = u32(f.read(4))
print("num_of_keys:", num_of_keys)

pubs = []
for i in range(num_of_keys):
    pubs.append(f.read(u32(f.read(4))))

pub = pubs[0]
g = io.BytesIO(pub)

h_base = f.tell() + 4
priv = f.read(u32(f.read(4)))
h = io.BytesIO(priv)

print("[PUBLIC KEY]")
keytype = g.read(u32(g.read(4)))
print(" keytype:", keytype)
e = int.from_bytes(g.read(u32(g.read(4))), "big")
print(" e:", hex(e))
n = int.from_bytes(g.read(u32(g.read(4))), "big")
print(" n:", hex(n))

print("[PRIVATE KEY]")
assert h.read(4) == h.read(4) # check-int
keytype = h.read(u32(h.read(4)))
print(" keytype:", keytype)
n = int.from_bytes(h.read(u32(h.read(4))), "big")
print(" n:", hex(n))
e = int.from_bytes(h.read(u32(h.read(4))), "big")
print(" e:", hex(e))

print(h_base + h.tell())
d = int.from_bytes(h.read(u32(h.read(4))), "big")
print(" d:", hex(d))

# from factordb
from ptrlib import inverse
q = 2239102666933135561942199225708085018160481780967034043147173047566147839050050738202428315348958522434264115740745667710189513247094196395750632928574653735378028005561278278302038978641694242277350257427678723520067491316314717666202751730495393103266518360996211384098928951906037855023727142231156286566083489482555221641491792832584115123420535751875053864863400361980329030505560413787641522985318765990149397031003492064905853575121122602512739897993727029
p = 2304720975948376863359610286058391363543268995439609172215661899731700376813775204230297106360450320802501976838081201590579942180067889742222688077008414263691552744984726414092635334119541091641830599189755142061897046040736060543164061630311637326841024045175364439686677871532139068178542223943598854738781925353561709503743701960412785035904083638325749823108093723759162117295626415249606225750633917328179020525462879693481494374226894054438335455724371133

# fix iqmp
iqmp = inverse(q, p)
iqmp = int.to_bytes(iqmp, u32(h.read(4)), 'big')
if len(iqmp) > 190:
    assert data[454 + 808 + 190:454 + 808 + len(iqmp)] == iqmp[190:]
data = data[:454 + 808] + iqmp + data[454 + 808 + len(iqmp):]
h.read(len(iqmp))

p = int.to_bytes(p, u32(h.read(4)), 'big')
if len(p) > 193:
    assert data[454 + 1004 + 193:454 + 1004 + len(p)] == p[193:]
data = data[:454 + 1004] + p + data[454 + 1004 + len(p):]

h.read(len(p))
q = int.to_bytes(q, u32(h.read(4)), 'big')
if len(q) > 193:
    assert data[454 + 1201 + 193:454 + 1201 + len(q)] == q[193:]
data = data[:454 + 1201] + q + data[454 + 1201 + len(q):]

print(len(data))

import hashlib

HEADER = b'-----BEGIN OPENSSH PRIVATE KEY-----\n'
FOOTER = b'-----END OPENSSH PRIVATE KEY-----\n'
LINE_LENGTH = 70 # Excluding newline characters

data = base64.b64encode(data)
result = HEADER
for i in range(0, len(data), LINE_LENGTH):
    result += data[i:i+LINE_LENGTH] + b'\n'
result += FOOTER

with open("output.key", "wb") as f:
    f.write(result)

print("sdctf{" + hashlib.sha256(result).hexdigest() + "}")
