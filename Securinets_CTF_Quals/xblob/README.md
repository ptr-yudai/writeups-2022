# xblob - Securinets CTF 2022

## About Challenge
### Author
ptr-yudai

### Estimated Difficulty
Hard

### Description
```html
<p>eXclusive BLOB</p>
<code>nc {host} {port}</code><br>
<small>* If you make many connections in a short period of time, you will lose connection for several minutes.</small>
```
Default port is 9001.

### Flag
`securinets{1t's_v3ry_h4rd_2_byp4ss_SMAP_by_4bus1ng_timerfd_ctx}`

## Setup
### Requirements
Install xinetd, hashcash, and qemu-system.
```
# apt install xinetd hashcash qemu-system qemu-kvm
```

### Configuration
Modify port number, user name, and the path to `challenge/qemu/start-qemu.sh` in `challenge/pwn.xinetd`.
Default `per_source` is 3 but you can change it too.

### Run
Copy `challenge/pwn.xinetd` to `/etc/xinetd.d/xblob` and restart xinetd.
```
# cp challenge/pwn.xinetd /etc/xinetd.d/xblob
# systemctl restart xinetd
```
