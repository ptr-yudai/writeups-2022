# gdb -n -q -x solver.py
import gdb
import re
import string


gdb.execute("set pagination off")
gdb.execute("b *0x0000555555401804")
gdb.execute("b *0x0000555555401311")

flag = ""
while True:
    for c in string.printable:
        print(c)
        with open("flag", "w") as f:
            f.write(flag + c)
        gdb.execute("r < flag")
        for i in flag:
            gdb.execute("continue")
        al = gdb.execute("p $al", to_string=True).strip().split("= ")[1]
        cl = gdb.execute("p $cl", to_string=True).strip().split("= ")[1]
        if al == cl:
            flag += c
            print("FLAG:", flag)
            break
