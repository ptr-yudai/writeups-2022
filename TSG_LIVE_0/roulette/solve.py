from ptrlib import *

while True:
    sock = Socket("nc chall.live.ctf.tsg.ne.jp 61234")

    sock.sendlineafter("> ", "1")
    sock.sendlineafter("> ", "0")
    v1 = int(sock.recvlineafter("Result: "))
    sock.sendlineafter("> ", "1")
    sock.sendlineafter("> ", "0")
    v2 = int(sock.recvlineafter("Result: "))
    sock.sendlineafter("> ", "1")
    sock.sendlineafter("> ", "0")
    v3 = int(sock.recvlineafter("Result: "))
    if v1 == v2 == v3:
        print("HIT!", v1, v2, v3)
        break

    sock.close()

sock.sh()
