from string import ascii_uppercase,ascii_lowercase,digits,punctuation

ascii_all=''
for i in range(len(ascii_uppercase)):
  ascii_all=ascii_all+ascii_uppercase[i]+ascii_lowercase[i]
letter=ascii_all+digits+punctuation

def decode(plain):
  cipher=''
  for i in plain:
    index=letter.index(i)
    cipher=cipher+letter[(index+14)%len(letter)]
  return cipher

cipher = open("caesar_output.txt", "r").read()

plain = ''
for c in cipher:
    if c not in letter: continue
    index = letter.index(c)
    plain += letter[(index - 14) % len(letter)]

print(plain)

    
