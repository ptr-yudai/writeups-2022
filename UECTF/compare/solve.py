from PIL import Image

a = Image.open("UECTF_org.bmp")
b = Image.open("UECTF_new.bmp")
#c = Image.new('RGB', a.size)

flag = ""
for y in range(a.size[1]-1, -1, -1):
    for x in range(a.size[0]):
        ca = a.getpixel((x, y))
        cb = b.getpixel((x, y))
        col = (ca[0]^cb[0], ca[1]^cb[1], ca[2]^cb[2])
        if ca != cb:
            if cb[0] != 255:
                flag += chr(cb[0])
            if cb[1] != 255:
                flag += chr(cb[1])
            if cb[2] != 255:
                flag += chr(cb[2])
            print((x, y), flag[-1])

print(flag)
