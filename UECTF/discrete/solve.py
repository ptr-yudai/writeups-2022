import angr
import claripy
from logging import getLogger, WARN

getLogger("angr").setLevel(WARN + 1)
getLogger("claripy").setLevel(WARN + 1)

p = angr.Project("./chall", load_options={"auto_load_libs": False})
state = p.factory.entry_state()
simgr = p.factory.simulation_manager(state)

x = simgr.explore(find=0x40213d, avoid=(0x40200d, 0x4020f3, 0x402150))
print(x.found[0].posix.dumps(0))
