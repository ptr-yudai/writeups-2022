def xor_image(data, key):
    if type(key) != bytes:
        return

    for i, d in enumerate(data):
        yield d ^ key[i % len(prefix)]

def f():
    if len(user_key) != 31:
        return
    if user_key[:len(prefix)] != prefix:
        return
    if H(user_key) != "ce6f4d9b828498b851adea9ba3bd5f6e21ec3f1a463616ed0d3ebd61954d3448":
        return

    output = b''
    with open("flag.jpg_", "rb") as f:
        image_data = f.read()

    output = xor_image(image_data, user_key)

    
