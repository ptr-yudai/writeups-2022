from ptrlib import xor

# UECTF{oh..did1s0m3h0wscr3wup??}
jpeg = open("flag.jpg", "rb").read()
open("output.jpg", "wb").write(xor(jpeg, b"UECTF{"))
