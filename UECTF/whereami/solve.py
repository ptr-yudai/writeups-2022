import re

"""
"23456789CFGHJMPQRVWX"

s = set()
with open("mail.txt", "r") as f:
    for line in f:

print(s)
"""
from openlocationcode import decode,encode
from PIL import Image

img = Image.new("RGB", (100, 500))

flag = ""
with open("mail.txt", "r") as f:
    for line in f:
        v = decode(line).latlng()
        x, y = int(v[0] * 10) - 200, int(v[1] * 10) - 1500
        print(x, y)
        img.putpixel((x,y), (255,0,0))
        #print(v[0], v[1])
        c = int()
        flag += chr(c)

img.save("test.png")
print(flag)
