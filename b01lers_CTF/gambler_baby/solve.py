import ctypes
from ptrlib import *

libc = ctypes.cdll.LoadLibrary("/lib/x86_64-linux-gnu/libc-2.31.so")

#sock = Process("./gambler-baby1")
sock = Socket("nc ctf.b01lers.com 9202")

for i in range((1000 - 100) // 10):
    print(i)
    guess = ''.join([
        chr(libc.rand() % 26 + 97)
        for i in range(4)
    ])
    sock.sendlineafter("letters: ", guess)

sock.interactive()
