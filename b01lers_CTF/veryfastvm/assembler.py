import re

labels = {}
insns = []

with open("exploit.S", "r") as f:
    for line in f:
        insns.append(line.strip())

addr = 0
new_insns = []
for insn in insns:
    if not insn:
        continue
    if insn.startswith("#"):
        continue
    if ":" in insn:
        labels[insn[:-1]] = addr
    else:
        addr += 1
        insn = insn.replace(",", "")
        new_insns.append(insn)

addr = 0
output = ""
for insn in new_insns:
    r = re.findall("<(.+)>", insn)
    for l in labels:
        if r:
            insn = insn.replace(l, str(labels[l]))
        else:
            insn = insn.replace(l, str(labels[l] - addr))
    r = re.findall("<(.+)>", insn)
    if r:
        r = eval(r[0])
        insn = re.sub("(<.+>)", str(r), insn)

    output += insn + "\n"
    addr += 1
output += "\n\n\n"

with open("generated.S", "w") as f:
    f.write(output)
