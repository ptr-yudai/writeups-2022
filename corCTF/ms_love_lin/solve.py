from ptrlib import *

with open("./hoge.exe", "rb") as f:
    f.seek(0x210)
    buf = f.read(0x12)
    f.seek(0x210 + 0x12)
    buf2 = f.read(0x13)

flag = ""
for c in buf:
    flag += chr(ror(c, 5, 8))
for c in buf2:
    flag += chr(c ^ 0xd)

print(flag)
