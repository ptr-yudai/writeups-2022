from Crypto.Cipher import AES
import zlib

with open("msfrob", "rb") as f:
    f.seek(0x2180)
    key = f.read(0x20)
    iv = b'\x00' * 0x10
    f.seek(0x2020)
    enc = f.read(0x2180 - 0x2020)


for i in range(20):
    cipher = AES.new(key, AES.MODE_CBC, iv)
    buf = cipher.decrypt(enc)
    buf = buf[:-buf[-1]]
    enc = zlib.decompress(buf)

print(enc)
