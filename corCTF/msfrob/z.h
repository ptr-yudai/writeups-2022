typedef struct z_stream_s {
    unsigned char    *next_in;
    unsigned int     avail_in;
    unsigned long    total_in;

    unsigned char    *next_out;
    unsigned int     avail_out;
    unsigned long    total_out;

    char     *msg;
    struct internal_state *state;

    void *zalloc;
    void *zfree;
    unsigned char*     opaque;

    int     data_type;
    unsigned long   adler;
    unsigned long   reserved;
} z_stream;
